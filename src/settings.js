_CCSettings = {
    "platform": "android",
    "groupList": [
        "default"
    ],
    "collisionMatrix": [
        [
            true
        ]
    ],
    "rawAssets": {
        "assets": {
            "001488ed-61c9-40a1-a7e0-9152dfe8df0e": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "0033eba7-d47b-4de7-9f80-b6c29c71e066": [
                "resources/textures/images/JoinRoom/Num15.png",
                "cc.Texture2D"
            ],
            "006c6abe-45c5-421e-be27-44d827370cc9": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "009b55b1-565e-483a-8a5c-f9195d4cfa76": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "00d442fc-5cf0-417a-bb75-566f8d97b3b4": [
                "resources/textures/images/efx/guafeng7",
                "cc.SpriteFrame",
                1
            ],
            "0107f7bb-a93d-4c7d-b0e4-2967304d3a22": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "0127a5c8-3692-46f9-8bfb-ab869d931ca3": [
                "resources/textures/images/chat",
                "cc.SpriteFrame",
                1
            ],
            "014e2b9c-aece-45ab-a588-ec19c669546b": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "01a6096d-05ff-4e86-ae0f-c5623c292174": [
                "resources/textures/AQRes/CreateRoom/mkRoll.png",
                "cc.Texture2D"
            ],
            "01ce6d04-c3f8-4fb2-90a0-063813b8fd9b": [
                "resources/textures/bk/top_bar",
                "cc.SpriteFrame",
                1
            ],
            "01d505bd-ed51-458f-b352-5c13e7024818": [
                "resources/textures/images/status/xinhao1.png",
                "cc.Texture2D"
            ],
            "0205788f-58a8-4a00-a601-38241f6c2451": [
                "resources/aqsounds/mj/w_diangang.mp3",
                "cc.AudioClip"
            ],
            "02070caa-b061-47a3-b8fe-8be4bb252b3a": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "021dd735-2275-48c0-88be-3262e61df608": [
                "resources/sounds/nv/6.mp3",
                "cc.AudioClip"
            ],
            "02491912-f027-4dce-88b9-9296333c8dee": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "026b4040-ebfd-487c-a7d4-0806ea5820e1": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "02fb2d75-6aff-4e2f-b971-ecc8a65de7a5": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "02ffd9ec-5eff-426f-a58f-7a771a627a88": [
                "resources/aqsounds/mj/w_12_2.mp3",
                "cc.AudioClip"
            ],
            "03082215-5162-43c9-87df-e1a275d40258": [
                "resources/textures/images/createroom",
                "cc.SpriteFrame",
                1
            ],
            "033b55ea-b302-4dc0-8712-6a4f9c5891bb": [
                "resources/textures/MJ/mjEmpty2",
                "cc.SpriteFrame",
                1
            ],
            "035608a9-3a9a-40dc-b34b-161eed24d3a9": [
                "resources/textures/AQRes/tongIP",
                "cc.SpriteFrame",
                1
            ],
            "0377aeac-841b-4813-890c-8d487d04b367": [
                "resources/textures/images/status/xinhao4.png",
                "cc.Texture2D"
            ],
            "03a6660a-ee76-4896-8673-0f9b9e65a1c0": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "03a988d2-a7c8-4c62-b205-5e38971143f4": [
                "resources/textures/images/GameEnd/GameEnd16",
                "cc.SpriteFrame",
                1
            ],
            "03aef4a9-c4ee-48a7-b66c-e1d352657e60": [
                "resources/textures/loading/dian3",
                "cc.SpriteFrame",
                1
            ],
            "03dd702a-324a-4cf4-ba00-244c404b1ca3": [
                "resources/textures/ShaiZiAni/dice.plist",
                "cc.SpriteAtlas"
            ],
            "03f4c8ac-5e39-48bc-b465-bdf1ea027bea": [
                "resources/aqsounds/mj/26_2.mp3",
                "cc.AudioClip"
            ],
            "0406e813-de32-4d34-8de8-c2774ac54fb7": [
                "resources/textures/images/history_record",
                "cc.SpriteFrame",
                1
            ],
            "04175e45-7938-487d-a471-f6b8204c55e1": [
                "resources/textures/setting/checkbox_full.png",
                "cc.Texture2D"
            ],
            "0449b0bc-f46a-4328-8405-ed35102072bf": [
                "resources/ver/cv.txt",
                "cc.RawAsset"
            ],
            "045cbcbc-b04d-4966-942f-c5fa20e8b925": [
                "resources/textures/AQRes/MJOpsNew/opXianNing",
                "cc.SpriteFrame",
                1
            ],
            "048575bf-b56f-427b-b472-3dc419c883e2": [
                "resources/textures/images/JoinRoom/Num16",
                "cc.SpriteFrame",
                1
            ],
            "04a63aea-dac3-4baa-acef-8aad7dc4ee8c": [
                "resources/aqsounds/mj/w_hu_1.mp3",
                "cc.AudioClip"
            ],
            "04bde641-0dc4-4f16-8a5b-688dc20e38e5": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "04c1d823-f725-45be-a314-f23300134407": [
                "resources/aqsounds/mj/22_1.mp3",
                "cc.AudioClip"
            ],
            "04c56b56-33ea-44be-b318-15b73fb4c2a6": [
                "resources/aqsounds/mj/w_18_1.mp3",
                "cc.AudioClip"
            ],
            "04d79bb4-0cff-43ce-bd62-d1c46b609255": [
                "babykylin.png",
                "cc.Texture2D"
            ],
            "04ddd588-57c3-4d45-8322-0bb2eb8e0dd1": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "04f9d009-67ab-4ecb-bdcb-c52e1f5c8831": [
                "resources/textures/AQRes/MJOpsNew/opZimo.png",
                "cc.Texture2D"
            ],
            "059ebe81-c575-49ff-8a71-3d899278040c": [
                "resources/textures/MJ/right/Z_right.png",
                "cc.Texture2D"
            ],
            "05b80eb8-2054-4f28-9be0-36044421240d": [
                "resources/textures/images/public_ui",
                "cc.SpriteFrame",
                1
            ],
            "06357dfd-5b12-40cd-b94c-cfa0e20d2e72": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "0642b1db-2cd0-4457-a5d8-4b2b70117bfc": [
                "resources/aqsounds/mj/w_guan.mp3",
                "cc.AudioClip"
            ],
            "067c0d10-fb3e-4cdf-bc78-757ca6c68c64": [
                "resources/textures/AQRes/CreateRoom/创建房间.png",
                "cc.Texture2D"
            ],
            "06882731-d0c2-4094-8309-1a95ee3f770b": [
                "resources/textures/images/GameEnd/GameEnd12.png",
                "cc.Texture2D"
            ],
            "06ab8343-75d9-41b2-9388-165764ded274": [
                "resources/textures/voice/v2",
                "cc.SpriteFrame",
                1
            ],
            "06cb0389-d2fc-45bb-a454-b01285153a10": [
                "resources/textures/png/count_down_num.png",
                "cc.Texture2D"
            ],
            "06d0634d-85bc-4c3a-a92f-980420d06dbb": [
                "resources/textures/images/playScenesc/sichuan_room_huatong_002.png",
                "cc.Texture2D"
            ],
            "06d4bb9e-6c43-4338-896b-376455357acb": [
                "resources/textures/images/status/Z_power",
                "cc.SpriteFrame",
                1
            ],
            "072c09ea-3414-4073-9273-97fb0f638311": [
                "resources/textures/AQRes/GameOverOps/txtHU",
                "cc.SpriteFrame",
                1
            ],
            "07367ace-c5b6-430d-ba93-af3488cd3ce3": [
                "resources/textures/AQRes/CreateRoom/btnFree",
                "cc.SpriteFrame",
                1
            ],
            "073df302-1989-4307-aba9-73f2b6364525": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "0742b288-ef58-49da-aa18-226ca275227f": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "0779fc13-7da0-47b5-8a18-ebbdad107f68": [
                "resources/textures/images/GameEnd/GameEnd15.png",
                "cc.Texture2D"
            ],
            "07b178b4-e554-430a-8d6f-732fcac35b6b": [
                "resources/textures/images/Login/btn_weixin",
                "cc.SpriteFrame",
                1
            ],
            "07b19e0a-b505-4d93-a200-e5edc66c99b3": [
                "resources/textures/MJ/mjEmpty",
                "cc.SpriteFrame",
                1
            ],
            "07ef7eba-7d2c-416f-8dc5-80b50fea62e3": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "081223b3-b04d-4d0f-bc16-a80718b48e62": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "0848ba6d-3d07-425a-96ae-44eace0ca3b0": [
                "resources/aqsounds/mj/w_3_2.mp3",
                "cc.AudioClip"
            ],
            "08616609-bfbf-459c-a8af-cf9ea29e453d": [
                "resources/textures/MJRoom/Z_arrow_frame",
                "cc.SpriteFrame",
                1
            ],
            "088725d4-b66d-40d0-a3fe-aeeec07307b5": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "08aa8adc-4f84-4b32-bc75-bb402d5b134b": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "08c092f7-25fe-4479-9c10-f10b6bfe1e20": [
                "resources/aqsounds/voice/chat_m_14.mp3",
                "cc.AudioClip"
            ],
            "092833c8-627a-4f92-9774-83e743083685": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "0970c647-a97f-4dd1-9841-c5a879b9287e": [
                "resources/textures/loading/dian5",
                "cc.SpriteFrame",
                1
            ],
            "09868de4-287c-4845-89f3-89579f51d784": [
                "resources/textures/images/efx/guafeng6.png",
                "cc.Texture2D"
            ],
            "09a1b575-5456-4341-b48a-07f712e4fd8b": [
                "resources/textures/images/status/xinhao2",
                "cc.SpriteFrame",
                1
            ],
            "09bfe702-65eb-47f4-9ccb-7593d9233e0d": [
                "resources/textures/images/setting.plist",
                "cc.SpriteAtlas"
            ],
            "09de0d73-53fe-44c3-8fd5-f02d3dfe80bb": [
                "resources/textures/images/efx/guafeng3",
                "cc.SpriteFrame",
                1
            ],
            "09e3083b-13a3-44f1-b9eb-b468cdff6aeb": [
                "resources/textures/MJRoom/Z_money_frame",
                "cc.SpriteFrame",
                1
            ],
            "0a030286-ff57-47a3-bd96-818af7154024": [
                "resources/textures/images/status/powerG.PNG",
                "cc.Texture2D"
            ],
            "0a0a734f-3633-4b7e-8480-e01c52cb9b6f": [
                "resources/textures/images/chat",
                "cc.SpriteFrame",
                1
            ],
            "0a21888f-598b-4849-9561-700a22e42454": [
                "resources/textures/images/public_ui.png",
                "cc.Texture2D"
            ],
            "0a32c3c0-32d3-4c09-9d87-2bc46021a29b": [
                "resources/textures/images/history_record",
                "cc.SpriteFrame",
                1
            ],
            "0a3bf38c-a691-469e-bd1d-e134c3176ed3": [
                "resources/textures/setting/z_fuxuan_off",
                "cc.SpriteFrame",
                1
            ],
            "0a565548-302e-41ff-904a-2f3100925510": [
                "resources/textures/images/PopupScene/PopupScene16",
                "cc.SpriteFrame",
                1
            ],
            "0a6fef68-87a0-430b-abe7-82b0526bb5bc": [
                "resources/textures/images/public_ui.plist",
                "cc.SpriteAtlas"
            ],
            "0a7d1bf3-6a53-4ebf-ac44-ceda9ca52a24": [
                "resources/textures/images/efx/hu_glow3",
                "cc.SpriteFrame",
                1
            ],
            "0a88337d-d80a-4ebb-91a8-d59053d13980": [
                "resources/textures/AQRes/CreateRoom/返回房间",
                "cc.SpriteFrame",
                1
            ],
            "0acd8ea4-0cc4-4240-9438-b5591b52d6bb": [
                "resources/textures/images/setting",
                "cc.SpriteFrame",
                1
            ],
            "0ad17ec5-044d-4c91-bfad-a12b74acd84e": [
                "resources/aqsounds/mj/12_1.mp3",
                "cc.AudioClip"
            ],
            "0ae56550-08e9-4444-9b8a-ef356408029b": [
                "resources/textures/images/createroom/creatroom10.png",
                "cc.Texture2D"
            ],
            "0af3806a-fc3e-4554-ae11-72122fc2a87d": [
                "resources/textures/images/efx/peng_glow",
                "cc.SpriteFrame",
                1
            ],
            "0b03962f-bfc2-42b3-ba0b-d355eacbd11c": [
                "resources/textures/images/status/xinhao2.png",
                "cc.Texture2D"
            ],
            "0b06e653-7f69-42a1-9892-7b265f6e2a44": [
                "resources/textures/images/Login/bg.jpg",
                "cc.Texture2D"
            ],
            "0b44d615-3d30-4e0e-8536-428e9a1baaf7": [
                "resources/textures/images/efx/rain6.png",
                "cc.Texture2D"
            ],
            "0b76b4ec-bbc9-4aeb-a1de-cbe665509f05": [
                "resources/textures/png/room_num.png",
                "cc.Texture2D"
            ],
            "0b79c326-c0e2-4d0f-bddb-3945ee7f73cd": [
                "resources/textures/MJ/mjEmpty",
                "cc.SpriteFrame",
                1
            ],
            "0b79f9c7-27bc-4cbe-b226-5cae256501c3": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "0b9cf141-3b97-4465-a5bd-fe04e8f6628c": [
                "resources/textures/images/createroom/creatroom5",
                "cc.SpriteFrame",
                1
            ],
            "0b9ded5e-a116-464f-869e-b177c9c02042": [
                "resources/aqsounds/mj/18_1.mp3",
                "cc.AudioClip"
            ],
            "0bc3e47f-8eef-4913-83cd-f1bea2e789a6": [
                "resources/textures/images/efx/rain3.png",
                "cc.Texture2D"
            ],
            "0bd87732-bbd7-4533-88dc-afa996a8b29f": [
                "resources/textures/images/JoinRoom/Num4",
                "cc.SpriteFrame",
                1
            ],
            "0c503f66-4348-4934-9ced-33b9e5b76e72": [
                "resources/textures/images/chat",
                "cc.SpriteFrame",
                1
            ],
            "0c68b464-2b10-4d64-a025-e93952845161": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "0c985548-c639-4c10-9952-50a1381d627e": [
                "resources/textures/AQRes/MJOpsNew/opGuo",
                "cc.SpriteFrame",
                1
            ],
            "0d0d216f-c415-4022-b52d-935c30b91c14": [
                "resources/textures/AQRes/CreateRoom/战绩.png",
                "cc.Texture2D"
            ],
            "0d17a5da-4ce4-468c-ae7c-560d6d04f746": [
                "resources/textures/images/JoinRoom/Num1",
                "cc.SpriteFrame",
                1
            ],
            "0d530de2-7c69-407b-a81e-58f8aa35e92a": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "0d6a1d80-5319-4829-861a-5d28bbff3782": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "0d80e186-2436-4cc8-96c3-176ada38eaa9": [
                "resources/textures/AQRes/GameOverOps/mkFang.png",
                "cc.Texture2D"
            ],
            "0dfec78f-a925-4122-b309-1547e7afed8c": [
                "project.manifest",
                "cc.RawAsset"
            ],
            "0e056be9-3cba-4ffd-8114-ce897359a842": [
                "resources/textures/AQRes/CreateRoom/麻将牌ICON.png",
                "cc.Texture2D"
            ],
            "0e08d727-5083-4210-872c-fd5d2faddd3b": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "0e1a1779-cd6d-466b-880c-fd89275274fa": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "0e1adc87-2354-43b0-a597-ce80d5fe6172": [
                "resources/textures/images/PopupScene/PopupScene6.png",
                "cc.Texture2D"
            ],
            "0e451e72-7884-4ead-9527-535e0794fdbb": [
                "resources/textures/AQRes/CreateRoom/btnDBlueB",
                "cc.SpriteFrame",
                1
            ],
            "0e4a0397-ad57-4eb3-81c8-bbb73a2bc6e1": [
                "resources/textures/AQRes/GameOverOps/txtPH.png",
                "cc.Texture2D"
            ],
            "0e6b66aa-1318-43b7-8874-21b3166ecae2": [
                "resources/textures/AQRes/Hall/txtRound",
                "cc.SpriteFrame",
                1
            ],
            "0e836310-ce9c-4070-b62f-71212c81fcff": [
                "resources/aqsounds/mj/w_peng_2.mp3",
                "cc.AudioClip"
            ],
            "0eb58ac1-9097-4322-8878-b0c4ffeead29": [
                "resources/textures/images/createroom",
                "cc.SpriteFrame",
                1
            ],
            "0ebd449f-102f-4aa2-b56d-703942ef38c8": [
                "resources/sounds/select.mp3",
                "cc.AudioClip"
            ],
            "0f2c2d0c-5e50-4083-95fa-24318c3ec045": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "0f3cf77b-e7f1-49af-8be6-6d1d31a2ddca": [
                "resources/aqsounds/voice/chat_w_11.mp3",
                "cc.AudioClip"
            ],
            "0f4ac92f-b2b9-4f92-a58b-330190ba9e60": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "0f95b280-dc05-4110-97e2-d315a11dbce7": [
                "resources/textures/ops/penggang_bottom",
                "cc.SpriteFrame",
                1
            ],
            "0f9ab8eb-446c-416f-9eac-87fafe193b9e": [
                "resources/textures/images/PopupScene/PopupScene2.png",
                "cc.Texture2D"
            ],
            "0fa4979d-58c8-40d4-b4d7-b40a2b24fae1": [
                "resources/textures/images/chat",
                "cc.SpriteFrame",
                1
            ],
            "0fa69643-22ea-47ac-8307-db3104f214b4": [
                "resources/textures/images/PopupScene/PopupScene1",
                "cc.SpriteFrame",
                1
            ],
            "0fb27d8f-8360-4c08-92c5-2d18eb854610": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "0fd65e09-64e6-4139-94e5-7e0970d0dfc5": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "10284596-4ce7-4153-a4b3-d35f9cccd59e": [
                "resources/textures/images/JoinRoom/Num10",
                "cc.SpriteFrame",
                1
            ],
            "102f4dd8-cb58-463e-bc4f-1b0a85e3a345": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "1054cdb7-065f-4a23-80fe-bb1896a9f6d6": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "10b2f590-dd13-4057-9ea3-ac53c1487106": [
                "resources/textures/chat/chatbg_ld",
                "cc.SpriteFrame",
                1
            ],
            "10f8ab8e-1ff0-4f29-9629-6123d7a217f9": [
                "resources/textures/images/Login/yonghuxieyi.png",
                "cc.Texture2D"
            ],
            "111122ca-2fa5-48c3-96b5-f1b78a2a27bc": [
                "resources/textures/AQRes/MJOpsNew/opBuZhao",
                "cc.SpriteFrame",
                1
            ],
            "11241202-69c2-49ac-a07d-56a8f172c4d5": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "1132394a-d9cc-4421-8e45-fec617cde021": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "1188769b-bd20-41c1-81b0-52a7cda472b3": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "118e175e-deaa-47da-8f22-aa3b92233165": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "11ac894c-3582-4768-bc25-80de5f46a0cd": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "11e1e0e7-8f29-4f20-b8c3-63a1df760681": [
                "resources/textures/MJ/bottom/Z_bottom.png",
                "cc.Texture2D"
            ],
            "124287eb-7819-4f20-91da-50759cb16063": [
                "resources/textures/bk/btn_create_room",
                "cc.SpriteFrame",
                1
            ],
            "1272c881-cc49-4cca-be3f-1dfd7be3a100": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "1283923b-7ba2-47a6-b45b-5d26c746e0c8": [
                "resources/textures/images/JoinRoom/Num13.png",
                "cc.Texture2D"
            ],
            "12e8bd3b-0771-43fb-a746-193612cbd975": [
                "resources/textures/images/createroom/creatroom13",
                "cc.SpriteFrame",
                1
            ],
            "135fe57a-7ae8-489e-8b86-e55f927522ae": [
                "resources/textures/AQRes/distance",
                "cc.SpriteFrame",
                1
            ],
            "13aed263-d9f2-4bb9-8c32-14e98079d987": [
                "resources/textures/images/createroom/creatroom14",
                "cc.SpriteFrame",
                1
            ],
            "13b58dd0-76e4-47ec-8a37-f89329404493": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "14272b0f-9000-4015-a365-e12fc26abb21": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "14630a6e-3d97-4fda-ae43-61809774e116": [
                "resources/sounds/nv/22.mp3",
                "cc.AudioClip"
            ],
            "147fe42f-7640-4b8a-adf2-b4097e0c7e65": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "14c252f0-e5cc-4e20-b191-c63a8a224421": [
                "resources/textures/AQRes/CreateRoom/nv1",
                "cc.SpriteFrame",
                1
            ],
            "1504fb27-c4c3-4d10-9781-fd527df30a19": [
                "resources/textures/images/efx/hu_glow4",
                "cc.SpriteFrame",
                1
            ],
            "150829b1-d3a8-4b43-a7d2-b894c15fb133": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "1517fd6b-e1d5-41e2-9ca6-c0e716735a4f": [
                "resources/textures/images/GameEnd/GameEnd6",
                "cc.SpriteFrame",
                1
            ],
            "15779fcc-c126-461a-ab40-ea7f86b57798": [
                "resources/textures/AQRes/GameOverOps/txtZJ.png",
                "cc.Texture2D"
            ],
            "15f9d9e5-b7e6-4c46-a86e-0243d1db86e4": [
                "resources/textures/images/GameEnd/GameEnd3.png",
                "cc.Texture2D"
            ],
            "16228643-2a90-4cd0-b781-45f6bdc338d6": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "163dc8fe-8ccf-4eb4-bdd5-d3c2578bb010": [
                "resources/sounds/fix_msg_6.mp3",
                "cc.AudioClip"
            ],
            "16472ed8-a860-4006-ae0d-670cc57dc98b": [
                "resources/textures/images/JoinRoom/Num24",
                "cc.SpriteFrame",
                1
            ],
            "16628d5f-248a-49ca-a874-039b72b4b8f3": [
                "resources/textures/AQRes/CreateRoom/chkNo.png",
                "cc.Texture2D"
            ],
            "16ccf94f-85ee-49a4-80ce-24859253da29": [
                "resources/textures/images/createroom/creatroom4.png",
                "cc.Texture2D"
            ],
            "16d3afa4-d1c5-45c6-9448-e56b37b91224": [
                "resources/textures/MJRoom/powerG",
                "cc.SpriteFrame",
                1
            ],
            "16f00a20-3ae0-4865-ad20-5374205e3ff3": [
                "resources/textures/images/JoinRoom/Num8.png",
                "cc.Texture2D"
            ],
            "16f456fc-f3f2-4e0e-b469-3e8d83222be4": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "170ec580-0453-48cf-932f-20798e6ef1ad": [
                "resources/textures/bk/nv.png",
                "cc.Texture2D"
            ],
            "17ad7104-10d9-418c-96cc-0af9154385d4": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "17aea674-35a1-45fc-a43b-c0e84019cce8": [
                "resources/textures/images/GameEnd/GameEnd8.png",
                "cc.Texture2D"
            ],
            "17c15905-21e5-4911-b272-afa575f1d9c2": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "17d21db3-2bd9-4689-a516-de466ff3a146": [
                "resources/textures/AQRes/CreateRoom/progBar.png",
                "cc.Texture2D"
            ],
            "17e73135-88d1-4d85-a528-afd721f44dbe": [
                "resources/textures/images/JoinRoom/Num22",
                "cc.SpriteFrame",
                1
            ],
            "17ecfbb7-86b1-4302-8bcf-5e3e67bece41": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "17f83368-4588-4933-8f57-453ebc21c3ed": [
                "resources/aqsounds/mj/71_1.mp3",
                "cc.AudioClip"
            ],
            "1800c629-4cd4-4c48-96ec-0ae266e6460f": [
                "resources/aqsounds/mj/w_9_1.mp3",
                "cc.AudioClip"
            ],
            "180c0977-60a4-4821-b910-2a561dbe412f": [
                "resources/textures/AQRes/MJOpsNew/opTa",
                "cc.SpriteFrame",
                1
            ],
            "1839fc15-5c8d-41e1-9322-b5a6a323f491": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "18ded696-ea9f-4eba-bcec-deb73d2b63ab": [
                "resources/aqsounds/mj/6_1.mp3",
                "cc.AudioClip"
            ],
            "18fed740-e8f3-4349-8e97-73673edef33e": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "192fb25a-a99c-4023-83cb-0b72a741ba27": [
                "resources/aqsounds/mj/w_kan_2.mp3",
                "cc.AudioClip"
            ],
            "19800559-9d4f-4615-a91f-83b6e82daed6": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "19902851-ccc2-4726-96c2-4d25a61d2a6c": [
                "resources/textures/AQRes/GameOverOps/txtPH",
                "cc.SpriteFrame",
                1
            ],
            "19996b41-8a18-4e93-83a0-50d6a643a5d2": [
                "resources/textures/AQRes/MJOps/opPass",
                "cc.SpriteFrame",
                1
            ],
            "19c90d0e-5557-4b9f-9567-a5813f113e95": [
                "resources/aqsounds/mj/w_8_1.mp3",
                "cc.AudioClip"
            ],
            "19cf2c64-10c2-42eb-b6a4-a3d60b17dd68": [
                "resources/textures/MJRoom/Z_corner_rbottom",
                "cc.SpriteFrame",
                1
            ],
            "19e480fd-213d-4896-bf1c-c355851d83b4": [
                "resources/textures/chat/chatbg_rd.png",
                "cc.Texture2D"
            ],
            "1a06acbc-b41b-49a3-b933-0c96e72c9597": [
                "resources/aqsounds/mj/11_1.mp3",
                "cc.AudioClip"
            ],
            "1a320e5e-e31c-4f54-b7b8-5e6dc7ffc19c": [
                "resources/aqsounds/mj/w_24_1.mp3",
                "cc.AudioClip"
            ],
            "1a69f4e8-766e-4685-bb19-a37cd1138f67": [
                "resources/textures/chat/send_h",
                "cc.SpriteFrame",
                1
            ],
            "1a96991d-0fec-46e8-a593-768de4e1d105": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "1aa1eae2-22a6-4a46-985a-071d51439651": [
                "resources/textures/images/Login/btn_checkbox.png",
                "cc.Texture2D"
            ],
            "1abc02dc-385f-42e1-8538-c55cb93d544a": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "1aecfa70-0b27-44ba-b6c5-65efb20c5948": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "1b484497-3bab-434f-986d-2f12c762a3ba": [
                "resources/textures/images/Login/check_mark",
                "cc.SpriteFrame",
                1
            ],
            "1b4e777a-9a6f-4635-a9c0-76f95c92b07e": [
                "resources/aqsounds/mj/1_1.mp3",
                "cc.AudioClip"
            ],
            "1b59695e-5540-424c-9039-1f104391aeda": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "1b650047-9873-4e70-a87e-f5355261f7aa": [
                "resources/textures/images/playScenesc/sichuan_room_fangxiang_004_01",
                "cc.SpriteFrame",
                1
            ],
            "1b82ac78-83a7-44c9-8b87-367cbbc44f54": [
                "resources/aqsounds/voice/chat_m_4.mp3",
                "cc.AudioClip"
            ],
            "1b8631ea-8fe4-4875-813b-952b39efd34a": [
                "resources/textures/AQRes/CreateRoom/准备.png",
                "cc.Texture2D"
            ],
            "1b8853f2-29c4-4c1e-a04d-3976d4fca30e": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "1bd05074-4290-4ce2-87a2-81b7dfa838f6": [
                "resources/textures/AQRes/MJOpsNew/opBuTongYi",
                "cc.SpriteFrame",
                1
            ],
            "1be0a452-6cb3-45ca-b8b7-a03aa0420e1e": [
                "resources/textures/images/GameEnd/GameEnd7.png",
                "cc.Texture2D"
            ],
            "1be49286-2bf6-4081-9470-97cad682f5f9": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "1c41d745-c430-43f1-a4eb-7041f9084524": [
                "resources/textures/chat/chat_normol.png",
                "cc.Texture2D"
            ],
            "1c6ce929-7636-4bef-bd0c-f5e6afedf728": [
                "resources/aqsounds/mj/4_1.mp3",
                "cc.AudioClip"
            ],
            "1c7cfb7a-1156-4ce2-94cc-449ee730297f": [
                "resources/textures/AQRes/CreateRoom/dirRt",
                "cc.SpriteFrame",
                1
            ],
            "1c807c1c-3b26-4bce-8480-7d0447af76c7": [
                "resources/textures/AQRes/GameOverOps/txtSX3Z",
                "cc.SpriteFrame",
                1
            ],
            "1c88086c-8bdc-4480-b651-82a71273f079": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "1c92c59b-294b-4cb2-b023-d0961ced7701": [
                "resources/textures/images/PopupScene/PopupScene17.png",
                "cc.Texture2D"
            ],
            "1ca02602-2f5a-4484-8762-b014c741e229": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "1ca0ad3c-f918-44f8-b146-9c3afd14da18": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "1cb976c8-389d-4d22-a28e-232ae14f059a": [
                "resources/aqsounds/voice/chat_w_14.mp3",
                "cc.AudioClip"
            ],
            "1cbd6b1a-6cd4-4f59-a49f-53005d224e95": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "1cc34a36-700a-461d-b136-0f8ce6b9a284": [
                "resources/textures/AQRes/CreateRoom/lobby",
                "cc.SpriteFrame",
                1
            ],
            "1cf3b039-aca7-4cde-8987-9eb965c28557": [
                "resources/textures/AQRes/BtnPNG/消息.png",
                "cc.Texture2D"
            ],
            "1d09396e-f87c-4ba0-8b4b-6b1473ff279b": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "1d612a2e-6a32-4141-8e5e-f1b5d3f32301": [
                "resources/textures/AQRes/MJOpsNew/opPeng.png",
                "cc.Texture2D"
            ],
            "1d638d00-9c5b-4c0e-9358-9554293d4aee": [
                "resources/textures/AQRes/MJOps/opPeng",
                "cc.SpriteFrame",
                1
            ],
            "1d8873d5-1f11-43be-b233-8047bb53fa5f": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "1d944e27-df1b-41ef-875e-6fc8cc003353": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "1d96874b-13bf-4d95-be33-7d45957d435a": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "1dd2be1a-f5cb-4386-a063-625c0acd8eb6": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "1de08c57-3928-4b19-bfbe-1180881ae115": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "1e1d77d8-78e5-48f1-8bb5-f78e08379d38": [
                "resources/textures/images/playScenesc/sichuan_room_huatong_001.png",
                "cc.Texture2D"
            ],
            "1e1d9a78-31e8-4d5e-a13e-8effc29436f8": [
                "resources/textures/AQRes/MJOpsNew/opTa.png",
                "cc.Texture2D"
            ],
            "1e2dea84-30f8-46bb-8d4f-ad635075fa4a": [
                "resources/aqsounds/mj/peng_2.mp3",
                "cc.AudioClip"
            ],
            "1ef330bd-dece-4e22-98a9-cb5125059222": [
                "resources/textures/images/JoinRoom/Num17",
                "cc.SpriteFrame",
                1
            ],
            "1efbc606-874d-4c6c-b5e9-1799a28ad426": [
                "resources/textures/images/createroom",
                "cc.SpriteFrame",
                1
            ],
            "1f050b19-2ac9-4dbd-9907-e79b479a843e": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "1f128bdb-6db3-4548-a3e1-217520f26245": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "1f262abc-c8cf-4ec6-85f6-e252ef2e1223": [
                "resources/aqsounds/voice/chat_m_5.mp3",
                "cc.AudioClip"
            ],
            "1f337ec9-dafc-41c7-9f98-213d943efcb9": [
                "resources/textures/images/dingque",
                "cc.SpriteFrame",
                1
            ],
            "1f3f7910-9f9f-42fe-bb75-232047d45055": [
                "resources/textures/images/status/xinhao4",
                "cc.SpriteFrame",
                1
            ],
            "1f7d9ced-8c7a-4afb-865a-e73236e33562": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "1f7e6bb4-8413-4569-adb8-4aece672f566": [
                "resources/textures/setting/titlebg",
                "cc.SpriteFrame",
                1
            ],
            "1f83093f-b3ca-4781-86fc-7de76ababa40": [
                "resources/textures/images/JoinRoom/Num20",
                "cc.SpriteFrame",
                1
            ],
            "1fdd675c-f41b-4fdf-9ff2-8f848772a92a": [
                "resources/textures/images/history_record",
                "cc.SpriteFrame",
                1
            ],
            "203d3fc3-ed0c-4233-bf82-c7588f4b71c7": [
                "resources/sounds/horse/bet.mp3",
                "cc.AudioClip"
            ],
            "209ec84a-5f4d-44f1-b6b3-7f06a19554e6": [
                "resources/textures/AQRes/MJOps/opZhaoNing",
                "cc.SpriteFrame",
                1
            ],
            "20df9c86-952e-43f0-a014-d3f5a3686a14": [
                "HotUpdate/Texture/gb_inputbox.png",
                "cc.Texture2D"
            ],
            "20e91468-a3cc-4bcb-b150-8cc19161b1ba": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "2113dde6-d3ae-4298-947d-6f74dda3f683": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "2133eb49-bb89-480b-b451-43becbb3cef2": [
                "resources/textures/MJ/right/R_character_1.png",
                "cc.Texture2D"
            ],
            "216ecf6c-1d78-4218-a52d-9ed7712b5831": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "21b4a1e0-3c00-4150-a6b6-b4b41829f1e2": [
                "resources/textures/AQRes/Hall/erweima",
                "cc.SpriteFrame",
                1
            ],
            "22080299-d7f9-4918-a7d9-cccefd0e8dc5": [
                "resources/textures/images/createroom/creatroom3",
                "cc.SpriteFrame",
                1
            ],
            "2216fc04-899e-4815-bf7c-d290e385bf38": [
                "resources/textures/AQRes/CreateRoom/邀请好友.png",
                "cc.Texture2D"
            ],
            "222a745c-276f-4315-951d-e9a9a7f64a92": [
                "resources/textures/images/createroom/creatroom15",
                "cc.SpriteFrame",
                1
            ],
            "225cd024-e1df-4ebc-8769-ef31947d3051": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "225cf3da-f4d4-463d-ad5a-6db4211fec6f": [
                "resources/textures/images/createroom",
                "cc.SpriteFrame",
                1
            ],
            "22769470-75bb-45df-9e02-cc357d7f3dea": [
                "resources/aqsounds/mj/w_3_1.mp3",
                "cc.AudioClip"
            ],
            "227ac05d-89d6-4939-bf0b-b7c1e48bc56d": [
                "resources/textures/loading/dian5.png",
                "cc.Texture2D"
            ],
            "229ca516-c90e-4092-9f73-2f758b46b2fb": [
                "resources/textures/images/history_record",
                "cc.SpriteFrame",
                1
            ],
            "22db738b-5605-4b88-ae0a-41fd1afab3e2": [
                "resources/textures/AQRes/Hall/txtRound.png",
                "cc.Texture2D"
            ],
            "23255f33-a36f-4030-aa68-1b7cc770b278": [
                "resources/textures/AQRes/GameOverOps/txtTP.png",
                "cc.Texture2D"
            ],
            "234dedb0-678d-4893-9486-2e471a0f0a17": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "2352a9bf-5e78-4c9c-9509-33673bea9b74": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "235c04a8-e57f-4d4f-860c-7eaaed62a1ed": [
                "resources/textures/images/createroom/creatroom11.png",
                "cc.Texture2D"
            ],
            "237032f8-9668-4bdc-bc99-960e5c53bd9f": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "23790826-1d37-4f95-8765-1a77abc15c3f": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "238d0017-c32c-44fa-bcc5-0211ed1cd08e": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "2397833d-46a0-4fff-b8ab-422b96e2c4df": [
                "resources/textures/voice/v7",
                "cc.SpriteFrame",
                1
            ],
            "23c1df4f-05f6-4beb-8171-4d323697ebe3": [
                "resources/textures/images/JoinRoom/Num21.png",
                "cc.Texture2D"
            ],
            "23d25b30-e11b-4057-ad29-758f259c1c40": [
                "resources/textures/MJRoom/Z_bg_bottom",
                "cc.SpriteFrame",
                1
            ],
            "23d33ec7-e09d-48ed-b585-a5ec8a9f5ecd": [
                "HotUpdate/Texture/button_orange.png",
                "cc.Texture2D"
            ],
            "23fdb5af-9999-464a-9131-bcf186d31075": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "24670f05-f6d6-42a2-b7a4-ef9e463011f7": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "24697d0f-0baa-431b-82e8-d74c1124a91c": [
                "resources/textures/images/replayBtn.plist",
                "cc.SpriteAtlas"
            ],
            "248264c1-6e44-424d-84f2-874f6d910514": [
                "resources/textures/AQRes/Hall/btnVoice",
                "cc.SpriteFrame",
                1
            ],
            "249efc01-de45-4d92-8851-12b8e612751d": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "24bd55f3-2acf-46f4-99b5-7e32b4127773": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "24d5f805-9693-41e3-9916-b5a4c9d4a57e": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "253e4e20-3e81-456d-af80-ec25c31590da": [
                "resources/textures/images/PopupScene/PopupScene18",
                "cc.SpriteFrame",
                1
            ],
            "254047b0-2815-4789-b31d-33f7caf0c12e": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "25b01ded-55d1-4ea1-a84c-401d10341444": [
                "resources/sounds/fix_msg_2.mp3",
                "cc.AudioClip"
            ],
            "26069576-2594-45a0-a28a-693ee5471312": [
                "resources/textures/MJ/my/M_character_3.png",
                "cc.Texture2D"
            ],
            "2625c55c-353c-4953-82b7-13472755e8ca": [
                "resources/sounds/ui_click.mp3",
                "cc.AudioClip"
            ],
            "26aba82d-8cef-45cb-948a-70673550e365": [
                "resources/sounds/nv/14.mp3",
                "cc.AudioClip"
            ],
            "26c30d9f-add5-4218-b3e2-c0ad68c33899": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "26db4da3-a1a1-4145-99b8-dc05d712d1c4": [
                "resources/textures/images/efx/peng_glow.png",
                "cc.Texture2D"
            ],
            "270c44d3-8a33-42e3-9543-11c2abeea16e": [
                "HotUpdate/Texture/bg_rankinglist.png",
                "cc.Texture2D"
            ],
            "272b1e58-0469-43a4-97ad-12c0ba28aa77": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "276d22af-1571-4d2d-9ba0-e97ff28b8df5": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "278637fb-c606-45a9-8ebd-7772964f75db": [
                "resources/textures/AQRes/GameOverOps/mkXian",
                "cc.SpriteFrame",
                1
            ],
            "2799b8e3-7e4a-4963-a626-dedf9f936481": [
                "resources/textures/images/history_record",
                "cc.SpriteFrame",
                1
            ],
            "280e5e06-0f18-4343-9a38-2652bb6561f8": [
                "resources/aqsounds/mj/w_24_2.mp3",
                "cc.AudioClip"
            ],
            "2811571c-9228-48f8-9663-69f4bb7daf00": [
                "resources/textures/images/JoinRoom/Num18.png",
                "cc.Texture2D"
            ],
            "2822bb44-286f-40a1-9398-35a3bb0bd761": [
                "resources/textures/images/playScenesc/wenzi002",
                "cc.SpriteFrame",
                1
            ],
            "282e64c4-424f-4d65-b265-430ae59d9fa5": [
                "resources/textures/MJ/mjEmpty",
                "cc.SpriteFrame",
                1
            ],
            "283bfa19-e361-49b5-82b8-51defce612f1": [
                "resources/textures/images/JoinRoom/Num8",
                "cc.SpriteFrame",
                1
            ],
            "285825b5-c3e3-49a8-b9c8-eea6d3581dc1": [
                "resources/textures/images/playScenesc/sichuan_room_fangxiang.png",
                "cc.Texture2D"
            ],
            "28676dcd-8632-4ba8-abef-0ba3cbaee8c4": [
                "resources/aqsounds/mj/2_1.mp3",
                "cc.AudioClip"
            ],
            "28cd14db-6ba3-458d-bcb3-ea53a7adddc3": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "293b5669-025c-486a-b872-8323da18436f": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "29491748-67d0-4e30-86bc-dff3bf09cc0c": [
                "resources/aqsounds/mj/19_1.mp3",
                "cc.AudioClip"
            ],
            "29943682-0380-4bb1-b0ff-77e314f4d444": [
                "resources/aqsounds/mj/81_2.mp3",
                "cc.AudioClip"
            ],
            "2a090963-a3c5-41dd-95c2-62bc2b9ada79": [
                "resources/sounds/nv/21.mp3",
                "cc.AudioClip"
            ],
            "2a40e28e-fdc0-4139-82cc-2e9051c05660": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "2a5d1317-547d-40b3-9360-1b876afa8189": [
                "resources/textures/images/history_record",
                "cc.SpriteFrame",
                1
            ],
            "2a712174-365d-40a5-8c39-85860c809e56": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "2ad8e984-fb49-4a51-ac0f-46bb27251f6e": [
                "resources/textures/images/efx/rain5",
                "cc.SpriteFrame",
                1
            ],
            "2b2161e2-b451-4940-9cea-5b617bd0d91f": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "2b5fe287-d337-41ad-be98-d757ea40791f": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "2b6ea8a8-0ec2-4eb9-8013-1badbd3fbed2": [
                "resources/textures/chat/emoji_action_texture.png",
                "cc.Texture2D"
            ],
            "2bc2dd60-cc80-4ede-a6e1-3bb0a701d56e": [
                "resources/textures/images/createroom/creatroom15.png",
                "cc.Texture2D"
            ],
            "2bc75b7d-2add-4833-b72b-96f1f27391f0": [
                "resources/aqsounds/mj/w_26_2.mp3",
                "cc.AudioClip"
            ],
            "2bd00ccf-20e1-44af-b8d8-94bb85a65b50": [
                "resources/textures/images/playScenesc/sichuan_room_fangxiang_003.png",
                "cc.Texture2D"
            ],
            "2c0b5062-8b59-4c1e-abfb-89b7d6bb517d": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "2c5b589e-8c49-4f61-b48b-b90a655d31af": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "2c9aa949-8b8e-4dc3-a42e-88939ee2e84d": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "2c9b7b23-1d41-4a46-bb3d-59f276d1d239": [
                "resources/textures/images/youqingTip.png",
                "cc.Texture2D"
            ],
            "2cbb5a42-9c2a-4d84-9e29-ba97c0f68f23": [
                "resources/aqsounds/mj/16_1.mp3",
                "cc.AudioClip"
            ],
            "2ccbd5f5-df3b-416e-aaeb-c8e662f8641d": [
                "resources/textures/voice/v_anim3",
                "cc.SpriteFrame",
                1
            ],
            "2cda6a88-6f24-457f-9c16-5044639b5912": [
                "resources/textures/AQRes/CreateRoom/btnOrge.png",
                "cc.Texture2D"
            ],
            "2d0478bd-d02e-408c-9a07-f743376ebdcf": [
                "resources/textures/png/fangkaxiaobeijing.png",
                "cc.Texture2D"
            ],
            "2d167769-a80b-4b3d-89f1-b150fa64008e": [
                "resources/textures/images/PopupScene",
                "cc.SpriteFrame",
                1
            ],
            "2d4ce8ab-e533-478f-bacf-bea0f249cca9": [
                "resources/textures/AQRes/GameOverOps/outmjtile_sign",
                "cc.SpriteFrame",
                1
            ],
            "2d8d85bc-728f-4add-8508-28eb5e2cb56d": [
                "resources/sounds/nv/25.mp3",
                "cc.AudioClip"
            ],
            "2db7b3e5-2efd-409b-b434-17bd3c764929": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "2e3e5873-faef-4a1f-9c02-fcc15cd67c18": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "2e3f21b1-37d3-418a-b3e5-2f4080201d4d": [
                "resources/textures/images/PopupScene/PopupScene2",
                "cc.SpriteFrame",
                1
            ],
            "2e507f7d-3c51-478f-944f-7c96883e666d": [
                "resources/aqsounds/voice/chat_w_16.mp3",
                "cc.AudioClip"
            ],
            "2e777302-cc56-4695-95fc-ea14d45369fd": [
                "resources/aqsounds/mj/xianning.mp3",
                "cc.AudioClip"
            ],
            "2ed991cc-e006-45bd-ae5d-4abab896b0a9": [
                "resources/aqsounds/mj/25_1.mp3",
                "cc.AudioClip"
            ],
            "2ee28dff-bc0a-4535-a492-d13828cd2326": [
                "resources/textures/images/playScenesc/wenzi004.png",
                "cc.Texture2D"
            ],
            "2f2ceb8c-9676-47de-8bdc-aaeec566600c": [
                "resources/textures/images/PopupScene.png",
                "cc.Texture2D"
            ],
            "2f3c2efd-f7e9-405f-949d-9ca561626702": [
                "resources/aqsounds/mj/w_81_2.mp3",
                "cc.AudioClip"
            ],
            "2f63db7a-573e-4821-89b2-35f72edf67f0": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "2fd6b9fd-6fc7-4e08-abc6-8a1f3482d57c": [
                "resources/textures/chat/chat_emoji1.png",
                "cc.Texture2D"
            ],
            "30220141-dcea-4b2c-82ea-f6932b703abd": [
                "resources/textures/images/loading",
                "cc.SpriteFrame",
                1
            ],
            "3040cec3-8b19-4c7a-929c-9bff1cd882ed": [
                "resources/textures/bk/btn_weixin_login",
                "cc.SpriteFrame",
                1
            ],
            "3042e3a3-85e7-4113-ace6-88fe2b819d6a": [
                "resources/textures/AQRes/MJOps/opKan.png",
                "cc.Texture2D"
            ],
            "305ca619-c7e1-4237-b11a-af4b8d70f016": [
                "resources/textures/MJ/right/Z_right2.png",
                "cc.Texture2D"
            ],
            "306a0d16-09f5-4c00-9bdb-0dc53c00990c": [
                "resources/textures/images/PopupScene",
                "cc.SpriteFrame",
                1
            ],
            "308baae5-265e-43a0-b972-edc36c70d76d": [
                "resources/textures/images/JoinRoom/Num7.png",
                "cc.Texture2D"
            ],
            "30951737-7724-48b7-b6d4-39e3d590048b": [
                "resources/textures/AQRes/CreateRoom/btnFree.png",
                "cc.Texture2D"
            ],
            "309678dd-ac02-480c-a9b5-b07c3ae807b0": [
                "resources/textures/AQRes/Hall/bgNum.png",
                "cc.Texture2D"
            ],
            "30a1da14-4bcc-420c-8f6f-2bf454f76f89": [
                "resources/textures/loading/loading_image",
                "cc.SpriteFrame",
                1
            ],
            "30f64244-e7a1-4ec7-a516-e23dd3455aff": [
                "resources/textures/MJ/bottom/B_bamboo_5.png",
                "cc.Texture2D"
            ],
            "312c5295-8210-4203-b06e-c72d765f0928": [
                "resources/textures/images/public_ui",
                "cc.SpriteFrame",
                1
            ],
            "31a0eb37-d5b5-4984-9821-b6d2633a3e61": [
                "resources/textures/images/PopupScene/PopupScene21.png",
                "cc.Texture2D"
            ],
            "31a27cc7-b11c-4c19-b249-e3a0c1770ec4": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "31a389df-2100-4e4b-89b3-eb2607a2b7bc": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "31a3e154-80f7-49c6-93dc-3175b26fa332": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "31aa3b0f-133e-4fe4-9daf-f5c267d8c8d1": [
                "resources/textures/images/createroom",
                "cc.SpriteFrame",
                1
            ],
            "31e651f0-4cf7-4da8-a63c-d112bfb9f786": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "31f437f7-7887-42e3-8ed5-a251209d690c": [
                "resources/textures/images/history_record",
                "cc.SpriteFrame",
                1
            ],
            "3206a1c4-cbc6-4dff-b19b-d145312257e1": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "322ee6ba-7532-47a0-8128-5e63b474c582": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "325e47b0-3fb8-4cc1-b6eb-45891fd122de": [
                "resources/textures/chat/chat_easychat1",
                "cc.SpriteFrame",
                1
            ],
            "329a4adb-a9ca-4d95-9e9b-85475a1d74ab": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "32c8c078-be76-4229-8a1c-b3d986764fb6": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "32edec6d-10ab-4b19-b78c-b07af6a8d309": [
                "resources/textures/images/GameEnd/GameEnd1.png",
                "cc.Texture2D"
            ],
            "32fc197c-7bbc-4591-9625-2baf648da5ef": [
                "resources/textures/images/efx/guafeng7.png",
                "cc.Texture2D"
            ],
            "331ab7b5-5be2-42dd-8ea1-a40274fdfe36": [
                "resources/textures/images/main_scene.png",
                "cc.Texture2D"
            ],
            "332056c9-c90e-4ead-bb17-07e935a35469": [
                "resources/textures/images/JoinRoom/Num14.png",
                "cc.Texture2D"
            ],
            "3329caac-ff7b-4e43-815f-ae3ce3d83c7a": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "3334ab91-5c14-4cb9-ba0a-25efb7c28fd2": [
                "resources/textures/AQRes/MJOpsNew/opHu.png",
                "cc.Texture2D"
            ],
            "336b8ac5-c44d-4419-8e69-0ffb8910eaa0": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "3397af2e-5ee1-4618-8617-928a579a7b2b": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "33a87e63-83e7-4f98-a40e-96feece06697": [
                "resources/textures/images/loading",
                "cc.SpriteFrame",
                1
            ],
            "33dced3a-1554-477f-893f-7b79da7225cb": [
                "resources/aqsounds/mj/w_11_1.mp3",
                "cc.AudioClip"
            ],
            "3418814d-97a1-458e-b336-d1db30a70643": [
                "resources/textures/images/JoinRoom/Num7",
                "cc.SpriteFrame",
                1
            ],
            "343161f7-49fd-4509-8f56-e5fab266f275": [
                "resources/sounds/win.mp3",
                "cc.AudioClip"
            ],
            "347575c5-fc00-4b99-9cca-1c89f8e39415": [
                "resources/textures/MJ/mjEmpty.png",
                "cc.Texture2D"
            ],
            "3479e792-95f8-4d4d-b196-2f8c6a125cd4": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "3484fee3-8d13-4858-9aa5-4eaf5f559510": [
                "resources/sounds/nv/61.mp3",
                "cc.AudioClip"
            ],
            "349b548a-27e6-4a14-b849-98a681d0eb22": [
                "resources/textures/MJRoom/Z_corner_ltop.png",
                "cc.Texture2D"
            ],
            "34ad96a6-6163-47dc-81d5-7ec6d21c4516": [
                "resources/textures/voice/v5.png",
                "cc.Texture2D"
            ],
            "350940fd-9691-4aaf-8685-7764627333ad": [
                "resources/textures/images/PopupScene",
                "cc.SpriteFrame",
                1
            ],
            "35231195-44c0-4a75-8697-0a15a54b7bdd": [
                "resources/textures/images/createroom/creatroom2.png",
                "cc.Texture2D"
            ],
            "35246e42-2c3d-44d1-b9f1-9dd63d38e6ea": [
                "resources/textures/images/history_record.plist",
                "cc.SpriteAtlas"
            ],
            "3541c4d1-54f5-4ffc-a35b-75b83b5f3042": [
                "resources/textures/images/JoinRoom/Num16.png",
                "cc.Texture2D"
            ],
            "35527d11-f0fd-4800-93b4-2749294d67a9": [
                "resources/textures/AQRes/CreateRoom/dirRt.png",
                "cc.Texture2D"
            ],
            "35b3b2d4-3f4d-4b82-98b9-f635952c7fb7": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "35e3e7d7-2bcb-46cf-aa5e-c10fa40eb6d7": [
                "resources/textures/AQRes/CreateRoom/bgInput",
                "cc.SpriteFrame",
                1
            ],
            "360eb57b-9704-40f6-9284-c2593ebbc324": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "36839b7e-2d75-4b5c-9e36-9c4005ac8be0": [
                "resources/textures/images/createroom",
                "cc.SpriteFrame",
                1
            ],
            "3684adf2-1ddb-447f-8e3e-c01d6a5ab7a6": [
                "resources/textures/images/GameEnd/GameEnd21.png",
                "cc.Texture2D"
            ],
            "36947334-3056-435d-8888-387f5fa53d54": [
                "resources/textures/MJRoom/Z_user.png",
                "cc.Texture2D"
            ],
            "36d8be20-9137-46d0-8274-dfe1473f90af": [
                "resources/textures/chat/send_h.png",
                "cc.Texture2D"
            ],
            "370dee27-9ee0-4af9-9c2e-1166320faf77": [
                "resources/textures/loading/dian2.png",
                "cc.Texture2D"
            ],
            "37481f79-c4df-464a-bf6c-a75486171fd6": [
                "resources/textures/MJ/mjEmpty",
                "cc.SpriteFrame",
                1
            ],
            "3757bf86-3fac-4cbc-9556-93e763dd0c1a": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "3776c26b-553b-419d-b952-a2e0db712436": [
                "resources/textures/images/dingque",
                "cc.SpriteFrame",
                1
            ],
            "379b4755-9c85-429f-ae42-86b09f6aed36": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "37a50f60-19a3-48f1-b026-610271dc08c3": [
                "resources/textures/AQRes/CreateRoom/bgRoom.png",
                "cc.Texture2D"
            ],
            "37af60aa-dadb-479c-8d5c-85e0a8e1e669": [
                "resources/textures/images/playScenesc/wenzi001.png",
                "cc.Texture2D"
            ],
            "37baaa16-b7be-4e0e-8c38-d471fd2bf334": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "37d4ee65-bd1a-47ae-be4d-eddc881e8ff0": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "3818ec47-6770-4357-bf71-f319d6ec6bb6": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "3836d8d3-441e-4db9-a771-3d17e1233842": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "3846f030-dc17-4aaf-b4e5-fc68e15115e3": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "38542111-ff3b-49b7-a1d8-05dee1b1da76": [
                "resources/aqsounds/mj/19_2.mp3",
                "cc.AudioClip"
            ],
            "38744dcf-8d01-40b5-868b-e74014d78dcd": [
                "resources/textures/images/PopupScene",
                "cc.SpriteFrame",
                1
            ],
            "3889a464-4b1c-4716-b7af-a4ab6eb88f9a": [
                "resources/aqsounds/mj/w_8_2.mp3",
                "cc.AudioClip"
            ],
            "38a4e733-ec1d-43a7-b0e5-df7e0c9a7ae0": [
                "resources/textures/bk/btn_create_room.png",
                "cc.Texture2D"
            ],
            "38aec0a3-0371-4216-8c40-f817a157cc4b": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "38be1123-65ab-4374-9b79-5ba993c9e089": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "38d62777-d02a-4b17-a3f0-493cc47d334e": [
                "resources/textures/images/efx/rain4",
                "cc.SpriteFrame",
                1
            ],
            "38fecf5f-94eb-4586-aabe-3bc2ae184d0f": [
                "resources/textures/images/playScenesc/sichuan_room_fangxiang_002.png",
                "cc.Texture2D"
            ],
            "3910b006-24a5-4e8b-9c51-4592b88ad2c7": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "393f1987-dda3-4e38-8936-1aa059f8ee7a": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "396d8965-aed9-4e2c-a07e-20df0e103d29": [
                "resources/aqsounds/voice/chat_w_7.mp3",
                "cc.AudioClip"
            ],
            "3970706b-b147-461d-82cb-497fa72c3cef": [
                "resources/textures/images/efx/hu_glow3.png",
                "cc.Texture2D"
            ],
            "397f7028-2225-43bd-9c38-80f857a464cb": [
                "resources/textures/images/public_ui",
                "cc.SpriteFrame",
                1
            ],
            "39b1a5ab-8b10-49f5-9b68-dc69e3fbea1c": [
                "resources/textures/images/JoinRoom/Num3",
                "cc.SpriteFrame",
                1
            ],
            "39b2a4f3-5b5c-491f-8290-1c394298cb30": [
                "resources/aqsounds/mj/w_7_2.mp3",
                "cc.AudioClip"
            ],
            "39c35782-a56c-44a0-8180-f73c78fe44cd": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "39c6e070-9c82-4c49-8cd4-51e9a05d9637": [
                "resources/sounds/nv/18.mp3",
                "cc.AudioClip"
            ],
            "39f0cacf-e4a4-40eb-bffb-ee05ad70b2bb": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "3a146ece-8553-4d46-a086-3c2fa0e56d51": [
                "resources/sounds/guess/bgm.mp3",
                "cc.AudioClip"
            ],
            "3a3a48a1-be45-46c4-b82b-1b5d10a7132e": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "3a4bc43a-320e-496b-84e3-9cd7b192cafa": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "3ae6a92f-00f8-41e2-838c-ba9639c1de78": [
                "resources/aqsounds/voice/chat_w_6.mp3",
                "cc.AudioClip"
            ],
            "3b1098e0-8291-46ce-8a91-1d62d6cce1bc": [
                "resources/textures/voice/v_anim1.png",
                "cc.Texture2D"
            ],
            "3b20ea89-c10e-4437-b1c7-6b7c14e09811": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "3b63b1ac-0e9d-4550-ac53-afb9364490bc": [
                "resources/textures/images/JoinRoom/Num22.png",
                "cc.Texture2D"
            ],
            "3b7ae76c-3b0a-4bc0-a6d6-66b44595c03d": [
                "resources/textures/images/playScenesc/nvxiongmao001",
                "cc.SpriteFrame",
                1
            ],
            "3be0c2da-0b75-4890-8c6e-cd3f3e8ccb46": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "3bfb690f-1785-4d56-8cb9-981449f5abe7": [
                "resources/aqsounds/voice/chat_w_8.mp3",
                "cc.AudioClip"
            ],
            "3c2e7a36-6cdd-44bf-ba08-25ffa70f95ec": [
                "resources/textures/images/playScenesc/wenzi002.png",
                "cc.Texture2D"
            ],
            "3c45ae01-9d4a-4a4b-a37b-cd455578c477": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "3c5b70b1-96cb-4dc2-865e-8a883d2756d6": [
                "resources/textures/AQRes/CreateRoom/btnBrown",
                "cc.SpriteFrame",
                1
            ],
            "3c83a88f-ffc9-4db5-8925-323eba63cccb": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "3c896ebf-5977-4663-a603-5d50428f2373": [
                "resources/textures/AQRes/CreateRoom/bgRoom",
                "cc.SpriteFrame",
                1
            ],
            "3cd441e4-716b-47db-bade-dca42f5a7568": [
                "resources/textures/images/createroom.png",
                "cc.Texture2D"
            ],
            "3d08e1eb-e155-4540-a4cd-8575dddb2be9": [
                "resources/textures/bk/notice",
                "cc.SpriteFrame",
                1
            ],
            "3d241371-739e-4441-963c-b0159a01a19e": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "3d29250c-190a-49f1-9ca5-21b78b2f378a": [
                "resources/textures/chat/input",
                "cc.SpriteFrame",
                1
            ],
            "3da34545-0b07-47f7-a10e-5b149e7b2634": [
                "resources/textures/AQRes/GameOver/bgWinner",
                "cc.SpriteFrame",
                1
            ],
            "3dad14aa-1217-43e2-a654-c22ae3a85d84": [
                "resources/textures/images/loading.plist",
                "cc.SpriteAtlas"
            ],
            "3dae001a-9bfa-4fcc-b018-fb3c74e77eed": [
                "resources/textures/setting/cr_check_bg",
                "cc.SpriteFrame",
                1
            ],
            "3db313d3-a167-451f-8d0a-ab4346493ee9": [
                "resources/textures/images/playScenesc/sichuan_room_xiaoxi_001",
                "cc.SpriteFrame",
                1
            ],
            "3dbce642-676b-4129-a151-ef0011d82d15": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "3dd40bbb-4cfc-430c-8f46-e9ea95313447": [
                "resources/textures/images/GameEnd/GameEnd18",
                "cc.SpriteFrame",
                1
            ],
            "3dfc3ead-35d3-45f5-a6be-32c076d1288a": [
                "resources/textures/images/createroom/creatroom2",
                "cc.SpriteFrame",
                1
            ],
            "3e01c30e-044d-4d20-9cb9-05c97a4bee7a": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "3e01d7fd-8188-4703-b3b8-c08e89aa20b2": [
                "resources/aqsounds/mj/w_26_1.mp3",
                "cc.AudioClip"
            ],
            "3e178941-3ab2-47a1-b601-71189f2f41f6": [
                "resources/textures/images/createroom/creatroom12.png",
                "cc.Texture2D"
            ],
            "3e20ae43-9bcc-4564-afa6-afda96fa6b66": [
                "resources/textures/AQRes/tongIP.png",
                "cc.Texture2D"
            ],
            "3e2ce021-e723-452d-94ed-bb743dfdc7ee": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "3e614f43-4836-446b-81e0-459c0d35a99b": [
                "resources/aqsounds/mj/w_22_1.mp3",
                "cc.AudioClip"
            ],
            "3e741280-da5f-4c3d-b5d8-cb90e474c4a2": [
                "resources/textures/voice/yuyin_bg.png",
                "cc.Texture2D"
            ],
            "3eb6e700-3535-49a7-9513-008c83bf5452": [
                "resources/textures/images/GameEnd/GameEnd22",
                "cc.SpriteFrame",
                1
            ],
            "3edb56bb-012e-4b13-a421-2751d3a2afa5": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "3f67eb2a-f507-45a0-8ec0-9aa037446656": [
                "resources/aqsounds/mj/w_6_2.mp3",
                "cc.AudioClip"
            ],
            "3f995474-491e-4f4e-8644-25d5940be4d3": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "3fa8e33b-2a70-4bfb-8693-b1d1c6fbef6a": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "3fa90332-39d7-4ef0-aa1e-289ba203421e": [
                "resources/textures/MJ/bottom/e_mj_b_up",
                "cc.SpriteFrame",
                1
            ],
            "3fb41aca-8cfd-405b-aa8a-30ad39fd7023": [
                "resources/textures/images/unpack.py",
                "cc.RawAsset"
            ],
            "3fb624b4-ffc4-4452-ac76-7986a733fb5d": [
                "resources/textures/setting/whitebackground",
                "cc.SpriteFrame",
                1
            ],
            "40070852-dace-4de0-976c-f37aa482b996": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "4016d2d2-bcb1-4c8d-a473-42fb7a0b8de0": [
                "resources/textures/images/createroom/creatroom8.png",
                "cc.Texture2D"
            ],
            "401e8fd9-514d-4deb-af53-8b98a8305867": [
                "resources/textures/images/GameEnd/GameEnd9.png",
                "cc.Texture2D"
            ],
            "4039389c-cfbd-43a7-9700-c2ea85b0d4f7": [
                "resources/aqsounds/mj/peng_1.mp3",
                "cc.AudioClip"
            ],
            "40781aa3-2fcd-42d4-832e-e000ce60f51a": [
                "resources/textures/images/playScenesc/sichuantouxiang002.png",
                "cc.Texture2D"
            ],
            "40a7ce28-a17d-4952-8d33-6853ee0d060c": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "40b74c6a-207b-4d26-95ce-f4ad15edf09a": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "410d62af-0c20-4e18-ba68-17cc2c1f798d": [
                "resources/textures/images/playScenesc/sichuan_room_huatong_002",
                "cc.SpriteFrame",
                1
            ],
            "41c5f395-3f6e-4ec1-be02-88f99f4e8376": [
                "resources/sounds/nv/peng.mp3",
                "cc.AudioClip"
            ],
            "41dacf85-2c21-4f6e-af65-70cc37fd3423": [
                "resources/textures/AQRes/MJOpsNew/opGang.png",
                "cc.Texture2D"
            ],
            "41f357e0-872e-4cf4-a4e4-aa44e7a773e2": [
                "resources/sounds/fix_msg_8.mp3",
                "cc.AudioClip"
            ],
            "4201ef1f-4abc-4af0-8e84-e9e776943900": [
                "resources/textures/images/play_scene.plist",
                "cc.SpriteAtlas"
            ],
            "4226dac1-b3ec-43e0-bd22-9fe16f8c4de7": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "422fdf7d-1ed4-43b6-8a3a-ed379c08210d": [
                "resources/textures/images/playScenesc/sichuan_room_fangxiang_002_01",
                "cc.SpriteFrame",
                1
            ],
            "42357971-cd40-4884-aa4a-3fe679d5b921": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "423640c1-da82-43ef-b94c-0ca3a31285bd": [
                "resources/aqsounds/mj/w_5_1.mp3",
                "cc.AudioClip"
            ],
            "4243664a-6b56-4c14-8344-1d264f284d7e": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "428568f5-5ac6-4d74-8c7d-06d5c5008ba2": [
                "resources/textures/AQRes/GameOverOps/txtXJ.png",
                "cc.Texture2D"
            ],
            "42a50b85-90bb-4955-9d63-ede2418818fa": [
                "resources/textures/png/fangkaxiaobeijing",
                "cc.SpriteFrame",
                1
            ],
            "42b93dfd-edd2-4df7-9f64-fd9f2cf8b298": [
                "resources/textures/AQRes/GameOver/imgWin",
                "cc.SpriteFrame",
                1
            ],
            "42c34357-0452-4ce0-95e2-faea1855e74c": [
                "resources/sounds/guess/lose.mp3",
                "cc.AudioClip"
            ],
            "42d1a30b-c8ec-40b2-a673-12f6440ad588": [
                "resources/textures/images/chat",
                "cc.SpriteFrame",
                1
            ],
            "42fff579-3e8b-4b57-babb-7ef5e6a31ba9": [
                "resources/textures/MJ/my/M_character_3",
                "cc.SpriteFrame",
                1
            ],
            "4327d1d7-afcc-49af-b6e0-f0abd482649c": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "4338b286-ca55-4eec-877f-ca22150bc313": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "43554c84-ff61-4bb6-aa29-ed8bbc83b320": [
                "resources/textures/hall/z_datingtouxiang",
                "cc.SpriteFrame",
                1
            ],
            "43b9fcb2-199d-484a-a0ee-ecd177ac8654": [
                "resources/textures/MJ/mjEmpty2",
                "cc.SpriteFrame",
                1
            ],
            "43cf194b-951f-41d2-abf8-4b00596fc99c": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "43d7bab4-1194-49bd-9086-8e194cb0fad7": [
                "resources/textures/images/status/Z_power.png",
                "cc.Texture2D"
            ],
            "44020ba3-52dc-4a39-aeb0-f1e50105ea65": [
                "resources/textures/voice/v_anim1",
                "cc.SpriteFrame",
                1
            ],
            "44129e17-53c1-4d26-9254-bd4a631eaffa": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "4424c05a-7f85-4e5f-8c8c-a8102270bbf1": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "443c94c6-7c19-47d2-afea-91a6c097f7a6": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "4440a060-8fd6-480a-853d-4b7082acb9d6": [
                "resources/aqsounds/mj/w_14_1.mp3",
                "cc.AudioClip"
            ],
            "4470d075-f131-429d-97cf-3c526f50698c": [
                "resources/aqsounds/mj/w_angang_1.mp3",
                "cc.AudioClip"
            ],
            "44bb22d9-c57a-41cc-99f1-6b641fb7457c": [
                "resources/textures/AQRes/logoB.png",
                "cc.Texture2D"
            ],
            "44ec75a5-e20f-47ea-bf4e-da03f9b4c806": [
                "resources/textures/bk/LOGO_mini.png",
                "cc.Texture2D"
            ],
            "45035b82-8e37-4e81-b6bd-850bbcc12bff": [
                "resources/textures/images/GameEnd/GameEnd19.jpg",
                "cc.Texture2D"
            ],
            "453df665-6bdf-4990-a57e-4dd754766c93": [
                "resources/textures/images/dingque.png",
                "cc.Texture2D"
            ],
            "45729989-3a23-48fc-ba5b-d25f6eb83781": [
                "resources/textures/AQRes/Hall/btnChat.png",
                "cc.Texture2D"
            ],
            "4574e3dc-ca58-4a31-b973-9acfe4ef04f0": [
                "resources/textures/images/GameEnd/GameEnd7",
                "cc.SpriteFrame",
                1
            ],
            "45cb0083-6686-499f-9435-59b2e046f27a": [
                "resources/aqsounds/voice/chat_m_1.mp3",
                "cc.AudioClip"
            ],
            "45ea0d44-3b78-4fd1-90d0-9be8bac103e4": [
                "resources/textures/AQRes/CreateRoom/btnGreen",
                "cc.SpriteFrame",
                1
            ],
            "464602ca-c348-455f-9709-8774e50132d4": [
                "resources/textures/images/efx/rain2.png",
                "cc.Texture2D"
            ],
            "467f9022-0098-4c13-81a4-228596dd3b6d": [
                "resources/textures/AQRes/CreateRoom/解散房间",
                "cc.SpriteFrame",
                1
            ],
            "46869ed2-4b12-49c4-b85b-bb6dc91b890e": [
                "resources/aqsounds/mj/w_71_2.mp3",
                "cc.AudioClip"
            ],
            "46d77bdc-5159-4e35-9a23-6710a2d2e1e0": [
                "resources/textures/loading/dian6.png",
                "cc.Texture2D"
            ],
            "46e3d043-e86d-4936-9883-839d124339f1": [
                "resources/aqsounds/mj/25_2.mp3",
                "cc.AudioClip"
            ],
            "46f40ead-3bfb-4769-97db-c3e2da8ad280": [
                "resources/textures/AQRes/CreateRoom/nv1.png",
                "cc.Texture2D"
            ],
            "46f9a5ad-41e5-4428-978b-5918ed32f7ca": [
                "resources/aqsounds/mj/w_25_1.mp3",
                "cc.AudioClip"
            ],
            "4778d46b-cbd3-4306-aacb-61875a5fed9d": [
                "resources/aqsounds/mj/w_11_2.mp3",
                "cc.AudioClip"
            ],
            "477c958b-96bd-4ad6-9492-918003feb0ab": [
                "resources/textures/images/PopupScene/PopupScene22.png",
                "cc.Texture2D"
            ],
            "47c594e7-f9d4-407c-8a53-5baf308ceb2d": [
                "resources/sounds/nv/1.mp3",
                "cc.AudioClip"
            ],
            "47e441c3-81cc-4916-bf1c-fa13f1359c8f": [
                "resources/textures/images/PopupScene",
                "cc.SpriteFrame",
                1
            ],
            "47fbfae0-fa36-477f-877a-99af225cf9fb": [
                "resources/textures/images/efx/peng_glow2.png",
                "cc.Texture2D"
            ],
            "48285984-e828-4033-8a54-dd4e5654f01e": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "4847516a-87d2-4711-b571-a5f0d5c97f54": [
                "resources/textures/AQRes/GameOverOps/txtFQ",
                "cc.SpriteFrame",
                1
            ],
            "48b4c355-d155-4d8f-8e2e-a25ac45dbefe": [
                "resources/textures/images/chat",
                "cc.SpriteFrame",
                1
            ],
            "48bbc34e-d304-4947-a0fc-bba08f27205b": [
                "resources/textures/MJ/right/Z_right.plist",
                "cc.SpriteAtlas"
            ],
            "48c1d635-cb9e-4f2a-96ec-07c30130e107": [
                "resources/textures/MJRoom/Z_user",
                "cc.SpriteFrame",
                1
            ],
            "49023c3c-26b3-42a9-bbf5-061387e6bfaf": [
                "resources/textures/images/createroom/creatroom9.png",
                "cc.Texture2D"
            ],
            "493590e2-08a1-4d4d-bae5-dde74df63fd6": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "49689b80-bfcd-43a7-b095-8c799ae8cc7c": [
                "resources/textures/AQRes/CreateRoom/chkYes.png",
                "cc.Texture2D"
            ],
            "498383c9-347c-434f-84e3-53e4f304d72a": [
                "resources/textures/images/setting",
                "cc.SpriteFrame",
                1
            ],
            "49916d55-e358-4e13-8467-d7d8bfc10127": [
                "resources/textures/AQRes/MJOps/opChi.png",
                "cc.Texture2D"
            ],
            "499cf869-7ba5-42bc-8564-795bbeb49a88": [
                "resources/textures/AQRes/BtnPNG/玩法",
                "cc.SpriteFrame",
                1
            ],
            "49b48549-9276-4962-bce2-5c6733965a79": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "49d66b6a-f5c0-46e1-9410-00b119ee9850": [
                "resources/textures/images/Login/btn_ traveler.png",
                "cc.Texture2D"
            ],
            "4a110901-52ed-4eb3-af08-5341ae4b93cc": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "4a171fbc-4d51-48b9-be4a-face1c24b1ca": [
                "resources/textures/MJ/bottom/B_bamboo_1.png",
                "cc.Texture2D"
            ],
            "4ab32a27-aaeb-4bfa-867f-9752a0a8302d": [
                "resources/aqsounds/mj/28_1.mp3",
                "cc.AudioClip"
            ],
            "4ab37a3f-60f0-44df-a1b5-2ea5e56e4883": [
                "resources/aqsounds/mj/21_2.mp3",
                "cc.AudioClip"
            ],
            "4ac30c8e-023a-4e83-aa95-9022172c9bee": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "4afa1cfd-87fe-40a0-b8d9-0567336c3f98": [
                "resources/textures/AQRes/CreateRoom/返回大厅",
                "cc.SpriteFrame",
                1
            ],
            "4afc8a40-c557-4507-9998-1eecb96c46e6": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "4b40d27e-d45c-42e4-9eb7-d0bc2900de7a": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "4b466703-570f-426e-be96-360627f9391a": [
                "resources/aqsounds/mj/kan_2.mp3",
                "cc.AudioClip"
            ],
            "4b48cbcd-ecdf-418a-83d1-a109e527bcf1": [
                "resources/textures/images/replayBtn",
                "cc.SpriteFrame",
                1
            ],
            "4b4f8910-1f4d-4d2c-a4da-a57921ace189": [
                "resources/textures/loading/dian4",
                "cc.SpriteFrame",
                1
            ],
            "4b82bfd7-c347-48d7-ae61-519081a158b2": [
                "resources/textures/setting/createroom_check.png",
                "cc.Texture2D"
            ],
            "4bcb0c38-9d2e-4701-b877-f7c0df2f4f9f": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "4bd241ee-d125-4bf9-af53-6a5b734a07ae": [
                "resources/textures/images/GameEnd/GameEnd5.png",
                "cc.Texture2D"
            ],
            "4be58b20-1dc8-422a-94b2-b4d3882f7133": [
                "resources/textures/loading/loading_image.png",
                "cc.Texture2D"
            ],
            "4c1e3125-a324-404a-a515-639653fdeb54": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "4c231117-b04c-4ec0-b54a-06028254fe13": [
                "resources/aqsounds/mj/91_2.mp3",
                "cc.AudioClip"
            ],
            "4c2f5efb-7665-403d-ba70-565b194ce592": [
                "resources/textures/AQRes/Hall/txtZhang",
                "cc.SpriteFrame",
                1
            ],
            "4c4a9d3c-37b6-4aa6-b692-8e04c6818225": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "4c5cf5d7-ffd6-4d8d-bcb5-4b469e689850": [
                "resources/textures/bk/btn_return_room.png",
                "cc.Texture2D"
            ],
            "4cd3e754-eed7-4544-adbe-9a1936b7d13a": [
                "resources/textures/images/efx/guafeng2.png",
                "cc.Texture2D"
            ],
            "4ce3b837-de40-4f63-b43b-5171c043cd8b": [
                "resources/textures/AQRes/Hall/desk",
                "cc.SpriteFrame",
                1
            ],
            "4cf4ed66-5b23-4410-ad0e-ddeac313e067": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "4d07ed2e-7095-48cb-b70b-cd3c6f5919af": [
                "resources/textures/chat/input.png",
                "cc.Texture2D"
            ],
            "4d42ddcf-8e6c-4396-bf69-26dd2b288e2b": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "4d53c7ba-0950-4e59-833e-2d2cb816a110": [
                "resources/textures/images/playScenesc/sichuan_room_fangxiang",
                "cc.SpriteFrame",
                1
            ],
            "4d66b0a2-efd4-455a-9854-410ed514f375": [
                "resources/aqsounds/mj/14_2.mp3",
                "cc.AudioClip"
            ],
            "4da718fc-aa53-4be0-a166-f8269b715272": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "4db720d9-b5ed-47db-b457-e7dff65d0c6f": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "4de83bb7-b70d-4463-991d-171370d155ec": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "4dee9526-3350-459c-b2d9-62e637332701": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "4e416ab2-9204-4d4c-ae0d-1fc7a665a541": [
                "resources/sounds/nv/9.mp3",
                "cc.AudioClip"
            ],
            "4ea5ae2f-0f9e-47b6-af10-5a78924840c5": [
                "resources/sounds/nv/17.mp3",
                "cc.AudioClip"
            ],
            "4f53c6ba-8d49-4232-af0f-2276904235a0": [
                "resources/textures/MJ/bottom/Z_bottom2.png",
                "cc.Texture2D"
            ],
            "4f916f6f-c06a-45a8-929e-019c80c3df8e": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "4fc9dd40-3aff-41be-8ed8-c0ec66d3b912": [
                "resources/textures/images/GameEnd/GameEnd2.png",
                "cc.Texture2D"
            ],
            "4fd5b1c7-ef98-4390-9586-4df4c24670b4": [
                "resources/aqsounds/mj/23_1.mp3",
                "cc.AudioClip"
            ],
            "4fda1e95-ce60-43fc-808e-46a9dabc0d48": [
                "resources/textures/images/JoinRoom/Num6.png",
                "cc.Texture2D"
            ],
            "4fe25066-c6a7-472a-b128-1829bc117da0": [
                "resources/textures/AQRes/MJOpsNew/opTongYi",
                "cc.SpriteFrame",
                1
            ],
            "50500b39-b2bb-4c1e-838b-b7874b8370a1": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "5071c6b3-5b8f-4da1-baaf-59d19d6a22b0": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "50bfe9e8-2df2-49fe-9cdf-1412023aa3b6": [
                "resources/textures/MJ/bottom/Z_bottom2.plist",
                "cc.SpriteAtlas"
            ],
            "50e77c44-284c-44b7-8797-2a246a7b60fd": [
                "resources/textures/images/public_ui",
                "cc.SpriteFrame",
                1
            ],
            "50f73b03-a2a5-4455-b19e-83ba562fd173": [
                "resources/textures/images/PopupScene/PopupScene8",
                "cc.SpriteFrame",
                1
            ],
            "50ff2247-c84d-4e18-bba9-59285c32d4bb": [
                "resources/aqsounds/mj/w_9_2.mp3",
                "cc.AudioClip"
            ],
            "511f6292-b652-44ea-9a7f-37e3f501f558": [
                "resources/textures/images/GameEnd/GameEnd11.png",
                "cc.Texture2D"
            ],
            "513f4657-c070-4b27-a694-f424c63e0958": [
                "resources/textures/AQRes/MJOps/opGang",
                "cc.SpriteFrame",
                1
            ],
            "513fe529-e749-4dcb-af5a-344702ea6f05": [
                "resources/textures/images/createroom/creatroom8",
                "cc.SpriteFrame",
                1
            ],
            "514d202c-5468-47b5-a0ff-724b0e6432c4": [
                "resources/aqsounds/mj/27_2.mp3",
                "cc.AudioClip"
            ],
            "515b0125-af3a-4931-aba8-a0cfccea10d8": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "516faa4e-cd1e-4501-b5e3-6f83650549c4": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "51bdb722-b293-4554-951c-9de51650845d": [
                "resources/textures/voice/voice_to_short",
                "cc.SpriteFrame",
                1
            ],
            "51e95a41-1b45-4638-8bf3-65625a7420cc": [
                "resources/textures/images/JoinRoom/Num11",
                "cc.SpriteFrame",
                1
            ],
            "51eafd60-de47-4a80-b2c9-091d824270b8": [
                "resources/textures/voice/v_anim2",
                "cc.SpriteFrame",
                1
            ],
            "521b4858-11da-45d5-95ac-5a9577f59e2c": [
                "resources/textures/MJRoom/Z_power",
                "cc.SpriteFrame",
                1
            ],
            "52a03f52-0d18-4e5c-a87b-f88eb9b01fe3": [
                "resources/textures/png/count_down_num",
                "cc.SpriteFrame",
                1
            ],
            "52a1abf5-8762-4bb7-82dd-c6c7afc88610": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "52a7522a-a70c-48ea-a2ed-f4e08841a8d0": [
                "resources/textures/AQRes/CreateRoom/加入房间",
                "cc.SpriteFrame",
                1
            ],
            "5307d8d6-69aa-4ac7-9b67-8be5d16759e2": [
                "resources/textures/images/JoinRoom/Num23.png",
                "cc.Texture2D"
            ],
            "5322e3e8-b47b-408a-9bf0-23ec6fc1936c": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "535753f9-162b-4819-969d-6726b942ab53": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "5359c4d0-0996-45dc-b404-acf132ba9cf6": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "5368328b-77a9-48e3-9398-ff4265ba65ed": [
                "resources/textures/images/youqingTip",
                "cc.SpriteFrame",
                1
            ],
            "536eece0-8032-4a52-9d48-67ef2866e53d": [
                "resources/aqsounds/mj/chi_2.mp3",
                "cc.AudioClip"
            ],
            "537e486e-8ef6-48ce-8094-32aa50d9410e": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "539804c3-bf8c-41a1-adb8-2e8949a4746e": [
                "resources/sounds/guafeng.mp3",
                "cc.AudioClip"
            ],
            "539d920f-2dd2-40d2-ae1c-41f880f07042": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "53ca47b4-0893-427c-8f32-af077874b300": [
                "resources/textures/chat/chatbg_lt.png",
                "cc.Texture2D"
            ],
            "53dd92a5-1fbd-4640-894d-e0beefe3e5b1": [
                "resources/textures/images/Login/check_mark.png",
                "cc.Texture2D"
            ],
            "53f7829c-0db7-4721-860d-0b5750394369": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "53fe22cf-ae48-47c8-8ce0-3ac782cedcc9": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "54547f8f-c485-467b-b03e-2b5071fbe9f3": [
                "resources/textures/AQRes/MJOpsNew/opGuo.png",
                "cc.Texture2D"
            ],
            "54981393-3c96-41e0-8694-b871cf7564d9": [
                "resources/textures/images/chat",
                "cc.SpriteFrame",
                1
            ],
            "54a6caf2-73d4-4f69-a6dd-507e00f7539f": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "55820244-86f9-49f2-b729-7a988c9789eb": [
                "resources/textures/images/createroom/creatroom11",
                "cc.SpriteFrame",
                1
            ],
            "55830f4d-a21d-4a98-a7c4-a3fb006c3d42": [
                "resources/textures/setting/checkbox_full",
                "cc.SpriteFrame",
                1
            ],
            "55b197ac-35a8-4f86-b416-5e312c7d478f": [
                "resources/textures/images/createroom/creatroom1",
                "cc.SpriteFrame",
                1
            ],
            "562631da-5416-4a01-bd6a-d45f67ecf191": [
                "resources/textures/AQRes/MJOps/opHu.png",
                "cc.Texture2D"
            ],
            "56305015-bf37-42fb-a2a5-37868b8a3c93": [
                "resources/textures/AQRes/CreateRoom/progBG.png",
                "cc.Texture2D"
            ],
            "563ab8c2-4c26-441a-b04b-8ab369d4948d": [
                "resources/textures/AQRes/Hall/userinfo.prefab",
                "cc.Prefab"
            ],
            "563d622c-fc5b-4733-adfe-9c3c3706b09c": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "5673fe3d-3130-4ae0-b7e8-9b1340c25bdf": [
                "resources/textures/images/JoinRoom/Num17.png",
                "cc.Texture2D"
            ],
            "5683f3a3-b0ef-4b92-b5a4-643a3aa10911": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "56958185-085b-40b3-8c94-6e58181b7ca5": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "56a65ee0-9ed8-4a88-a573-0594dcbb16b9": [
                "resources/textures/MJRoom/Z_count_down_num",
                "cc.SpriteFrame",
                1
            ],
            "56be11b4-9c6a-4350-b4ab-d660a127e72e": [
                "resources/textures/hall/fangkaxiaobeijing",
                "cc.SpriteFrame",
                1
            ],
            "56c808ef-299b-4049-b58b-6e0ccc79cd5b": [
                "resources/textures/images/JoinRoom/Num19.png",
                "cc.Texture2D"
            ],
            "56e298df-a360-49c2-90e8-3c8eab80091e": [
                "resources/textures/AQRes/GameOver/lixian.png",
                "cc.Texture2D"
            ],
            "56f0c8d0-5d35-431b-bda9-50a8cac76b1f": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "5703d1da-4911-4396-a82d-b6b5f0f7eab6": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "57090f27-ab96-4149-ab37-477ab09992e2": [
                "resources/aqsounds/mj/w_21_1.mp3",
                "cc.AudioClip"
            ],
            "5734a679-ba6e-4277-a8b6-5659d1470070": [
                "resources/textures/AQRes/MJOpsNew/opBuTongYi.png",
                "cc.Texture2D"
            ],
            "574a6b87-40f5-467f-9bd4-5decd2e86138": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "57bdfc9b-ee84-458d-9b8e-733bc1215511": [
                "resources/textures/voice/cancel",
                "cc.SpriteFrame",
                1
            ],
            "57c08b9b-a3bc-41e7-b4d2-b307d901caa2": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "58006717-8ef1-4cfb-b652-84fe661f34bb": [
                "resources/textures/images/PopupScene/PopupScene15",
                "cc.SpriteFrame",
                1
            ],
            "5814d418-f75c-49fa-b3ff-910c85c9c9ba": [
                "resources/textures/images/dingque",
                "cc.SpriteFrame",
                1
            ],
            "58189fa6-582c-4bc1-8818-d64112bed4e1": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "582ea5e6-d79e-4dea-8c27-aec232630566": [
                "resources/aqsounds/voice/chat_w_13.mp3",
                "cc.AudioClip"
            ],
            "583f998b-da82-4a5f-ab27-12109f495fa6": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "585568a2-a137-440d-930a-c92c3864a9b1": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "58ac7267-1489-431b-b1b4-b27995c63e8a": [
                "resources/textures/voice/adj",
                "cc.SpriteFrame",
                1
            ],
            "58c53b75-bfa3-4b1a-b430-9eda7a788ee7": [
                "resources/textures/images/GameEnd/GameEnd1",
                "cc.SpriteFrame",
                1
            ],
            "58c845f8-00fa-4752-a44b-a105869674fc": [
                "resources/aqsounds/mj/71_2.mp3",
                "cc.AudioClip"
            ],
            "58d6af99-d267-455e-8401-2b77b7454370": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "58f148b2-79ef-4764-8c90-a12d1aefcd82": [
                "resources/textures/images/status/xinhao3.png",
                "cc.Texture2D"
            ],
            "5914f056-b6c1-4bbd-979b-92b5b721c7f4": [
                "resources/textures/images/PopupScene",
                "cc.SpriteFrame",
                1
            ],
            "59409a24-303b-4212-b2c3-d3ae09c28d7f": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "59606dc9-cba7-456d-b160-3ab40f13bc64": [
                "resources/textures/images/GameEnd/GameEnd20.png",
                "cc.Texture2D"
            ],
            "59735cfe-7b45-41af-944b-a86ea7e0f4e1": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "59788439-5d0c-4124-842b-44d03e21f1bd": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "59821a17-576c-458a-9bbc-9930b55e0690": [
                "resources/textures/images/createroom",
                "cc.SpriteFrame",
                1
            ],
            "599ee608-1a08-48ef-b0be-643bce6bcb26": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "59bbd743-7bfd-42bc-84d3-d0bb0898bea5": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "59c0485e-2f80-4164-8aea-3aee2b2e94b6": [
                "resources/textures/images/playScenesc/sichuan_room_fangxiang_001_01.png",
                "cc.Texture2D"
            ],
            "5a4b139c-cf52-4e01-b03c-eea87102060a": [
                "resources/textures/images/replayBtn",
                "cc.SpriteFrame",
                1
            ],
            "5a6264d0-c33b-46d3-b38f-d526335be99f": [
                "resources/textures/images/mahjong_table.jpg",
                "cc.Texture2D"
            ],
            "5a64ca79-d766-45d2-9797-8315e1c79323": [
                "resources/sounds/fix_msg_7.mp3",
                "cc.AudioClip"
            ],
            "5a8ba0b6-f0d2-40c8-a712-8a42f323c602": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "5a8d2b0a-d4c0-4c42-8892-2fc3cb163b17": [
                "resources/textures/images/PopupScene/PopupScene20",
                "cc.SpriteFrame",
                1
            ],
            "5a94418d-0b8c-4a5f-b8d8-c983eb71278f": [
                "resources/textures/AQRes/CreateRoom/退出房间",
                "cc.SpriteFrame",
                1
            ],
            "5a9a8a8d-ac75-4332-9d59-d336f73624ca": [
                "resources/textures/images/PopupScene/PopupScene14",
                "cc.SpriteFrame",
                1
            ],
            "5ab46b86-6642-499f-89ea-b70b7ce6752e": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "5afd072f-8024-4b26-90a2-b80f78b3a05c": [
                "resources/aqsounds/mj/w_xianning.mp3",
                "cc.AudioClip"
            ],
            "5b7ee4c3-c6c1-4578-a34b-f2d1308b39ea": [
                "resources/textures/MJRoom/Z_arrow_frame.png",
                "cc.Texture2D"
            ],
            "5ba85f44-eef1-495c-a091-03ddf1942031": [
                "resources/textures/setting/z_fuxuan_on.png",
                "cc.Texture2D"
            ],
            "5bb86f14-3ed0-4990-af57-270b0bd5678f": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "5bbb5fae-3058-4ae6-8566-2263eecf1305": [
                "resources/textures/setting/titlebg.png",
                "cc.Texture2D"
            ],
            "5bd44f21-8a1b-413d-8d64-08e3c4b67f90": [
                "resources/textures/images/efx/guafeng5",
                "cc.SpriteFrame",
                1
            ],
            "5c16d753-6196-48e1-9f33-bba0dd6ed13d": [
                "resources/textures/images/GameEnd/GameEnd19",
                "cc.SpriteFrame",
                1
            ],
            "5c27ea1c-ead5-4b7a-b7fc-60ed40a3cd94": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "5c283df5-b6fb-4d01-ba2c-5ea3f6853175": [
                "resources/textures/bk/notice.png",
                "cc.Texture2D"
            ],
            "5c2eff74-5168-4659-b83c-d113af210c33": [
                "resources/aqsounds/mj/w_23_1.mp3",
                "cc.AudioClip"
            ],
            "5c43e9f1-79ae-4e95-a7ba-15defc4a9da1": [
                "resources/textures/AQRes/Hall/btnQuit",
                "cc.SpriteFrame",
                1
            ],
            "5c72c12b-d37a-47aa-8a04-faa20f4d8be0": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "5c8f3de3-2dba-4e42-b354-d776cadfa12b": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "5cac63a0-5716-436d-9066-8ade4e05d6a6": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "5cb2b418-e4c5-4d82-af36-d7b61a612d42": [
                "resources/textures/images/createroom/creatroom7.png",
                "cc.Texture2D"
            ],
            "5cdac727-5588-47d7-8230-007a9cb10e77": [
                "resources/textures/voice/voice_to_short.png",
                "cc.Texture2D"
            ],
            "5cdf3758-4cce-4dcf-9321-a257a408269c": [
                "resources/textures/bk/btn_enter_room",
                "cc.SpriteFrame",
                1
            ],
            "5ce27541-1f4b-4455-ace5-40d5963992eb": [
                "resources/textures/AQRes/CreateRoom/bgPanel.png",
                "cc.Texture2D"
            ],
            "5d675ac4-84ed-4cc9-aa3f-1b44b32e07a2": [
                "resources/aqsounds/mj/29_1.mp3",
                "cc.AudioClip"
            ],
            "5dec5a15-1c35-46ca-94a1-df6f0d024e29": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "5df0fb42-67cf-4df8-8ba4-539ef53a3ba7": [
                "resources/textures/images/public_ui",
                "cc.SpriteFrame",
                1
            ],
            "5e05589d-9849-456f-8479-a682a456fe29": [
                "resources/textures/images/playScenesc/sichuan_room_fangxiang_001.png",
                "cc.Texture2D"
            ],
            "5e14cc7f-5ec7-40ca-810e-8c1786eed0f8": [
                "resources/textures/images/public_ui",
                "cc.SpriteFrame",
                1
            ],
            "5e44d54d-7404-43c9-b8cf-43c8296f60da": [
                "resources/aqsounds/mj/angang_2.mp3",
                "cc.AudioClip"
            ],
            "5e5d01f9-5a85-4abc-9330-a89e7f44dbbb": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "5e96f73b-d008-436f-afed-b044c096da1f": [
                "resources/textures/AQRes/GameOverOps/txtBZ.png",
                "cc.Texture2D"
            ],
            "5ea4cbbf-d3ad-469d-8488-e3b5c9e14d66": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "5eabeb79-1ca0-4e66-b570-0c196485aad1": [
                "resources/textures/images/createroom/creatroom16.png",
                "cc.Texture2D"
            ],
            "5ebc8906-0653-4bf2-9325-d4368a3d46db": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "5ed6842a-1d33-46a3-bf05-b8925f427775": [
                "resources/textures/images/JoinRoom/Num9.png",
                "cc.Texture2D"
            ],
            "5ee2a80e-36f3-4b82-8891-e88cadf67ed9": [
                "resources/aqsounds/mj/guan.mp3",
                "cc.AudioClip"
            ],
            "5eec799d-19eb-4553-9382-2ac0e65e63be": [
                "resources/textures/hall/fangkaxiaobeijing.png",
                "cc.Texture2D"
            ],
            "5f2ac589-4aa8-49e4-b05e-250966238126": [
                "resources/textures/images/GameEnd/GameEnd14",
                "cc.SpriteFrame",
                1
            ],
            "5f3270dd-4181-42c6-a8a7-56aad55c509a": [
                "resources/textures/AQRes/MJOps/opGuan",
                "cc.SpriteFrame",
                1
            ],
            "5f7bb741-7c92-4cc9-ad53-0e5b704d6283": [
                "resources/aqsounds/mj/w_27_2.mp3",
                "cc.AudioClip"
            ],
            "5f8094b8-32ac-45b4-93e4-70aa6795365e": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "5faa1f6f-a55e-4e8e-8f04-320fd39f49c6": [
                "resources/aqsounds/mj/w_91_1.mp3",
                "cc.AudioClip"
            ],
            "5fae1714-f8c0-4683-bef2-8ad3dc5d96a8": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "5fb95305-8a53-4075-b8d2-e0d916a1a6a9": [
                "resources/textures/images/PopupScene/PopupScene8.png",
                "cc.Texture2D"
            ],
            "5fbb01e9-eb13-415a-9f30-17a2a8e637f2": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "5fcb8009-ae70-4dcd-aea5-079bc448ac55": [
                "resources/textures/MJ/mjEmpty",
                "cc.SpriteFrame",
                1
            ],
            "604314cf-cc26-4f75-8571-74f21e691d5a": [
                "resources/textures/images/JoinRoom/Num25.png",
                "cc.Texture2D"
            ],
            "6062c9da-a21f-49ce-b7aa-a57031cbd27a": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "606775c9-8727-482f-8013-5ba083282842": [
                "resources/textures/images/playScenesc/sichuan_room_shezhi_001",
                "cc.SpriteFrame",
                1
            ],
            "60a385cf-e2f4-49ae-b1d0-071e77ded151": [
                "resources/textures/images/chat",
                "cc.SpriteFrame",
                1
            ],
            "60b070a2-91ad-45b8-a9bc-6ca678165e8b": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "60c52758-eca1-41a0-97b2-1139dbd508ab": [
                "resources/aqsounds/mj/w_2_2.mp3",
                "cc.AudioClip"
            ],
            "60d599d0-5883-45ea-9b73-23ee5c908ddd": [
                "resources/sounds/nv/28.mp3",
                "cc.AudioClip"
            ],
            "60d88d29-5aa4-436b-bbb0-a58bdf178633": [
                "resources/textures/images/efx/gang_glow2.png",
                "cc.Texture2D"
            ],
            "60fbb8b2-184e-4132-9f18-486f1a7fa39a": [
                "resources/textures/AQRes/CreateRoom/加入房间.png",
                "cc.Texture2D"
            ],
            "6120fc28-be94-496c-9a0b-e636a54ef049": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "6122ca93-d106-4b25-b065-41b544edf7f7": [
                "resources/textures/AQRes/GameOverOps/mkFang",
                "cc.SpriteFrame",
                1
            ],
            "61749169-1f57-43e2-91d0-e473992efe49": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "618fff69-1cf5-40a9-b30f-4280b73cec6b": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "61b4cc23-12d5-45ee-a302-b15d54acaf4c": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "61f45545-f8cb-461e-a7a4-0dc551d1d004": [
                "resources/textures/images/GameEnd/GameEnd4",
                "cc.SpriteFrame",
                1
            ],
            "62176a70-123b-4d93-9811-b2038ecfd192": [
                "resources/textures/AQRes/GameOver/gameEnd",
                "cc.SpriteFrame",
                1
            ],
            "6226f9b7-9e94-4bef-b8be-a40b916a3a8b": [
                "resources/textures/images/PopupScene/PopupScene18.png",
                "cc.Texture2D"
            ],
            "62c609ef-fa1a-4c27-bbb4-622cbe2331f7": [
                "resources/sounds/sort.mp3",
                "cc.AudioClip"
            ],
            "62ce077e-9af1-4617-ab01-50d67a3b457a": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "62ce9bb3-96d1-4a97-8151-c8e97aaad8d9": [
                "resources/aqsounds/mj/angang_1.mp3",
                "cc.AudioClip"
            ],
            "630d3be3-d850-489c-94d3-bc52be73e5aa": [
                "resources/textures/images/playScenesc/wenzi001",
                "cc.SpriteFrame",
                1
            ],
            "630de832-9583-4210-9e7f-e25f6d28fd45": [
                "resources/textures/images/createroom/creatroom18.png",
                "cc.Texture2D"
            ],
            "6312fcc5-5a15-4ed4-87b2-f446eddf0fb1": [
                "resources/textures/MJ/mjEmpty2",
                "cc.SpriteFrame",
                1
            ],
            "63ab6212-db3c-4a56-850b-cb3f970e038c": [
                "resources/textures/images/playScenesc/play_scene_5.png",
                "cc.Texture2D"
            ],
            "63b8692b-d24b-422c-be31-58c680bc5ce2": [
                "resources/aqsounds/mj/w_kan_1.mp3",
                "cc.AudioClip"
            ],
            "63c63e1a-d30c-450a-8795-e60c8408f715": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "63fa6292-3d47-4714-83fc-3cd356f4a9fb": [
                "resources/textures/AQRes/Hall/twater",
                "cc.SpriteFrame",
                1
            ],
            "642e1638-9814-483a-989b-35d15fa2c638": [
                "resources/textures/MJ/mjEmpty2.plist",
                "cc.SpriteAtlas"
            ],
            "6444fd19-c8c7-489e-9f94-6c84563ca6a7": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "646257e5-a754-4f79-ac97-290810c4400f": [
                "resources/textures/AQRes/Hall/btnQuit.png",
                "cc.Texture2D"
            ],
            "64791f91-8f1b-4782-ae02-061bb3f6ec7d": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "6492c6a4-31f6-4832-81dd-1146850fb97e": [
                "resources/sounds/nv/13.mp3",
                "cc.AudioClip"
            ],
            "6493dc20-8fd1-48a3-9d50-8b7b2a360b27": [
                "resources/textures/images/JoinRoom/Num25",
                "cc.SpriteFrame",
                1
            ],
            "64cb6e2b-51b8-4834-a908-ba158a4c2803": [
                "resources/sounds/nv/91.mp3",
                "cc.AudioClip"
            ],
            "64fd300f-ac11-4467-a6f8-91ee90f766ee": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "64fdd981-c297-4460-b3c5-147da72f0ed0": [
                "resources/textures/AQRes/MJOpsNew/opBuRenShu",
                "cc.SpriteFrame",
                1
            ],
            "653c33e2-391d-4d4b-b922-e48e6b9f3a42": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "657b7db6-b17b-44ab-8ad0-83bd3d733a0d": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "65adbe7d-ad01-4e89-9727-5fc0edc24c22": [
                "resources/textures/images/playScenesc/play_scene_4.png",
                "cc.Texture2D"
            ],
            "65f60710-5e7a-420b-8be7-50f81c38f9c6": [
                "resources/textures/images/createroom.plist",
                "cc.SpriteAtlas"
            ],
            "6634918f-b092-4703-838a-487f113a3f57": [
                "resources/aqsounds/mj/w_chi_2.mp3",
                "cc.AudioClip"
            ],
            "6638e2d2-85b9-4412-8af4-ce5ab3301f69": [
                "resources/sounds/horse/bgBet.mp3",
                "cc.AudioClip"
            ],
            "6656bc3b-58ee-4b27-b058-13208d96e148": [
                "resources/textures/images/createroom",
                "cc.SpriteFrame",
                1
            ],
            "667a75f9-c138-42c3-b60b-0442b125545e": [
                "resources/textures/voice/v5",
                "cc.SpriteFrame",
                1
            ],
            "667bb892-2b32-4444-9750-6c7dfa65b275": [
                "resources/textures/images/efx/rain6",
                "cc.SpriteFrame",
                1
            ],
            "66a82289-42b1-4fb6-a223-220e32129cfa": [
                "resources/textures/images/setting",
                "cc.SpriteFrame",
                1
            ],
            "66b308cb-b553-47c2-a12c-21370349a729": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "66cf2369-501c-4107-892e-ec73fe773f59": [
                "resources/aqsounds/mj/28_2.mp3",
                "cc.AudioClip"
            ],
            "67368907-8fe6-493f-be21-083e0ed0306b": [
                "resources/textures/MJ/right/R_character_1",
                "cc.SpriteFrame",
                1
            ],
            "6742aab2-b309-4d71-a3c0-22203cce0bb6": [
                "resources/textures/AQRes/MJOps/opBuZhaoNing.png",
                "cc.Texture2D"
            ],
            "6756aeaf-a941-47e1-b050-40340620e637": [
                "resources/textures/images/chat",
                "cc.SpriteFrame",
                1
            ],
            "675b3e0a-507a-4ea2-a5aa-bc3a2d7fb00a": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "676a445d-bb91-4c54-aa79-f748cd862233": [
                "resources/textures/images/playScenesc/sichuan_room_huatong_001",
                "cc.SpriteFrame",
                1
            ],
            "6772c21d-ce73-439b-8765-1b45509b9d09": [
                "resources/textures/images/JoinRoom/Num18",
                "cc.SpriteFrame",
                1
            ],
            "679538af-5c16-448f-9ab3-e6c44730b19b": [
                "resources/aqsounds/mj/16_2.mp3",
                "cc.AudioClip"
            ],
            "67a4a21e-ebe7-4f16-993d-a8f9c2549e7d": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "67bc1dd1-5aa9-4275-93e8-03da1f0ea322": [
                "resources/textures/AQRes/CreateRoom/btnBlue.png",
                "cc.Texture2D"
            ],
            "67cd47b4-d618-4c22-b67a-cec10bac9e01": [
                "resources/textures/images/createroom/creatroom12",
                "cc.SpriteFrame",
                1
            ],
            "67e7e9b0-c5c7-43b0-a0cf-690104c0465b": [
                "resources/textures/images/playScenesc/nvxiongmao001.png",
                "cc.Texture2D"
            ],
            "67ff2693-da34-4f87-9f97-3d986a209dbe": [
                "resources/sounds/nv/11.mp3",
                "cc.AudioClip"
            ],
            "680b9fb5-f40f-4b5f-8a1f-6dd65a0c5b94": [
                "resources/textures/chat/chat_emoji1",
                "cc.SpriteFrame",
                1
            ],
            "680dba2f-6219-495f-b96a-6b93e5bfc6be": [
                "resources/textures/images/GameEnd/GameEnd15",
                "cc.SpriteFrame",
                1
            ],
            "68127178-10b0-4e9c-82b5-cf5dcf9e789c": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "6829abd2-5b8b-4867-8466-5dcf150c0713": [
                "resources/textures/AQRes/MJOpsNew/opBuZhao.png",
                "cc.Texture2D"
            ],
            "6835e3f3-ed48-43db-b2fb-062450b6f5d3": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "68406f4c-5c30-418d-88d4-da8d5b259fb3": [
                "resources/textures/AQRes/GameOverOps/txtSX3Z.png",
                "cc.Texture2D"
            ],
            "6854b8d9-094d-4d26-afc9-be83f3fd6307": [
                "resources/textures/MJRoom/Z_corner_rtop.png",
                "cc.Texture2D"
            ],
            "686133a8-26bf-4313-af26-5d41ed91057d": [
                "resources/aqsounds/mj/w_28_1.mp3",
                "cc.AudioClip"
            ],
            "68ea7b7d-b08d-4364-912e-64dd29f36786": [
                "resources/textures/images/JoinRoom/Num6",
                "cc.SpriteFrame",
                1
            ],
            "6927a755-077d-4825-a595-3c4c0998d29d": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "693b96ce-a4f6-4ace-a54e-d1cecfb93d6d": [
                "resources/textures/bk/bg2",
                "cc.SpriteFrame",
                1
            ],
            "69891e8b-d860-4e69-84e3-5508c591d72d": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "698f8b0b-3e01-4ee8-aeae-502e71c1d57a": [
                "resources/textures/setting/z_fuxuan_off.png",
                "cc.Texture2D"
            ],
            "69d89393-64d9-47ff-8411-f7147b162086": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "69de2f4f-8180-46b7-8455-eb93ffb620f1": [
                "resources/textures/voice/v3",
                "cc.SpriteFrame",
                1
            ],
            "69e245ba-31d9-4bf5-abfe-76e9132e42cb": [
                "resources/aqsounds/mj/w_ta_2.mp3",
                "cc.AudioClip"
            ],
            "6a0c7016-95be-42a8-8c7c-68447e6e6e31": [
                "resources/aqsounds/mj/26_1.mp3",
                "cc.AudioClip"
            ],
            "6a16e48b-b85c-47eb-a246-dbde91b1ceae": [
                "resources/textures/MJRoom/Z_corner_ltop",
                "cc.SpriteFrame",
                1
            ],
            "6a2399bf-128f-4d7f-93e6-adf7addb7bb6": [
                "resources/textures/AQRes/MJOps/opBuZhaoNing",
                "cc.SpriteFrame",
                1
            ],
            "6aa0aa6a-ebee-4155-a088-a687a6aadec4": [
                "HotUpdate/Texture/HelloWorld.png",
                "cc.Texture2D"
            ],
            "6aaf96bf-145c-422f-9003-6cee3f87088a": [
                "resources/textures/images/JoinRoom/Num3.png",
                "cc.Texture2D"
            ],
            "6aafaf9a-8162-4f19-95c0-15f603b4f76e": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "6ab272cb-1935-48d5-aedb-7534f08b321a": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "6ac36432-8401-455a-9e3b-a2098de789c5": [
                "resources/textures/AQRes/CreateRoom/btnBlue",
                "cc.SpriteFrame",
                1
            ],
            "6b057773-a3c0-43ae-a86c-9a1594204b09": [
                "resources/textures/AQRes/MJOps/opZhaoNing.png",
                "cc.Texture2D"
            ],
            "6b0c4811-b2c6-4992-a357-157372a3a3a6": [
                "resources/sounds/nv/16.mp3",
                "cc.AudioClip"
            ],
            "6b15e8bd-cb47-471b-afae-6d6b572752bf": [
                "resources/textures/MJ/mjEmpty2",
                "cc.SpriteFrame",
                1
            ],
            "6b68a1dc-11c3-4802-be3c-910740a299a1": [
                "resources/textures/images/JoinRoom/Num5.png",
                "cc.Texture2D"
            ],
            "6b74d71f-23b6-4ece-a4ce-904ca7e82a8c": [
                "resources/textures/images/PopupScene/PopupScene7",
                "cc.SpriteFrame",
                1
            ],
            "6be85d5c-ed50-4f26-829e-5193f43208a5": [
                "resources/textures/AQRes/GameOverOps/txtXJ",
                "cc.SpriteFrame",
                1
            ],
            "6c3945e6-edc1-4e80-9fb5-128bd8075733": [
                "resources/textures/AQRes/CreateRoom/btnGreen.png",
                "cc.Texture2D"
            ],
            "6ce8f508-ea3d-4d49-9138-44256a586b22": [
                "resources/aqsounds/mj/hu_2.mp3",
                "cc.AudioClip"
            ],
            "6cfad644-dfdd-40a9-b947-5644eb2906b4": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "6d1cb9a7-7ac5-4c5b-8044-17f5291283dc": [
                "resources/aqsounds/voice/chat_m_11.mp3",
                "cc.AudioClip"
            ],
            "6d468f70-56bb-4170-bffa-f58422df2584": [
                "resources/textures/AQRes/CreateRoom/btnBrown.png",
                "cc.Texture2D"
            ],
            "6d592fac-739f-43fc-b2ad-5a0e22e5e6f7": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "6d5fd117-932e-4145-89c7-a804c8b76658": [
                "resources/textures/AQRes/MJOpsNew/opBuXian",
                "cc.SpriteFrame",
                1
            ],
            "6d707f84-4d45-4735-aca9-2a8e6465c3dd": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "6d733fad-01cd-404f-bc0f-a2e262378be4": [
                "resources/textures/images/history_record",
                "cc.SpriteFrame",
                1
            ],
            "6d985fdd-4d46-4216-8f13-4a214f0c0977": [
                "resources/textures/images/history_record",
                "cc.SpriteFrame",
                1
            ],
            "6dacc7f4-54ea-4c4e-adca-f443d0592835": [
                "resources/aqsounds/voice/chat_m_7.mp3",
                "cc.AudioClip"
            ],
            "6dd4e069-6192-450f-9679-3a4fe0b1f401": [
                "resources/textures/MJ/mjEmpty2",
                "cc.SpriteFrame",
                1
            ],
            "6df48294-00fd-4003-bee3-14447d7cceb0": [
                "resources/textures/images/youqingTip",
                "cc.SpriteFrame",
                1
            ],
            "6e3f06a1-7edb-4a86-bfbf-2cd12d45c41e": [
                "resources/textures/MJRoom/Z_corner_rtop",
                "cc.SpriteFrame",
                1
            ],
            "6e574349-bab4-4e43-b860-8e54dbcbda09": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "6e6ccb6f-b30e-4cb3-9f91-ed6d59001410": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "6e8086d5-006c-4266-b4ca-048ec6b3f9bf": [
                "resources/sounds/lose.mp3",
                "cc.AudioClip"
            ],
            "6e972622-d153-4795-8db0-35f2692d5dc8": [
                "resources/textures/AQRes/GameOver/gameOver",
                "cc.SpriteFrame",
                1
            ],
            "6eb0dfaa-4b83-4727-ba10-27fea19804eb": [
                "resources/textures/AQRes/GameOver/jiesan.png",
                "cc.Texture2D"
            ],
            "6ed16926-a1df-40e2-b4c0-b9c2bf4a2aeb": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "6ef410ef-72e7-4a52-9d9b-1e3a965b607e": [
                "resources/textures/images/PopupScene/PopupScene6",
                "cc.SpriteFrame",
                1
            ],
            "6efe1639-68a2-4217-aea7-e5f28b681039": [
                "resources/aqsounds/mj/w_peng_1.mp3",
                "cc.AudioClip"
            ],
            "6f711013-a943-4391-a5e2-5dc5ee2c43fe": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "6fb8913c-4002-42c2-9c10-e9db64c2b2a3": [
                "resources/textures/images/PopupScene",
                "cc.SpriteFrame",
                1
            ],
            "6fbdf7d9-eeba-4a7d-b7b4-bba89d411657": [
                "resources/textures/images/replayBtn",
                "cc.SpriteFrame",
                1
            ],
            "6fd800e5-bfae-4081-a4ad-e036ab366470": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "70697a08-1f19-452b-a2db-deae8faf4452": [
                "resources/textures/images/JoinRoom/Num11.png",
                "cc.Texture2D"
            ],
            "709647ae-5aab-4247-988b-d07aab8dc2fd": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "70d2aa00-55f9-45fd-9fbf-33fae2a52aac": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "70e14a7e-9eac-46a0-9aab-6e6584e120ab": [
                "resources/textures/images/dingque",
                "cc.SpriteFrame",
                1
            ],
            "70ffb9be-b583-44af-a555-cb85d8390e39": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "711ddd7b-f415-4e37-9482-c5dde1d38521": [
                "resources/aqsounds/mj/11_2.mp3",
                "cc.AudioClip"
            ],
            "71449cab-5635-445f-b311-3cdd42115c62": [
                "resources/textures/images/JoinRoom/Num9",
                "cc.SpriteFrame",
                1
            ],
            "715a9850-adcd-4936-a1f5-a70f7ec47159": [
                "resources/aqsounds/mj/3_2.mp3",
                "cc.AudioClip"
            ],
            "718e98bd-522b-4ff0-b5cd-e955aa0d620b": [
                "resources/textures/images/public_ui",
                "cc.SpriteFrame",
                1
            ],
            "71a22b2b-287f-4480-b4ae-7c745bfa5424": [
                "resources/textures/images/playScenesc/sichuan_room_shezhi_001.png",
                "cc.Texture2D"
            ],
            "71c366f0-00c9-4e34-8b8e-29c1f6a5d93a": [
                "resources/textures/images/mahjong_table",
                "cc.SpriteFrame",
                1
            ],
            "7203af17-c18e-4d2c-a873-deca2120e600": [
                "resources/textures/AQRes/MJOps/opXianNing.png",
                "cc.Texture2D"
            ],
            "721fb52f-dc4f-48e0-b583-43fb72c4b452": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "72385906-a05c-4c15-a964-e874c5ca9088": [
                "resources/sounds/nv/26.mp3",
                "cc.AudioClip"
            ],
            "724439b5-065a-4a3d-9bb4-538eba684186": [
                "resources/aqsounds/mj/w_91_2.mp3",
                "cc.AudioClip"
            ],
            "726d60cb-b817-43a0-89ab-a6ce2e876b06": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "72d783e1-1331-449d-b7c5-4e7fb98520c7": [
                "resources/textures/images/PopupScene/PopupScene15.png",
                "cc.Texture2D"
            ],
            "72f3f982-810b-4ca7-8662-602d2424e179": [
                "resources/textures/images/efx/rain2",
                "cc.SpriteFrame",
                1
            ],
            "73551241-ffce-43ee-9e14-9159c4954e57": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "737ce392-0510-4d29-abc2-788dd4d02bff": [
                "resources/textures/images/efx/zimo_glow2",
                "cc.SpriteFrame",
                1
            ],
            "7394d8f5-2aae-4850-9a3e-e8dc71fad997": [
                "resources/textures/AQRes/MJOpsNew/opGang",
                "cc.SpriteFrame",
                1
            ],
            "739834dc-489b-4290-97ca-158d637f9dd2": [
                "resources/textures/images/replayBtn.png",
                "cc.Texture2D"
            ],
            "73b8f9f6-ac01-4db2-b9f8-25094db2ca37": [
                "resources/textures/images/GameEnd/GameEnd13.png",
                "cc.Texture2D"
            ],
            "73c15f0e-7dc2-4002-b206-11fef2951af3": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "73cc07e5-2ba7-4c9a-a0b9-b7778e2536e9": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "73d4b149-36e7-4f03-b402-977640431a63": [
                "resources/textures/images/createroom/creatroom18",
                "cc.SpriteFrame",
                1
            ],
            "73e08d04-e0d2-4a7a-8acc-82f90d81a373": [
                "resources/aqsounds/mj/w_15_1.mp3",
                "cc.AudioClip"
            ],
            "73e9da26-3bde-42ef-9905-9b080302bf85": [
                "resources/textures/MJRoom/Z_bg_lAr",
                "cc.SpriteFrame",
                1
            ],
            "7411c09d-2f89-4202-9d7a-1101493513d4": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "7448101a-99aa-4163-821d-8e1d74df23bd": [
                "resources/textures/chat/chat_easychat1.png",
                "cc.Texture2D"
            ],
            "74885e9d-0ba7-4c31-ad8a-2f5a2b4fc6e7": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "7491ebaf-d0f3-406e-8ebf-b0b540c62917": [
                "resources/textures/images/createroom/creatroom3.png",
                "cc.Texture2D"
            ],
            "7498f8b2-3d65-47a5-a5dc-3f484a6e5f42": [
                "resources/textures/images/efx/rain1",
                "cc.SpriteFrame",
                1
            ],
            "74b83ad5-4e4d-46db-8222-cc98a66cddef": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "74c13a66-8bf5-4f6e-9650-4e14615242cb": [
                "resources/textures/AQRes/GameOverOps/txtTP",
                "cc.SpriteFrame",
                1
            ],
            "74ce78bd-6b55-482d-8249-a98d8e303198": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "74d44adb-7b0a-4799-95a2-1812ba98e8cf": [
                "resources/textures/images/history_record.png",
                "cc.Texture2D"
            ],
            "752034e8-e2df-4b80-8dd5-e14b719d5e1b": [
                "resources/textures/images/playScenesc/sichuan_room_xiaoxi_002",
                "cc.SpriteFrame",
                1
            ],
            "756d3eda-86e9-4dbe-9b08-4578da7112c8": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "757141a1-2d34-4805-9151-2799f99b6ba8": [
                "resources/textures/chat/chatbg_ld.png",
                "cc.Texture2D"
            ],
            "758a5f69-3874-4dc7-9f99-14e171d7305b": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "75a98499-2f66-4c05-a7c3-6efdbf2becb5": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "75b22bef-022a-48b7-8c28-5a6c6d00016e": [
                "resources/textures/chat/chat_emoji.png",
                "cc.Texture2D"
            ],
            "761858de-4717-457e-aea5-cc8e500b9961": [
                "resources/textures/images/playScenesc/sichuan_room_shezhi_002",
                "cc.SpriteFrame",
                1
            ],
            "7635d991-faef-4770-bd40-3a3eebb3b281": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "763f2268-7cd9-4d6a-9df0-9e65f6b7f5f5": [
                "resources/textures/bk/top_bar.png",
                "cc.Texture2D"
            ],
            "76924c13-b3b9-42c9-a8bd-61f59eaab236": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "76bddb0f-eec5-4801-b527-5df333134dd5": [
                "resources/textures/images/efx/gang_glow",
                "cc.SpriteFrame",
                1
            ],
            "76c50036-e629-41dc-9674-e384be20f9de": [
                "resources/textures/setting/whitebackground.png",
                "cc.Texture2D"
            ],
            "770fb40d-90f1-4f30-bf62-6fb71709822a": [
                "resources/textures/images/createroom/creatroom4",
                "cc.SpriteFrame",
                1
            ],
            "77169e70-22e0-4a25-9fea-a1123dd035c9": [
                "resources/textures/images/playScenesc/sichuan_room_fangxiang_001_01",
                "cc.SpriteFrame",
                1
            ],
            "772eb957-95ef-4401-b6d2-ef125643fbfa": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "774dbbab-fb44-4d2b-b054-a1d7dff16e12": [
                "resources/textures/images/efx/gang_glow.png",
                "cc.Texture2D"
            ],
            "774ff8ea-e351-46b9-b697-8ae47bc191be": [
                "resources/textures/images/JoinRoom/Num13",
                "cc.SpriteFrame",
                1
            ],
            "7772cddc-0884-4e30-bffe-2992ecb56622": [
                "resources/textures/images/PopupScene/PopupScene14.png",
                "cc.Texture2D"
            ],
            "77751ff8-aec3-4571-af26-6c098d3c6b7a": [
                "resources/textures/images/Login/btn_weixin.png",
                "cc.Texture2D"
            ],
            "779b5b85-5444-47b7-aab8-7e1206186bb1": [
                "resources/textures/AQRes/CreateRoom/chkYes",
                "cc.SpriteFrame",
                1
            ],
            "77eda408-adfb-4939-a5ea-00a41b5de2ad": [
                "resources/aqsounds/mj/w_chi_1.mp3",
                "cc.AudioClip"
            ],
            "77f932ed-5482-451a-8919-2adc5aabd943": [
                "resources/textures/voice/recorder.png",
                "cc.Texture2D"
            ],
            "7809a730-a674-4d8c-ab05-31fb28a53ba0": [
                "resources/textures/AQRes/CreateRoom/退出房间.png",
                "cc.Texture2D"
            ],
            "7824e0e7-a8d6-4bed-b56d-de6cede5973b": [
                "resources/textures/MJRoom/Z_arrow",
                "cc.SpriteFrame",
                1
            ],
            "789d279c-3505-47f1-aff4-62d2cd22ad66": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "78d29fec-e016-4e0c-992b-c82383c53be2": [
                "resources/textures/images/playScenesc/sichuan_room_shezhi_002.png",
                "cc.Texture2D"
            ],
            "78e898c8-ab73-4740-912b-74f95a405d93": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "792edbfd-c7c3-4fbc-af11-abc267ab9d7d": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "79a3cddf-5aee-4cf6-b4bf-6a33c1fc99a3": [
                "resources/textures/images/createroom",
                "cc.SpriteFrame",
                1
            ],
            "79a466cc-6693-4d0b-a4a4-bd12161e9012": [
                "resources/textures/AQRes/Hall/desk.png",
                "cc.Texture2D"
            ],
            "79ca1461-bcce-4e92-a148-bbd4884778f1": [
                "resources/textures/images/main_scene.plist",
                "cc.SpriteAtlas"
            ],
            "7a56407b-28b3-447f-9a20-d16c0a7c03a3": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "7a731f97-f192-4f9b-ba2f-a5f7e6fa9b7a": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "7a80f347-8790-49a9-b48c-0538a505ae09": [
                "resources/textures/images/history_record",
                "cc.SpriteFrame",
                1
            ],
            "7aa67722-5e92-4a79-b123-0c1e485229a4": [
                "resources/textures/MJRoom/Z_power.png",
                "cc.Texture2D"
            ],
            "7aaea111-105d-4f25-9a0a-7fdef510bf69": [
                "resources/textures/MJRoom/Z_money_frame.png",
                "cc.Texture2D"
            ],
            "7aafcc6e-6f21-4c99-82b7-52dd5c529bbc": [
                "resources/textures/ops/pai_bottom.png",
                "cc.Texture2D"
            ],
            "7ae049d9-857e-4d3c-8997-fa1fe9b46500": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "7af0a870-0cd1-46c2-9240-1743af231fca": [
                "resources/sounds/nv/12.mp3",
                "cc.AudioClip"
            ],
            "7b06d436-bd43-425d-9e01-87153cb302e3": [
                "resources/textures/voice/yyDialog.png",
                "cc.Texture2D"
            ],
            "7b28fa7e-44ac-4ad4-af05-c55655fc9026": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "7b9b7f4f-c00c-40c8-8a27-4b92d9e20eb6": [
                "resources/textures/chat/chat_emojibg.png",
                "cc.Texture2D"
            ],
            "7ba9a631-88e3-43cb-8408-0814fe2370a5": [
                "resources/textures/images/JoinRoom/Num5",
                "cc.SpriteFrame",
                1
            ],
            "7c138050-9c6a-4d38-a5c2-9728e47bfd88": [
                "resources/textures/voice/v7.png",
                "cc.Texture2D"
            ],
            "7c1e8c3f-c203-44f6-b6d7-00c636842c27": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "7c2ce215-1bd6-499a-a441-a242784bfe85": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "7c4bcc81-506d-4d5d-80b4-b13b9c4694a4": [
                "resources/textures/images/createroom",
                "cc.SpriteFrame",
                1
            ],
            "7c699688-794b-436c-92aa-8044ae3896f0": [
                "resources/textures/images/public_ui",
                "cc.SpriteFrame",
                1
            ],
            "7ce05240-d123-4f98-9258-7c67fee072b5": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "7d3576c9-40cc-4924-b6af-f2d0f611dd6b": [
                "resources/textures/bk/nv",
                "cc.SpriteFrame",
                1
            ],
            "7d6684f7-79e4-42fb-b668-aec8038664f9": [
                "resources/aqsounds/mj/15_2.mp3",
                "cc.AudioClip"
            ],
            "7d691697-7e83-4729-a1b2-8f7ad43304da": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "7d776ae8-340f-4a13-ba9e-8c05afa9ac30": [
                "resources/textures/images/efx/guafeng4",
                "cc.SpriteFrame",
                1
            ],
            "7da91495-b60f-48c9-8633-6d62d7561b31": [
                "resources/textures/loading/dian4.png",
                "cc.Texture2D"
            ],
            "7dacc6e0-2bd8-4e8d-ad65-2f42f7b2c1cf": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "7de01f9c-dfca-4591-8443-e0b8e1c090e4": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "7e35b463-181e-4951-8651-d3ab5a468ea0": [
                "resources/textures/MJ/mjEmpty2",
                "cc.SpriteFrame",
                1
            ],
            "7e4b0329-9455-4d54-8daa-8e36668a4a34": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "7e7a797e-7896-4577-92a9-7c39115c97cd": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "7e8fcd13-f7f8-4ddd-8641-bd69e0c41bcb": [
                "resources/textures/images/PopupScene/PopupScene23.png",
                "cc.Texture2D"
            ],
            "7e91d978-d04d-45da-b0d8-c24810bc0c20": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "7e91eac6-b184-45c2-82c6-92362176638e": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "7ec892c4-3541-4d63-a2de-913c96695f31": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "7ecd578f-9f4e-4b1b-aa7f-ca8ca0e2e7c0": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "7edc1f0f-2db2-45ba-8d93-2215736b67e5": [
                "resources/textures/images/chat",
                "cc.SpriteFrame",
                1
            ],
            "7eecafa6-b14f-4298-a71a-bc2c013f9fba": [
                "resources/textures/ShaiZiAni/ShaiZiClip.anim",
                "cc.AnimationClip"
            ],
            "7f089e3d-0547-4543-9a8e-fdd228ac5d60": [
                "resources/aqsounds/mj/kan_1.mp3",
                "cc.AudioClip"
            ],
            "7f0b519f-f862-43c2-95e9-268aa349af1b": [
                "resources/textures/AQRes/GameOverOps/txtHU.png",
                "cc.Texture2D"
            ],
            "7f224e05-a30c-4e02-afcb-4985914b24c8": [
                "resources/textures/images/createroom",
                "cc.SpriteFrame",
                1
            ],
            "7f59c7ae-665a-4154-8e01-8aca650a6fec": [
                "resources/textures/setting/cr_check_bg.png",
                "cc.Texture2D"
            ],
            "7f5d1ce1-ad62-48c3-891d-2feb0457a744": [
                "resources/textures/images/setting",
                "cc.SpriteFrame",
                1
            ],
            "7f83d309-ba3c-4869-8942-2d727697a3c3": [
                "resources/textures/images/createroom",
                "cc.SpriteFrame",
                1
            ],
            "7f884842-50bd-4266-b8fd-3e45a9c9fae0": [
                "resources/sounds/horse/go.mp3",
                "cc.AudioClip"
            ],
            "7faeeb93-1297-4d49-9a29-82795af3a7e9": [
                "resources/aqsounds/mj/w_4_1.mp3",
                "cc.AudioClip"
            ],
            "7fbf12b6-5e14-4014-9c2c-a5abf220f6e5": [
                "resources/textures/voice/v3.png",
                "cc.Texture2D"
            ],
            "7fd48fa0-f426-4a16-8ffa-35093afdef59": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "7fff0ae8-42af-43c5-b543-1e5ea20bef69": [
                "resources/textures/AQRes/BtnPNG/玩法.png",
                "cc.Texture2D"
            ],
            "80236eb7-5080-4170-96ee-a8c4f8a016cb": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "813d2583-3a1c-4686-8c12-be33c893fe52": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "815524ef-1c74-45c5-9b08-27960b851e85": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "8192a3ff-7ebf-4298-a972-68a6920c2616": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "81ac8927-5852-4c37-ba91-c9faaad18d20": [
                "resources/sounds/bgFight.mp3",
                "cc.AudioClip"
            ],
            "824f364f-6403-4ecb-8049-3cb432746878": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "82b137ab-75f8-44ab-b59a-a3e4082a3b96": [
                "resources/textures/images/GameEnd/GameEnd17.png",
                "cc.Texture2D"
            ],
            "82bc79eb-7392-4632-a970-eb7d087a9b8f": [
                "resources/textures/AQRes/Hall/btnSet",
                "cc.SpriteFrame",
                1
            ],
            "831c7d6a-e72a-4c25-8195-c31be44c5ceb": [
                "resources/textures/ops/penggang_bottom.png",
                "cc.Texture2D"
            ],
            "8350eb62-c416-4e8a-bb91-30762b045da6": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "835db317-51f5-4689-9dc8-71442c726ab4": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "836d2a01-b443-4b1a-92ca-642a907122fb": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "83a3e8f0-e9d7-4eab-a67a-cf9bc5ae69a5": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "84763af1-762f-44f0-9bd4-803a09b047de": [
                "resources/textures/loading/load",
                "cc.SpriteFrame",
                1
            ],
            "8482cbc1-1426-4289-8106-a0b2a0a172e0": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "84b64846-a29a-4dab-a733-b8c5744becd6": [
                "resources/textures/AQRes/CreateRoom/骰子ICON",
                "cc.SpriteFrame",
                1
            ],
            "84f41179-7c37-4487-9225-b872edfe711b": [
                "resources/aqsounds/voice/chat_w_1.mp3",
                "cc.AudioClip"
            ],
            "84f83fa2-dfd8-4aec-84af-13bc721ab8f9": [
                "resources/aqsounds/voice/chat_m_15.mp3",
                "cc.AudioClip"
            ],
            "85210fff-afc9-418a-8b69-e79caf61632f": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "85278faa-c64c-4385-81fc-7f0fb1fc2940": [
                "resources/sounds/nv/19.mp3",
                "cc.AudioClip"
            ],
            "85311d44-7e3a-4406-b1f9-5bf16ba8b210": [
                "resources/textures/AQRes/CreateRoom/bgInput.png",
                "cc.Texture2D"
            ],
            "8532d2cf-5b76-4801-a64f-cf9e12a138f7": [
                "resources/textures/MJ/bottom/B_bamboo_1",
                "cc.SpriteFrame",
                1
            ],
            "859e3989-c7cf-4f33-b66b-396a5788a7ef": [
                "resources/textures/AQRes/MJOps/opBuXianNing.png",
                "cc.Texture2D"
            ],
            "85a8cdbb-b77e-4d81-b843-9852c08c0215": [
                "resources/textures/ops/pai_bottom",
                "cc.SpriteFrame",
                1
            ],
            "85b04990-3bcf-4fa9-a889-20eeb66d7436": [
                "resources/textures/images/JoinRoom/Num21",
                "cc.SpriteFrame",
                1
            ],
            "85ce8829-9c69-425d-84ed-bfdbbe70e6c0": [
                "resources/textures/images/PopupScene/PopupScene21",
                "cc.SpriteFrame",
                1
            ],
            "85db6c99-5092-45a3-a8c0-98fb181aa327": [
                "resources/textures/images/JoinRoom.png",
                "cc.Texture2D"
            ],
            "866ccc82-41fe-4046-9c1e-4466c7933b0c": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "86706b36-dde2-4a75-bef2-4012b7c34bf1": [
                "resources/textures/images/JoinRoom/Num20.png",
                "cc.Texture2D"
            ],
            "867ab745-6488-47a4-81d3-455c2a94ceee": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "86a1f6eb-e8b7-4949-99c3-7b0dc6636b7c": [
                "resources/sounds/fix_msg_5.mp3",
                "cc.AudioClip"
            ],
            "86a5d0fd-9685-43d7-8d52-13c20033db5d": [
                "resources/textures/images/GameEnd/GameEndHu",
                "cc.SpriteFrame",
                1
            ],
            "86bebce9-f6f9-465f-a3d1-392c4d8e7d7e": [
                "resources/textures/images/PopupScene/PopupScene20.png",
                "cc.Texture2D"
            ],
            "86c83b49-ab28-415d-a2e1-7929c62a48f4": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "86e4599b-a34b-42b2-9648-d70e023c27ec": [
                "resources/textures/MJRoom/Z_bg_bottom.png",
                "cc.Texture2D"
            ],
            "870a43c8-fa7e-46f1-999c-3c5fd18d5365": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "870c289f-6778-4a0e-b7a7-788a831eba65": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "870d5fe7-4b6c-4a5a-8fc7-ac162b0892bf": [
                "resources/textures/AQRes/GameOverOps/outmjtile_sign.png",
                "cc.Texture2D"
            ],
            "871643d8-adad-40e5-bfd6-bfc056b315ce": [
                "resources/textures/AQRes/MJOpsNew/opBuRenShu.png",
                "cc.Texture2D"
            ],
            "87349c4b-caa4-428d-ae53-549b97438ce4": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "87679a64-f1ce-4fc5-b9fb-780e770d485d": [
                "resources/textures/bk/btn_weixin_login.png",
                "cc.Texture2D"
            ],
            "876bb2d1-28a8-491e-8770-21cddfe2863a": [
                "resources/sounds/nv/51.mp3",
                "cc.AudioClip"
            ],
            "87b90ce4-92f1-4888-ad52-77f4164d6af0": [
                "resources/textures/images/chat",
                "cc.SpriteFrame",
                1
            ],
            "87b96191-fa48-423f-b671-8b942f81b8b0": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "87cdd984-f534-476b-8234-fe302fe27acf": [
                "resources/textures/images/GameEnd/GameEnd22.png",
                "cc.Texture2D"
            ],
            "882013b2-8c26-441c-894d-d1ccc3fe7138": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "886dc670-2d16-4ddd-9ad5-40bc8f6cecfd": [
                "resources/textures/AQRes/CreateRoom/progBar",
                "cc.SpriteFrame",
                1
            ],
            "88828ec5-c842-43b9-b212-84a48cf424b6": [
                "resources/textures/setting/cr_unused.png",
                "cc.Texture2D"
            ],
            "88cd38c9-aeaf-4be5-9082-4c71590780b3": [
                "resources/aqsounds/mj/w_19_1.mp3",
                "cc.AudioClip"
            ],
            "89250d26-0fc2-447e-bf18-58c7af624cb7": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "893d538d-23fc-4e55-8669-b717cb905636": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "895585dc-65c0-41a6-a6c6-fff6ec95714b": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "8968f007-44a9-4b0d-864a-4c37e2cf285b": [
                "resources/aqsounds/mj/w_29_1.mp3",
                "cc.AudioClip"
            ],
            "8976316c-cae9-43b9-8fa0-36532ccced65": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "89867af9-edba-4bb8-9a4a-fb974d3e8588": [
                "resources/textures/images/playScenesc/play_scene_6.png",
                "cc.Texture2D"
            ],
            "8988418a-46a0-461d-959b-07af93600051": [
                "resources/textures/png/Z_zhunbeizhuangt",
                "cc.SpriteFrame",
                1
            ],
            "89f3e2ac-26c4-4ad2-8039-c54a4aede48f": [
                "resources/textures/images/GameEnd/GameEnd21",
                "cc.SpriteFrame",
                1
            ],
            "89f57315-fad5-4b3f-8ecf-1e932886b163": [
                "resources/sounds/nv/23.mp3",
                "cc.AudioClip"
            ],
            "8a1ef75b-231d-4577-aad3-4ccce7a1cd95": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "8a52eb2f-380a-41b1-aa60-9bd65060d595": [
                "resources/textures/chat/chat_backbg.png",
                "cc.Texture2D"
            ],
            "8a68b8fa-1bcb-4c9a-92cb-bd47c4bd5df8": [
                "resources/textures/chat/chat_easychat",
                "cc.SpriteFrame",
                1
            ],
            "8a7516b1-6171-4406-8413-297aa4123326": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "8aa59a77-ca7f-41f2-8ff5-0978bd1fe4e8": [
                "resources/textures/images/dingque.plist",
                "cc.SpriteAtlas"
            ],
            "8ae39b2a-9963-42f5-8988-e5152803f870": [
                "resources/textures/chat/chat_emojibg",
                "cc.SpriteFrame",
                1
            ],
            "8affa554-e05c-4610-8ff3-a06d4e7e456b": [
                "resources/textures/MJ/bottom/e_mj_b_up.png",
                "cc.Texture2D"
            ],
            "8b018912-da14-4c07-a104-b7f00b41e9a9": [
                "resources/textures/MJRoom/Z_wifi",
                "cc.SpriteFrame",
                1
            ],
            "8b77d34d-496d-4e36-aa08-27096567bf0f": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "8b8f8afd-e7c9-4868-8f3a-c241d463c1c0": [
                "resources/aqsounds/mj/w_1_1.mp3",
                "cc.AudioClip"
            ],
            "8b926c66-642e-4caa-be6e-045b1508e0f3": [
                "resources/textures/AQRes/CreateRoom/btnDBlueB.png",
                "cc.Texture2D"
            ],
            "8bf395a3-1da7-4d31-9483-15efba4e6d51": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "8c082abd-20ad-4fb2-b1e2-4b99e943da66": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "8c0b35ca-019a-46e7-a9b7-b4f37727c640": [
                "resources/textures/images/PopupScene/PopupScene5.png",
                "cc.Texture2D"
            ],
            "8c2e8172-65aa-4d5f-ad62-5383e3f1f939": [
                "resources/textures/images/public_ui",
                "cc.SpriteFrame",
                1
            ],
            "8c3ba05e-1fbf-4af6-934e-40020fa8a371": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "8c5c832c-e19f-4f46-83ed-3c4dd318ec84": [
                "resources/textures/AQRes/GameOver/jiesan",
                "cc.SpriteFrame",
                1
            ],
            "8c76d92e-5904-4628-8f81-7d602f18f8e9": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "8c79a06a-a5d5-49d9-91f4-f77ac0ead2d2": [
                "resources/textures/images/GameEnd/GameEnd14.png",
                "cc.Texture2D"
            ],
            "8cc7b00a-137b-426d-95f1-2e3161c753e0": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "8ccc7c83-6ae1-4396-b287-d7cdf5dd87cc": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "8cf8ce11-2492-4dd7-9b5c-2623a6bf0a04": [
                "resources/aqsounds/mj/w_25_2.mp3",
                "cc.AudioClip"
            ],
            "8d09e976-25a0-4c78-81cd-65fac8505830": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "8d5677bd-d6b5-4b79-ab90-74fe525c4b67": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "8d651a30-6ca2-4201-921d-57cb78876a99": [
                "resources/textures/AQRes/MJOpsNew/opChi",
                "cc.SpriteFrame",
                1
            ],
            "8d68fedf-6682-4a51-8977-b860121d0b71": [
                "resources/textures/chat/chatbg_rd",
                "cc.SpriteFrame",
                1
            ],
            "8d959155-0863-40bf-abbc-f323f6c9c411": [
                "resources/textures/AQRes/MJOpsNew/opTongYi.png",
                "cc.Texture2D"
            ],
            "8d9a2026-aae8-4db2-b6e1-1d89eed524cf": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "8de0606b-04b0-49ed-b7a3-5261b6cedd60": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "8e0d9625-3f6f-46a6-861b-b5301050174c": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "8e32e58c-94c2-4196-8360-83171aa5e873": [
                "resources/textures/AQRes/CreateRoom/nv3",
                "cc.SpriteFrame",
                1
            ],
            "8e76b616-7ec0-4c9e-96e9-0496f443ae13": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "8ee53e49-397a-4974-b996-923c19d3cf0a": [
                "resources/textures/images/setting",
                "cc.SpriteFrame",
                1
            ],
            "8f100c01-ea79-41e2-8aa8-16cf777b64e9": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "8f13c52b-4efa-4357-99d9-0c5e74f9e20a": [
                "resources/aqsounds/mj/w_7_1.mp3",
                "cc.AudioClip"
            ],
            "8f13da7d-738a-457f-8a32-baf66badb27a": [
                "resources/textures/images/PopupScene/PopupScene16.png",
                "cc.Texture2D"
            ],
            "8f57596d-cd4a-480b-bd57-611464f493aa": [
                "resources/textures/images/playScenesc/sichuan_room_fangxiang_002_01.png",
                "cc.Texture2D"
            ],
            "8f63e355-627e-4c36-84dd-582117d7a1f0": [
                "resources/textures/MJRoom/roundnumbg.png",
                "cc.Texture2D"
            ],
            "8f63fcfb-d714-44db-b840-2cde17648738": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "8f65957d-38ab-47c5-8f52-ab42bb7e9592": [
                "resources/sounds/btnClick.mp3",
                "cc.AudioClip"
            ],
            "8f8b031e-48ad-4814-8eb0-0e51c2961831": [
                "resources/textures/images/setting",
                "cc.SpriteFrame",
                1
            ],
            "8f91d550-c7af-4087-99ea-55c5540d109e": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "8fa76a26-7936-4bc0-af9b-c81d5f39cb0b": [
                "resources/textures/images/PopupScene",
                "cc.SpriteFrame",
                1
            ],
            "8fae59f0-c405-46b3-ba6a-803969a1e59e": [
                "resources/aqsounds/mj/ta_2.mp3",
                "cc.AudioClip"
            ],
            "8fd6795f-6493-4b4a-98a7-b2b638fa1b2f": [
                "resources/aqsounds/mj/8_1.mp3",
                "cc.AudioClip"
            ],
            "8fe224e5-640d-4a87-99b6-fa1e261713bf": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "90d60810-f5a0-41ca-a895-f70407c8484c": [
                "resources/aqsounds/voice/chat_m_12.mp3",
                "cc.AudioClip"
            ],
            "90e4b45c-8044-49b6-a842-5bd38dafe909": [
                "resources/textures/images/playScenesc/sichuan_room_fangxiang_004",
                "cc.SpriteFrame",
                1
            ],
            "91580d3f-5abd-4c8b-abfb-a518bf621fee": [
                "resources/aqsounds/mj/w_81_1.mp3",
                "cc.AudioClip"
            ],
            "9184bc96-c6d2-4eeb-83b6-11bf6a4c9dad": [
                "resources/sounds/nv/4.mp3",
                "cc.AudioClip"
            ],
            "922abc6d-8ef3-45e1-8dab-e30cdd5ef110": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "92384832-ba10-4973-92e9-b85f7cf07530": [
                "resources/aqsounds/mj/18_2.mp3",
                "cc.AudioClip"
            ],
            "925193e3-7f5f-4f18-a289-a0a3f0ad9b4d": [
                "resources/textures/AQRes/CreateRoom/返回房间.png",
                "cc.Texture2D"
            ],
            "926fad4d-58f9-4703-9298-300293e852cc": [
                "resources/textures/images/replayBtn",
                "cc.SpriteFrame",
                1
            ],
            "9271fd3d-fc6f-4b2d-a4bd-c1a1b7acbfff": [
                "resources/aqsounds/mj/w_29_2.mp3",
                "cc.AudioClip"
            ],
            "92736375-1111-4844-b612-97f5f47a233c": [
                "resources/sounds/rain.mp3",
                "cc.AudioClip"
            ],
            "928345a5-0da5-4d97-8163-59ad46e44302": [
                "resources/sounds/horse/shutter.mp3",
                "cc.AudioClip"
            ],
            "9283cd92-ac83-457a-b2e0-acaf8c7c46e7": [
                "resources/textures/images/history_record",
                "cc.SpriteFrame",
                1
            ],
            "9289e00c-6c8c-4489-b57d-ee322cacc7d8": [
                "resources/textures/MJ/mjEmpty",
                "cc.SpriteFrame",
                1
            ],
            "935a4e23-06b6-4669-b00f-feaa68eb7f4e": [
                "resources/sounds/nv/3.mp3",
                "cc.AudioClip"
            ],
            "936f6078-06ab-4c8b-8ab7-4c25a0c16917": [
                "resources/textures/MJ/left/Z_left2.png",
                "cc.Texture2D"
            ],
            "93c93965-d3d4-4483-a96e-497e1b243dbc": [
                "resources/textures/images/PopupScene",
                "cc.SpriteFrame",
                1
            ],
            "94195fae-fcaf-440c-95d9-46b8dfa27914": [
                "resources/textures/images/createroom",
                "cc.SpriteFrame",
                1
            ],
            "94407aa6-7af9-4a74-84f2-8e80d38cad41": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "94879ab8-1f28-4dd4-9e75-4cbe27246ccf": [
                "resources/textures/images/replayBtn",
                "cc.SpriteFrame",
                1
            ],
            "9568065c-b8ec-40a9-b65b-40dc1ed8ea43": [
                "resources/textures/images/PopupScene/PopupScene19",
                "cc.SpriteFrame",
                1
            ],
            "95742161-74af-4cad-9b14-9539902da54b": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "959919cd-6b2f-4614-8d35-cd9442e7cb93": [
                "resources/textures/AQRes/CreateRoom/邀请好友",
                "cc.SpriteFrame",
                1
            ],
            "95992f20-92f0-4657-ad0f-bc0cb82a90fb": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "95c6bce6-9751-4396-bb1b-cc65005aa79d": [
                "resources/textures/AQRes/BtnPNG/战绩.png",
                "cc.Texture2D"
            ],
            "95c85b47-abd5-4ff1-bf7f-7ec7cbb6e596": [
                "resources/textures/images/setting",
                "cc.SpriteFrame",
                1
            ],
            "96053a47-4c56-4e4c-b7f6-1d11becf13a6": [
                "resources/textures/MJRoom/roundnumbg",
                "cc.SpriteFrame",
                1
            ],
            "962e3375-0506-4086-aa57-b1a1d5d3bc9c": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "96364271-796e-4c1d-8543-32d35515bc59": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "9639ea2b-ad15-48ac-9335-68c6e2f45330": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "96490529-584d-4ef7-bc25-f52ccf7abc04": [
                "resources/sounds/nv/2.mp3",
                "cc.AudioClip"
            ],
            "96639528-0d53-4f89-978e-d559c3f35c24": [
                "resources/textures/images/efx/guafeng1.png",
                "cc.Texture2D"
            ],
            "96934e02-32aa-4e99-9cbc-de01b7dff81b": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "96f0cabb-87f8-455c-b2aa-710934d4e5b8": [
                "resources/textures/images/JoinRoom/Num14",
                "cc.SpriteFrame",
                1
            ],
            "971ef20e-b148-4ed4-85bf-76b3ead47a4f": [
                "resources/textures/images/PopupScene/PopupScene22",
                "cc.SpriteFrame",
                1
            ],
            "9726de75-0100-4e08-b0ff-e1b563661cc3": [
                "resources/textures/images/efx/hu_glow",
                "cc.SpriteFrame",
                1
            ],
            "974f05c6-b39a-44ce-b352-4a5bbe9cc4a9": [
                "resources/aqsounds/mj/w_28_2.mp3",
                "cc.AudioClip"
            ],
            "97642bab-67c3-4371-aa43-b4c666583d36": [
                "resources/textures/images/GameEnd/GameEnd17",
                "cc.SpriteFrame",
                1
            ],
            "9770bb1e-26a5-4105-b369-25491c6ce586": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "977d5c04-3ba5-4577-a78e-debabf09d9aa": [
                "resources/textures/images/title.png",
                "cc.Texture2D"
            ],
            "97ea7c3b-dee0-4fde-a5c1-e277d0675992": [
                "resources/aqsounds/mj/12_2.mp3",
                "cc.AudioClip"
            ],
            "980b840d-604a-4098-aba3-1dd2edca1a4c": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "9882b86f-9515-49ab-95a6-25f8fe1d8d97": [
                "resources/aqsounds/mj/23_2.mp3",
                "cc.AudioClip"
            ],
            "988addaa-8694-457d-9735-bf937e3e7cc5": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "98f003d9-67d0-4a2e-9c32-eac85db84ea5": [
                "resources/textures/AQRes/MJOpsNew/opKan",
                "cc.SpriteFrame",
                1
            ],
            "9935d202-1938-4b59-98b7-ba8383303d8c": [
                "resources/textures/images/history_record",
                "cc.SpriteFrame",
                1
            ],
            "99590587-bbe2-476a-8f41-0ee43fb6e872": [
                "resources/textures/images/public_ui",
                "cc.SpriteFrame",
                1
            ],
            "9966f61d-2bdf-497a-94d3-e3988487d269": [
                "resources/textures/chat/playerinfoline.png",
                "cc.Texture2D"
            ],
            "99838854-3a79-4687-ac8d-5484f3c4f6aa": [
                "resources/textures/AQRes/CreateRoom/bgPanel",
                "cc.SpriteFrame",
                1
            ],
            "999350d5-eb8e-4a6e-9ac3-f1690a3ea842": [
                "resources/textures/images/PopupScene",
                "cc.SpriteFrame",
                1
            ],
            "99d2b170-2609-4014-b6d9-35262670becd": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "99eaf0ec-4201-4fb5-9edb-389538ff8e43": [
                "resources/textures/images/efx/gang_glow2",
                "cc.SpriteFrame",
                1
            ],
            "99f8c790-c97a-4173-b22c-536b6f728cc3": [
                "resources/textures/images/efx/guafeng5.png",
                "cc.Texture2D"
            ],
            "9a015ff4-f39b-41a5-bc1f-fff72dafcdb6": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "9a3eca63-0e5f-440a-96c6-da5f15f89975": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "9a764540-1212-4f35-a443-8738c6d13e91": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "9adbdc97-5b89-48bc-9769-9c9f54f950ac": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "9b1c5535-dfb0-4b57-9a2a-b82d66a5fbf0": [
                "resources/textures/images/efx/hu_glow4.png",
                "cc.Texture2D"
            ],
            "9b266e8b-c0ce-4fb6-af3a-e38758e00c66": [
                "resources/textures/images/efx/rain4.png",
                "cc.Texture2D"
            ],
            "9b2e97ec-e82c-4602-b9b7-3c073095962d": [
                "resources/textures/images/youqingTip",
                "cc.SpriteFrame",
                1
            ],
            "9b303182-c61c-4e2b-b67a-8c97ffbebbac": [
                "resources/textures/MJRoom/Z_count_down_num.png",
                "cc.Texture2D"
            ],
            "9b326c9e-2120-4262-9131-04cc8d824e24": [
                "resources/textures/MJ/mjEmpty",
                "cc.SpriteFrame",
                1
            ],
            "9b5079b4-d62b-4dcb-95c2-27e71c3d757d": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "9bb8d4cf-dc27-48e6-bdef-26088408c9fb": [
                "resources/textures/images/createroom/creatroom6",
                "cc.SpriteFrame",
                1
            ],
            "9be070de-91ff-4f41-9b82-fd2adeedfd27": [
                "resources/textures/png/money_frame",
                "cc.SpriteFrame",
                1
            ],
            "9beedd3d-cf7f-4cd1-ac78-f7f1f854db89": [
                "resources/aqsounds/mj/w_71_1.mp3",
                "cc.AudioClip"
            ],
            "9c24b693-4be6-4fee-8fec-9614208b37fc": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "9c49f08b-f821-4a09-9f66-c1711f0330e0": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "9c59b35a-d6c9-46a6-b4a6-100ec7979d38": [
                "resources/aqsounds/mj/w_ta_1.mp3",
                "cc.AudioClip"
            ],
            "9c5e7965-202b-4206-a3c2-4b39d7a2b0e1": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "9c68daaa-11ef-4314-8498-68daaa27f551": [
                "resources/aqsounds/mj/13_2.mp3",
                "cc.AudioClip"
            ],
            "9c6916a3-7a73-42b4-87c6-d67475c43f9e": [
                "resources/textures/images/JoinRoom/Num23",
                "cc.SpriteFrame",
                1
            ],
            "9c7a7833-db7e-4439-9434-20901dde5aa8": [
                "resources/textures/loading/dian1.png",
                "cc.Texture2D"
            ],
            "9c8dd523-f857-44ba-ba67-77eb5c258e76": [
                "resources/textures/MJRoom/Z_wifi",
                "cc.SpriteFrame",
                1
            ],
            "9c8e2f45-a85d-4b5a-9347-5868f3e355bc": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "9ca3657f-6216-4cb9-bc0b-f6972a05ae86": [
                "resources/textures/images/PopupScene",
                "cc.SpriteFrame",
                1
            ],
            "9cc2dc4e-e55c-498b-83b1-ce01f3804123": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "9d456e8d-b4a7-492e-bc9a-bd60dff93263": [
                "resources/textures/images/setting",
                "cc.SpriteFrame",
                1
            ],
            "9d47a58c-ee9f-4845-af0d-19981d15a302": [
                "resources/textures/images/playScenesc/sichuan_room_xiaoxi_001.png",
                "cc.Texture2D"
            ],
            "9d6427da-be3c-40e6-adf5-144216d8db41": [
                "resources/textures/voice/v6",
                "cc.SpriteFrame",
                1
            ],
            "9d6ec3cb-3fb3-4c27-b53c-cf5a2a68595c": [
                "resources/textures/AQRes/MJOps/opGuan.png",
                "cc.Texture2D"
            ],
            "9d8e6984-5daf-428b-966a-be82954f6270": [
                "resources/textures/AQRes/BtnPNG/设置.png",
                "cc.Texture2D"
            ],
            "9deb94a9-3861-4d48-b17c-2dc048d22da8": [
                "resources/textures/loading/dian2",
                "cc.SpriteFrame",
                1
            ],
            "9e19ffd6-a16d-4b13-ae6d-88c23b105202": [
                "resources/textures/AQRes/CreateRoom/准备",
                "cc.SpriteFrame",
                1
            ],
            "9e2804f4-de5a-4115-9388-07834b86d64c": [
                "resources/textures/AQRes/GameOverOps/txtBZ",
                "cc.SpriteFrame",
                1
            ],
            "9e65e011-14ff-48cd-935d-4cda9c43c9a3": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "9ea541bf-5c7d-4910-8330-31e7f474c8b0": [
                "resources/textures/chat/chat_chick",
                "cc.SpriteFrame",
                1
            ],
            "9eac6e81-9a8e-40c0-afb6-ccfdeb75362e": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "9ec606b5-3dc6-4119-920e-d34660d0c8d1": [
                "resources/textures/AQRes/MJOpsNew/opWuFenRenShu.png",
                "cc.Texture2D"
            ],
            "9ef235e3-46fd-45ef-8e0a-d6531e7f1ffb": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "9ef3b925-1469-46be-8f24-6f8cf6529777": [
                "resources/textures/MJRoom/Z_arrow.png",
                "cc.Texture2D"
            ],
            "9f1b5def-96ee-451a-93af-0a83f789a689": [
                "resources/textures/images/PopupScene/PopupScene3",
                "cc.SpriteFrame",
                1
            ],
            "9f844e2e-44df-45d4-bc1f-9cdef2d8d1cc": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "9fdbad27-6226-4502-a818-f93f2d543cf8": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "9fde12b5-e35e-4de7-8014-b96690ef1ff0": [
                "resources/textures/bk/LOGO",
                "cc.SpriteFrame",
                1
            ],
            "9fe76483-1a41-4ec8-9779-147f104cf4f7": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "9ff38dc8-149e-464e-b574-d093ce5967d5": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "a046059e-0c94-4865-a6c6-be200f23181f": [
                "resources/textures/MJRoom/Z_help",
                "cc.SpriteFrame",
                1
            ],
            "a0722de0-92ec-4123-b1b6-ecb1f6229cab": [
                "resources/aqsounds/voice/chat_w_5.mp3",
                "cc.AudioClip"
            ],
            "a0a28378-093e-471f-bb55-173c1b062ada": [
                "resources/textures/images/playScenesc/sichuan_room_fangxiang_004_01.png",
                "cc.Texture2D"
            ],
            "a0fc3f61-d6aa-459a-acce-8203cea59a66": [
                "resources/textures/images/efx/guafeng6",
                "cc.SpriteFrame",
                1
            ],
            "a10b6e2b-6769-4441-8b30-8ecb58302982": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "a11fd693-a1f7-451d-af17-afdcfa18ac68": [
                "resources/aqsounds/voice/chat_m_3.mp3",
                "cc.AudioClip"
            ],
            "a13a8bc0-f8dc-4c3f-b37b-edb746c05dae": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "a185106a-459a-4ed0-8884-eea7337bc23f": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "a18925f7-e1ba-4121-8654-7273aaa87412": [
                "resources/textures/bk/bg1.png",
                "cc.Texture2D"
            ],
            "a1aca5ef-b367-4b65-ab8e-e567e039e9c3": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "a1b6f0f2-5105-4910-adac-905068590909": [
                "resources/textures/images/createroom/creatroom6.png",
                "cc.Texture2D"
            ],
            "a1da9ea8-5a78-41f1-8937-253fa6fcb2b5": [
                "resources/textures/voice/v6.png",
                "cc.Texture2D"
            ],
            "a1f4b71a-669b-4ef9-90e3-5d30c8693e91": [
                "resources/aqsounds/mj/14_1.mp3",
                "cc.AudioClip"
            ],
            "a2112db5-77e1-49b8-99f7-c5b76167609c": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "a230e2f8-3203-4f06-8299-6a187f59b5eb": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "a2510fdc-b348-4a38-a5ea-b674d140425b": [
                "resources/aqsounds/voice/chat_m_2.mp3",
                "cc.AudioClip"
            ],
            "a27b9173-533c-4b87-8ac9-87f36df61b8d": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "a3289e63-c09e-4a4c-ad6f-22910a14a40e": [
                "resources/textures/images/playScenesc/sichuan_zhuang.png",
                "cc.Texture2D"
            ],
            "a33ab34b-fe7c-46f0-bbcd-d4b45b82605d": [
                "resources/aqsounds/mj/w_19_2.mp3",
                "cc.AudioClip"
            ],
            "a3b4e04d-1fbb-487e-9a70-18596c2a2239": [
                "resources/textures/images/GameEnd/GameEnd13",
                "cc.SpriteFrame",
                1
            ],
            "a40bf275-43ad-4948-9509-695448b0f94b": [
                "resources/textures/AQRes/MJOps/opKan",
                "cc.SpriteFrame",
                1
            ],
            "a41e1abe-f2b0-4973-8b7f-8e2e32474304": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "a42cfa3a-43b6-49f1-be9b-8839596e9976": [
                "resources/textures/chat/send",
                "cc.SpriteFrame",
                1
            ],
            "a45ebdea-ea32-4df4-800a-c0ce41779a36": [
                "resources/textures/MJRoom/Z_corner_lbottom.png",
                "cc.Texture2D"
            ],
            "a4637acb-7b61-4062-a90d-002c3db8e48a": [
                "resources/textures/voice/v_anim2.png",
                "cc.Texture2D"
            ],
            "a473ce24-d8a4-4174-90d2-619d254237db": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "a492da20-25c2-4a3b-aef5-cf34259ed405": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "a4ae414c-de85-4cce-a7b2-cce89a7b764c": [
                "resources/textures/AQRes/Hall/bgNum",
                "cc.SpriteFrame",
                1
            ],
            "a4d2bd31-3ef7-4642-9c10-4b63314a825c": [
                "resources/aqsounds/mj/17_1.mp3",
                "cc.AudioClip"
            ],
            "a4edf10a-382b-48b4-a259-90ba750f6f59": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "a5490aed-a2bf-48f8-bfdf-5f48327a061f": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "a5c2bb3a-45eb-41d7-ac46-f2823c669104": [
                "resources/textures/MJRoom/Z_wifi",
                "cc.SpriteFrame",
                1
            ],
            "a5ec6867-36f2-406c-b963-6c4f73e7258b": [
                "resources/sounds/nv/5.mp3",
                "cc.AudioClip"
            ],
            "a694183b-8e31-401e-a13f-692f22e1bea6": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "a6e70db1-0583-49ee-98c3-0a93bba8726c": [
                "resources/textures/MJRoom/Z_arrow_top.png",
                "cc.Texture2D"
            ],
            "a71be873-76dd-46f4-8bea-8418aac6ecb3": [
                "resources/textures/images/efx/rain5.png",
                "cc.Texture2D"
            ],
            "a71de07d-edc1-4176-96cc-2ddb445b522d": [
                "resources/textures/chat/send.png",
                "cc.Texture2D"
            ],
            "a7b08c37-ea5d-4c04-85ae-ec901991e574": [
                "resources/textures/images/playScenesc/sichuan_zhuang",
                "cc.SpriteFrame",
                1
            ],
            "a7b39b8e-43c6-476f-9581-8ee2eb9562c8": [
                "resources/textures/loading/dian6",
                "cc.SpriteFrame",
                1
            ],
            "a7f715ba-5366-4101-9dd3-32546913101e": [
                "resources/textures/images/setting",
                "cc.SpriteFrame",
                1
            ],
            "a8027877-d8d6-4645-97a0-52d4a0123dba": [
                "HotUpdate/Texture/singleColor.png",
                "cc.Texture2D"
            ],
            "a856e807-c132-4e91-bda1-b95a0593291b": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "a87ec412-9067-4265-a7f4-a8054f7a1c46": [
                "resources/textures/images/playScenesc/play_scene_5",
                "cc.SpriteFrame",
                1
            ],
            "a8880732-446c-4797-a83e-7b1376458309": [
                "resources/textures/images/PopupScene",
                "cc.SpriteFrame",
                1
            ],
            "a8b8cff8-f7ee-405a-905d-00ee80a86c86": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "a8be677d-5057-42f3-9c45-ef5b5595b5f5": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "a8c59850-66cd-4c18-9c2b-d72c4409bc88": [
                "resources/aqsounds/mj/3_1.mp3",
                "cc.AudioClip"
            ],
            "a8cce54f-961c-49b5-b3e1-4d293547f05b": [
                "resources/textures/images/play_scene.png",
                "cc.Texture2D"
            ],
            "a8d3e765-2db3-4289-9245-411651957e6a": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "a92c6a26-e25e-44d5-bdf5-e71dadf6639d": [
                "resources/sounds/nv/chi.mp3",
                "cc.AudioClip"
            ],
            "a94a0535-6451-4f94-a97f-27dcf86434d9": [
                "resources/aqsounds/mj/1_2.mp3",
                "cc.AudioClip"
            ],
            "a9cb833c-608a-4cd0-8cb1-ae3604023ce9": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "a9e537bc-83b7-404a-a7e9-874ee9049a5c": [
                "resources/aqsounds/mj/21_1.mp3",
                "cc.AudioClip"
            ],
            "aa02e7a4-30b1-4dab-a152-6ce669de7472": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "aa33de03-9b55-459d-b5b0-8e7c6fb20b00": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "aa52ef21-75a9-45ac-9725-afbf473df0b7": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "aa56f9b7-3cd1-44db-ab41-733ad9fcb235": [
                "resources/sounds/timeup_alarm.mp3",
                "cc.AudioClip"
            ],
            "aa589d8c-34bf-4338-b3f4-69613944f82c": [
                "HotUpdate/Texture/icon_back.png",
                "cc.Texture2D"
            ],
            "aa98f5fe-95cd-4633-8e11-c40deb30b72a": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "aa9cd25a-64c9-441b-9d24-0c27463ef557": [
                "resources/sounds/fix_msg_4.mp3",
                "cc.AudioClip"
            ],
            "ab2500fa-9ff8-4489-a1c7-5febd50136b7": [
                "resources/textures/MJ/my/Z_my.plist",
                "cc.SpriteAtlas"
            ],
            "aba10ad2-fc76-482a-9ff6-beae79b7b3c6": [
                "resources/textures/AQRes/MJOpsNew/opWuFenRenShu",
                "cc.SpriteFrame",
                1
            ],
            "aba9e519-81c2-4efe-9468-27e784ec0196": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "abb55ec9-187f-436e-9a1a-5ba431862e87": [
                "resources/textures/images/PopupScene",
                "cc.SpriteFrame",
                1
            ],
            "abd1e9cd-4685-4ddb-8fa3-0582e76e8669": [
                "resources/textures/MJ/mjEmpty2",
                "cc.SpriteFrame",
                1
            ],
            "abe298a3-71f9-4e37-908d-007aa727983c": [
                "resources/textures/images/history_record",
                "cc.SpriteFrame",
                1
            ],
            "abf21e13-82d4-4347-b135-b547dfbbe254": [
                "resources/textures/images/createroom",
                "cc.SpriteFrame",
                1
            ],
            "abf401f5-7550-4b15-aee5-070b45601953": [
                "resources/textures/AQRes/CreateRoom/个人信息.png",
                "cc.Texture2D"
            ],
            "ac2f73dd-07af-497f-9af1-a6c12260d106": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "ac582907-0ed7-4ad1-99d2-e4ae45d24ddf": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "ac66e690-86e7-4aee-9368-fa9e02643e0b": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "ac8be0c2-1c7f-4675-b462-dd8f52283017": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "ace46e09-37ab-4607-9282-99981f565cef": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "ad225319-5fc7-4545-ab4d-1d41ac597355": [
                "resources/aqsounds/voice/chat_m_8.mp3",
                "cc.AudioClip"
            ],
            "ad52929b-126b-4169-bdbc-540d71f4d1f5": [
                "resources/aqsounds/mj/13_1.mp3",
                "cc.AudioClip"
            ],
            "ad5300c1-35eb-448b-b629-095cf4e7411d": [
                "resources/sounds/fix_msg_1.mp3",
                "cc.AudioClip"
            ],
            "ad87660e-cf3d-4c4a-9058-de8d22700d61": [
                "resources/textures/setting/checkbox_void.png",
                "cc.Texture2D"
            ],
            "ade38b7c-3887-4e67-8e39-c4f8c36799dd": [
                "resources/textures/AQRes/MJOps/opXianNing",
                "cc.SpriteFrame",
                1
            ],
            "ae309bb9-e0aa-4905-8618-b03c8f21401e": [
                "resources/aqsounds/mj/7_2.mp3",
                "cc.AudioClip"
            ],
            "ae66c8ea-bf8d-4490-805d-e9d07577e36b": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "aeae578b-3e35-42a8-aa3f-1bcae5b1a84b": [
                "resources/textures/MJRoom/Z_nobody",
                "cc.SpriteFrame",
                1
            ],
            "aec3e5d6-af97-4fb9-ac3f-78bc5f919425": [
                "resources/sounds/nv/71.mp3",
                "cc.AudioClip"
            ],
            "aefc038c-cae0-4fdd-acd7-eb5b79e8ee19": [
                "resources/textures/AQRes/distance.png",
                "cc.Texture2D"
            ],
            "af18652e-cd2e-411f-bfc0-cb7496293145": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "af1c4afa-6fe9-433e-a9eb-5fb2e7086fd6": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "af421aaf-6a5b-4663-a033-8ed750ca5a54": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "af4c27fb-c781-4f56-9006-4b78f7749122": [
                "resources/textures/images/createroom/creatroom10",
                "cc.SpriteFrame",
                1
            ],
            "af8cfd9f-af40-4d94-b62d-e0add2caf196": [
                "resources/aqsounds/mj/w_13_1.mp3",
                "cc.AudioClip"
            ],
            "af984eb9-3ddc-4c57-aacd-82887f7a19ee": [
                "resources/textures/images/chat",
                "cc.SpriteFrame",
                1
            ],
            "afc96215-f657-4e45-b5e6-ff5152f52566": [
                "resources/textures/images/public_ui",
                "cc.SpriteFrame",
                1
            ],
            "afeef784-06d3-469a-8785-280c82a91ab9": [
                "resources/textures/images/JoinRoom/Num4.png",
                "cc.Texture2D"
            ],
            "affa8f85-a84c-4118-b555-3675656670f4": [
                "resources/textures/AQRes/BtnPNG/战绩",
                "cc.SpriteFrame",
                1
            ],
            "b003010f-fc3a-40ff-b15a-7aae7419e759": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "b03dc8dc-0c47-41b0-96e5-e638457d4fa4": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "b043e12f-af52-49e5-ab0e-17f848fbd3ec": [
                "resources/textures/images/createroom",
                "cc.SpriteFrame",
                1
            ],
            "b050ef26-485f-4db6-95f0-34d0555a6c42": [
                "resources/textures/images/JoinRoom/Num12",
                "cc.SpriteFrame",
                1
            ],
            "b0737cf8-25e7-4122-a577-66181b3ffbb8": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "b07f7813-72f2-4129-b5ae-62c1f4d42157": [
                "resources/aqsounds/mj/w_22_2.mp3",
                "cc.AudioClip"
            ],
            "b08518a0-b1fe-4f1c-b2b8-b18bdef4a630": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "b092af06-2d62-4a4f-ae7c-103926cb8a53": [
                "resources/textures/images/GameEnd/GameEnd20",
                "cc.SpriteFrame",
                1
            ],
            "b0ba2ebe-a39f-473f-8de1-4b49c8690940": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "b0c6c00a-ab31-43b6-89c6-cdbfbe8fca16": [
                "resources/textures/AQRes/CreateRoom/nv2",
                "cc.SpriteFrame",
                1
            ],
            "b1671f1a-3797-4611-a461-430f8836ecc4": [
                "resources/textures/voice/v1",
                "cc.SpriteFrame",
                1
            ],
            "b1851886-6654-48f8-bbda-22f762d3e3b3": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "b191c7fe-e16c-496a-94df-6cfd5865f8f2": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "b1b5f887-6e69-4701-b38c-b25252c3a569": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "b20407c1-9ea9-4562-ab05-16bdf4016020": [
                "HotUpdate/Texture/B131.png",
                "cc.Texture2D"
            ],
            "b23a8d60-d314-40c1-9abb-424a335ddc26": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "b23dea61-eec0-4d8b-8397-5148277e21ba": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "b24fd999-39a2-4837-8d99-194f958d36a4": [
                "resources/textures/AQRes/Hall/btnChat",
                "cc.SpriteFrame",
                1
            ],
            "b270df8d-9c0e-4357-8a00-4e94df03b82e": [
                "resources/aqsounds/mj/9_2.mp3",
                "cc.AudioClip"
            ],
            "b279ae58-afa2-4643-9f89-11feb92e1bba": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "b29f3e30-8301-4ea0-853b-400898348d7d": [
                "resources/aqsounds/mj/w_12_1.mp3",
                "cc.AudioClip"
            ],
            "b2a05403-3e08-4089-852f-d5b90261ade2": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "b2bde301-3bb6-4d07-a5b0-99cafcbec717": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "b2c1781b-17c0-4385-ae58-5ca7a67b741d": [
                "resources/textures/MJRoom/Z_arrow_top",
                "cc.SpriteFrame",
                1
            ],
            "b2dbb3a6-5177-40b4-8c24-ec2aabe2c4cf": [
                "resources/textures/images/playScenesc/wenzi005.png",
                "cc.Texture2D"
            ],
            "b2ef90d0-53d4-40dc-85e8-3657ec41e761": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "b2f29176-11c6-44dd-8c7a-0638772e4ead": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "b30dea44-8f96-492e-b068-3d78fc87768a": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "b3148a66-4df1-4a54-b989-4961fbf9e95c": [
                "resources/textures/AQRes/MJOpsNew/opChi.png",
                "cc.Texture2D"
            ],
            "b336e47e-89fc-40d9-9048-d4bfa232739c": [
                "resources/textures/images/dingque",
                "cc.SpriteFrame",
                1
            ],
            "b35a9d33-5790-459f-950c-eee12b27056c": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "b3daa091-3e8e-4358-8255-7543c401efd7": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "b3e784f7-26e9-4729-8264-e675b6bd8be1": [
                "resources/textures/AQRes/GameOverOps/txtXP",
                "cc.SpriteFrame",
                1
            ],
            "b412c14f-56c7-4725-8896-d342425609bd": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "b421470e-abca-4b84-9e4c-eab8b6b7806e": [
                "resources/textures/images/PopupScene",
                "cc.SpriteFrame",
                1
            ],
            "b43b1e6b-9b2e-4ca7-9a02-4e23633fb112": [
                "resources/textures/MJRoom/Z_offline.png",
                "cc.Texture2D"
            ],
            "b44e445a-e2fa-4326-9080-22ffacd9208e": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "b460ae13-6304-4916-a065-092f5544ae00": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "b46b6ea1-67da-4949-a2af-45e76463796c": [
                "resources/textures/images/dingque",
                "cc.SpriteFrame",
                1
            ],
            "b4b0b6da-6023-40a4-8f51-6892e0c00f8f": [
                "resources/textures/images/PopupScene/PopupScene17",
                "cc.SpriteFrame",
                1
            ],
            "b4cd8eb2-b901-43e6-85c2-8791641cecaa": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "b4eec295-9f2a-4cea-8d9d-b64f058890dd": [
                "resources/aqsounds/voice/chat_w_10.mp3",
                "cc.AudioClip"
            ],
            "b526316a-0879-4b6e-8b03-63cf91505eff": [
                "resources/textures/images/PopupScene",
                "cc.SpriteFrame",
                1
            ],
            "b562182c-43a0-47d1-ab16-87d7e15a1e86": [
                "resources/aqsounds/mj/9_1.mp3",
                "cc.AudioClip"
            ],
            "b5a659d5-eae2-4021-b6bf-dae425caa639": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "b686c99a-593f-447a-8a21-d096c0fc4da0": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "b687ff8b-33dc-4229-9c6b-bd4d70d9324f": [
                "resources/textures/images/JoinRoom/Num2.png",
                "cc.Texture2D"
            ],
            "b6db1388-a08c-4034-b44a-79ed091917ed": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "b71386fb-c7cd-4a3c-ac2b-0ac0f43e8d73": [
                "resources/sounds/deal.mp3",
                "cc.AudioClip"
            ],
            "b72b5f69-e7e2-4eb0-ae43-d668c5b4609a": [
                "resources/textures/images/GameEnd/GameEnd2",
                "cc.SpriteFrame",
                1
            ],
            "b73cb81c-0417-4ba2-8035-46a92b8a4528": [
                "resources/textures/AQRes/GameOverOps/mkZhuang.png",
                "cc.Texture2D"
            ],
            "b7b0dff4-defb-47eb-90fe-b8b232c946a0": [
                "resources/textures/images/createroom/creatroom7",
                "cc.SpriteFrame",
                1
            ],
            "b7e69259-3eee-47fc-b7a6-fb30b3534d6c": [
                "resources/aqsounds/mj/27_1.mp3",
                "cc.AudioClip"
            ],
            "b824bcbe-b02d-4032-9eee-253b4368c088": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "b831321f-46b0-4ee9-bf2b-0b484205b789": [
                "resources/textures/MJ/mjEmpty2.png",
                "cc.Texture2D"
            ],
            "b83a3ad2-17dd-4c8f-9c20-30dba9f80f99": [
                "resources/textures/images/Login/yonghuxieyi",
                "cc.SpriteFrame",
                1
            ],
            "b85a52c9-fae9-4d2f-ac10-238071cc869f": [
                "resources/textures/setting/createroom_check",
                "cc.SpriteFrame",
                1
            ],
            "b874b9e3-55b7-46b5-911e-46d8e00ae34d": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "b88dd24d-1603-464f-994e-1924e26fe26b": [
                "resources/textures/images/efx/hu_glow.png",
                "cc.Texture2D"
            ],
            "b8bed57c-7c55-41cc-8f87-b746d4bf0c73": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "b8d90545-c2e4-408b-891b-bc8a6f2f72ac": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "b907c6e6-3d07-4f19-bda2-e06dc9bb0502": [
                "resources/sounds/nv/24.mp3",
                "cc.AudioClip"
            ],
            "b98dbea2-b71d-47ff-a447-8b94caef4c0f": [
                "resources/textures/images/GameEnd/GameEnd16.png",
                "cc.Texture2D"
            ],
            "b9abadb1-b872-4b4e-b997-81d092746c94": [
                "resources/textures/MJ/mjEmpty",
                "cc.SpriteFrame",
                1
            ],
            "b9e6033e-3d63-439a-bbcb-ef77a7574dcb": [
                "resources/textures/images/history_record",
                "cc.SpriteFrame",
                1
            ],
            "b9febab6-da0f-4f9f-a65c-6db217d7acd1": [
                "resources/aqsounds/mj/22_2.mp3",
                "cc.AudioClip"
            ],
            "ba0c28cf-ca39-44a5-bccc-e27c418e829c": [
                "resources/textures/chat/chat_chick.png",
                "cc.Texture2D"
            ],
            "ba272c1b-eb47-4939-9878-2971c91fa764": [
                "resources/textures/images/GameEnd/GameEnd10",
                "cc.SpriteFrame",
                1
            ],
            "ba318dd9-5759-42a3-9d92-dd5765808651": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "ba3a81e5-84cd-4cb5-a01d-7bf5adcb1d40": [
                "resources/aqsounds/mj/2_2.mp3",
                "cc.AudioClip"
            ],
            "bac6f5f0-d06b-41be-8122-b47ea55c8788": [
                "resources/textures/MJ/mjEmpty",
                "cc.SpriteFrame",
                1
            ],
            "badbe0da-23f0-47fb-b448-276436c06588": [
                "resources/aqsounds/voice/chat_w_3.mp3",
                "cc.AudioClip"
            ],
            "bb1116c6-b418-4c7c-9f65-873d0e7bbceb": [
                "resources/textures/images/createroom/creatroom1.png",
                "cc.Texture2D"
            ],
            "bb32a3d6-8237-4434-b9b2-286b9cc07dc1": [
                "resources/textures/voice/v4",
                "cc.SpriteFrame",
                1
            ],
            "bb5e640f-7f9f-48c5-8695-deb1e57f0a91": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "bb6c6ad0-5e36-473d-a41f-370dfcdfcce5": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "bb9fa226-e1aa-40f9-955d-14c6c361d7be": [
                "resources/textures/loading/dian1",
                "cc.SpriteFrame",
                1
            ],
            "bbb5fe8c-0f31-41f3-8774-635da092fbff": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "bc230405-a547-46fd-8382-a88fe1de1a91": [
                "resources/textures/images/createroom/creatroom9",
                "cc.SpriteFrame",
                1
            ],
            "bc248b47-784f-4b2c-9068-77194294c7e2": [
                "resources/aqsounds/mj/81_1.mp3",
                "cc.AudioClip"
            ],
            "bc6eae82-6c8c-4d2e-be6d-e02ae3f5be07": [
                "resources/textures/setting/cr_unused",
                "cc.SpriteFrame",
                1
            ],
            "bc866d92-d324-4d6b-91bd-5b26df85577f": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "bcc4657a-c4e1-4ea7-9055-a4bd8db3e4f2": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "bce1c001-43f5-4e94-b2ca-8460d5a1960c": [
                "resources/textures/chat/Z_chat_bottom_1",
                "cc.SpriteFrame",
                1
            ],
            "bd28da36-5101-41f2-bcd2-d9ed5772e67d": [
                "resources/textures/images/JoinRoom/Num12.png",
                "cc.Texture2D"
            ],
            "bd489e17-6703-43ae-9f9a-f4962e2b5f6a": [
                "resources/textures/MJ/mjEmpty.plist",
                "cc.SpriteAtlas"
            ],
            "bd50fed4-576c-4e74-9641-7021f6999196": [
                "resources/textures/AQRes/GameOverOps/txtXP.png",
                "cc.Texture2D"
            ],
            "bd840aff-153b-4609-9f4e-c203d8d31d2e": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "bd90e422-9c22-4589-b810-204124de8953": [
                "resources/textures/images/JoinRoom/Num24.png",
                "cc.Texture2D"
            ],
            "bdc29270-9213-43b9-9c21-bb8c6684c8a1": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "bde088bf-fc4d-4b74-8b0b-3599ee306a36": [
                "resources/sounds/nv/gang.mp3",
                "cc.AudioClip"
            ],
            "be329022-a763-47b1-a16e-55e317f4464a": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "be329ec2-31be-4910-a8c6-ffd3361c9f59": [
                "resources/sounds/nv/8.mp3",
                "cc.AudioClip"
            ],
            "be38b769-8c5b-4993-8adf-89a1030fd1c7": [
                "resources/textures/images/createroom/creatroom16",
                "cc.SpriteFrame",
                1
            ],
            "bea93804-0272-4d44-a12f-f88972e359cb": [
                "resources/textures/images/PopupScene/PopupScene19.png",
                "cc.Texture2D"
            ],
            "bf2dbab1-06b0-4735-b270-270158435e32": [
                "resources/textures/AQRes/MJOps/opHu",
                "cc.SpriteFrame",
                1
            ],
            "bf5b7d86-5a26-4ffd-9388-afbf458ed3b8": [
                "resources/textures/images/GameEnd/GameEnd18.png",
                "cc.Texture2D"
            ],
            "bf6861d5-7745-42f4-a6a7-82278bb6d583": [
                "resources/textures/images/PopupScene",
                "cc.SpriteFrame",
                1
            ],
            "bf770d3d-7e24-4b87-8795-2bc25ed30aef": [
                "resources/textures/AQRes/GameOverOps/txtFQ.png",
                "cc.Texture2D"
            ],
            "bfab0cb0-774f-4c38-8263-4ed7c6c763f2": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "bfc5a117-78d6-40af-ae9c-a2ee8bc6f8b9": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "c0371335-9e49-4fb9-93c3-706e70fa6816": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "c08937b5-d001-4d18-8270-cd3ec1f0d4c0": [
                "resources/textures/chat/emoji_action_texture.plist",
                "cc.SpriteAtlas"
            ],
            "c09b72bf-c8e1-428a-b444-a476a552f590": [
                "resources/textures/images/createroom",
                "cc.SpriteFrame",
                1
            ],
            "c0e07b21-f5c5-4c49-ba3b-2240d9bde094": [
                "resources/textures/chat/yellowBg",
                "cc.SpriteFrame",
                1
            ],
            "c0e753fd-dfcf-4a82-9e39-da0c48c8846c": [
                "resources/textures/AQRes/MJOpsNew/opZhaoNing",
                "cc.SpriteFrame",
                1
            ],
            "c0f12e80-6caf-464c-94f4-6e22969b4ee7": [
                "resources/textures/images/playScenesc/sichuan_room_fangxiang_004.png",
                "cc.Texture2D"
            ],
            "c133eeb7-b095-4b0d-b52a-f6e62e25b8c6": [
                "resources/textures/MJRoom/Z_bg_lAr.png",
                "cc.Texture2D"
            ],
            "c1465d41-eb74-48aa-a3a2-e002d38185f4": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "c18daa7b-9e06-44a7-973a-391db6dce25c": [
                "resources/aqsounds/mj/w_6_1.mp3",
                "cc.AudioClip"
            ],
            "c190547d-93bf-45fa-8e7f-43847fc0ad26": [
                "resources/textures/voice/yyDialog",
                "cc.SpriteFrame",
                1
            ],
            "c203aa63-68bc-4d1e-b52b-4e28ae5b5ef3": [
                "resources/textures/AQRes/MJOps/opTaPai.png",
                "cc.Texture2D"
            ],
            "c20f66f1-0b1c-497d-91e0-db4d33870bc8": [
                "resources/textures/voice/v_anim3.png",
                "cc.Texture2D"
            ],
            "c20fd7f7-187e-4c40-8183-f6a896a2aed7": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "c2100b7f-8046-4bb3-bd52-cc4508178e06": [
                "resources/textures/images/createroom/creatroom19",
                "cc.SpriteFrame",
                1
            ],
            "c2176d76-6782-4489-8c79-f11a8a8830ee": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "c2236cae-cdc3-41d4-a58f-b5b4e18fb6e2": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "c247a0e4-43d3-4d92-a51a-6ed4c7a8c040": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "c2fdcfdd-a35e-40f2-bbb2-6ebabb119408": [
                "resources/textures/AQRes/MJOpsNew/opZimo",
                "cc.SpriteFrame",
                1
            ],
            "c310366e-416a-4166-ba90-9b5a589f1dd6": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "c35aecc1-b59d-4b7b-a4c9-96ae664695f0": [
                "resources/textures/images/history_record",
                "cc.SpriteFrame",
                1
            ],
            "c35d6dfe-70ee-4bc0-8bc8-38b9ec4618f2": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "c3c044c5-9bab-4f50-8fe9-38982a5a648e": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "c3fde634-7152-4408-813e-581c2ca09f45": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "c41cdad0-7a82-4850-bef7-1ca7d76dc99a": [
                "resources/textures/AQRes/GameOver/gameOver.png",
                "cc.Texture2D"
            ],
            "c462b30b-01c4-474f-a739-55066947ee14": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "c485c89b-5f66-43d0-bb9a-5de9cf23d4d2": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "c4d115bb-13ff-4b23-b77e-4c8cda3419b1": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "c4d40921-9a54-4928-8409-ce5c5c456840": [
                "resources/textures/images/setting",
                "cc.SpriteFrame",
                1
            ],
            "c51375b5-4c61-4f7c-b50f-a1a4dfec35d2": [
                "resources/textures/AQRes/BtnPNG/分享",
                "cc.SpriteFrame",
                1
            ],
            "c5d05eae-3036-461d-a295-3fb72044ef95": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "c600d178-02b7-4c68-920c-c79cf0f74790": [
                "resources/textures/images/createroom",
                "cc.SpriteFrame",
                1
            ],
            "c60594aa-0fc0-4c3e-ae2f-0f4f55b89a17": [
                "resources/textures/AQRes/CreateRoom/返回大厅.png",
                "cc.Texture2D"
            ],
            "c618bb0c-1c9d-4c1e-aab8-1cb593fbdf59": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "c61b13c5-2120-409a-a83c-ad8a3d6fdc2a": [
                "resources/textures/images/playScenesc/wenzi003",
                "cc.SpriteFrame",
                1
            ],
            "c6317a07-e5db-4930-a7d5-bd6fe7ef7b5e": [
                "resources/textures/images/playScenesc/play_scene_4",
                "cc.SpriteFrame",
                1
            ],
            "c6792fab-a883-4d0f-8ec5-ad4d738671cd": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "c682722e-501f-491f-83b9-6ac4cf7101e4": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "c6b304ee-4826-4b27-a6b7-488e344ec3fe": [
                "resources/textures/AQRes/GameOverOps/mkZhuang",
                "cc.SpriteFrame",
                1
            ],
            "c7289abd-1844-47d5-87b2-aabd502393c7": [
                "resources/textures/chat/chat_emoji",
                "cc.SpriteFrame",
                1
            ],
            "c7497944-f5d2-423e-a9ff-ae79ed23e315": [
                "resources/textures/AQRes/CreateRoom/chkNo",
                "cc.SpriteFrame",
                1
            ],
            "c788c07e-1228-4fd8-ad41-3f7fd3a6a94f": [
                "resources/aqsounds/mj/17_2.mp3",
                "cc.AudioClip"
            ],
            "c7998ec0-659f-42d2-9556-de29a958f84c": [
                "resources/aqsounds/mj/zhaoning.mp3",
                "cc.AudioClip"
            ],
            "c7aa7464-f605-4b77-80db-fcc3ebced9f0": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "c80abbb2-c412-4736-96c6-734930fa9dd9": [
                "resources/textures/MJ/bottom/Z_bottom.plist",
                "cc.SpriteAtlas"
            ],
            "c837c74e-88e8-447f-b588-e0c4ad9099d1": [
                "resources/aqsounds/voice/chat_w_4.mp3",
                "cc.AudioClip"
            ],
            "c839b3b9-85a0-4d72-8154-19018837fa8b": [
                "resources/textures/images/createroom/creatroom19.png",
                "cc.Texture2D"
            ],
            "c87018b3-f070-4296-89a1-b381ed5cf759": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "c8782787-96e7-45b0-9822-e165b1bb84d9": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "c8811479-fd90-4c3a-9ac6-61a1dfeb1c0e": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "c88c7f40-f82e-468d-856b-1b9c0150791b": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "c8915798-3669-49e8-a367-fad27840939a": [
                "resources/textures/MJ/my/Z_my2.png",
                "cc.Texture2D"
            ],
            "c97c08d8-0928-41f9-a577-05f4d19b31aa": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "c993886c-1bec-463c-a4dd-baab41d8173f": [
                "resources/textures/images/setting",
                "cc.SpriteFrame",
                1
            ],
            "c9a5c084-90f8-4ed3-abec-9deac3faf2ad": [
                "resources/aqsounds/mj/24_1.mp3",
                "cc.AudioClip"
            ],
            "c9b8641e-3063-4634-b242-cb58a6a1215e": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "ca3cd36e-e121-4a85-9b3f-0daa92af7d54": [
                "resources/sounds/bgMain.mp3",
                "cc.AudioClip"
            ],
            "ca63a0ab-a2bd-4ec1-8ec3-a72e34ed2499": [
                "resources/textures/images/replayBtn",
                "cc.SpriteFrame",
                1
            ],
            "caae3f7d-5afc-40df-b058-2c229216895e": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "cadcb5af-c7f1-4815-b57e-52289bafc0e8": [
                "resources/textures/chat/yellowBg.png",
                "cc.Texture2D"
            ],
            "caeb40a8-4f48-4fcc-b20e-2406ff408450": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "caeffa32-dd50-48ec-9df4-2ad873d8535c": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "cb10cfc4-0088-4bc5-8f3b-a2af00fb88b1": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "cb301ca5-df49-484a-9cfb-6192288b56ef": [
                "resources/textures/images/createroom/creatroom13.png",
                "cc.Texture2D"
            ],
            "cb4e21d0-25bc-478e-9c68-b5f3e210be19": [
                "resources/textures/AQRes/Hall/erweima.png",
                "cc.Texture2D"
            ],
            "cb83b0d1-3c71-4269-a1e3-3762bc9c3898": [
                "resources/aqsounds/voice/chat_m_16.mp3",
                "cc.AudioClip"
            ],
            "cba1400b-4e40-44d4-a288-1d74f6757332": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "cba31817-ea68-42a5-a400-1b979dd6e434": [
                "resources/textures/images/history_record",
                "cc.SpriteFrame",
                1
            ],
            "cba40643-a5b7-47c5-9f55-32fd075e0e0b": [
                "resources/sounds/dropCoin.mp3",
                "cc.AudioClip"
            ],
            "cc419b28-eb9a-4386-b836-1af7dac0d184": [
                "resources/textures/png/room_num",
                "cc.SpriteFrame",
                1
            ],
            "cc642103-5997-4603-a5ba-d01acd173437": [
                "resources/textures/images/loading",
                "cc.SpriteFrame",
                1
            ],
            "cc6528c2-c73e-4073-acc8-7c5e470a55a7": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "cc85a55a-6a5f-4376-a1e7-9def6d14f73b": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "cc8fee98-6e24-485c-8c73-0c51abc59d09": [
                "resources/textures/AQRes/CreateRoom/战绩",
                "cc.SpriteFrame",
                1
            ],
            "ccbd262d-9b47-4e96-a498-fe30e0ffe58f": [
                "resources/textures/MJRoom/Z_help.png",
                "cc.Texture2D"
            ],
            "cccb9d35-4492-48ef-87e1-e550adfca1c6": [
                "resources/textures/images/GameEnd/GameEnd8",
                "cc.SpriteFrame",
                1
            ],
            "ccd18d7e-f6c1-4764-8fd5-2bacd8e34d36": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "cceeb278-53dd-49a2-ad52-9aa7a73203da": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "cd0f27e6-0e4d-4008-a435-e5082a00adb9": [
                "resources/sounds/horse/bgRace.mp3",
                "cc.AudioClip"
            ],
            "cd29c45b-8029-40d1-82c6-592c23b7d3cb": [
                "resources/textures/MJ/left/Z_left.png",
                "cc.Texture2D"
            ],
            "cd2a8d74-a66a-4bea-b642-acc37b231b73": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "cd8f59ff-9bdc-4b86-8bd8-2ed99cc3da9a": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "cdb95d9b-b7ae-4d5d-bd67-fa757a4c282d": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "cdbf24b6-487a-47c4-89c8-a26c904af2d4": [
                "resources/textures/images/youqingTip.plist",
                "cc.SpriteAtlas"
            ],
            "cdffd874-3c5c-4b12-9512-b08a00b6b00c": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "ce1b111a-a697-46ed-a8cf-4c0a0d925535": [
                "resources/aqsounds/mj/w_14_2.mp3",
                "cc.AudioClip"
            ],
            "ce26aaba-deda-4f72-a768-ab9633a787d8": [
                "resources/textures/images/PopupScene/PopupScene3.png",
                "cc.Texture2D"
            ],
            "ce4170f0-5b70-47fa-b469-c681eb21d045": [
                "resources/aqsounds/mj/w_17_2.mp3",
                "cc.AudioClip"
            ],
            "cead5d37-33b1-4d99-a054-e64168a887f6": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "ceb4144f-fb9b-4869-ac37-cbcff0a0bef7": [
                "resources/textures/images/createroom",
                "cc.SpriteFrame",
                1
            ],
            "ceb6e600-b231-4f3e-9528-4131277712b7": [
                "resources/textures/images/playScenesc/play_scene_6",
                "cc.SpriteFrame",
                1
            ],
            "cedd15d4-6953-4ab8-b2c3-db44e1d961f6": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "ceddd85f-1240-4cce-81d8-d3cb25c13b31": [
                "resources/textures/images/history_record",
                "cc.SpriteFrame",
                1
            ],
            "cee0a6f9-4a13-4812-9345-716c410f655f": [
                "resources/textures/AQRes/MJOpsNew/opZhaoNing.png",
                "cc.Texture2D"
            ],
            "cf017ebf-8a52-4b86-b6b8-e84ad1dd9ff0": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "cf1eac64-de9c-4dd6-9a26-a417abeb55e1": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "cf4f1ee6-e71c-44a5-adf4-d71d7d33530e": [
                "resources/textures/images/createroom",
                "cc.SpriteFrame",
                1
            ],
            "cf7639d5-3f54-4160-94e6-01298d35fd02": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "cf878df7-9ee1-43b4-b87a-d6a457e4f8f6": [
                "resources/textures/images/GameEnd/GameEnd5",
                "cc.SpriteFrame",
                1
            ],
            "cf94d4b1-9537-4975-a21e-5d1287c59c0b": [
                "resources/textures/AQRes/Hall/btnSet.png",
                "cc.Texture2D"
            ],
            "cfd77e17-4fe0-452c-ad1b-b90d9f29f0dc": [
                "resources/aqsounds/voice/chat_w_17.mp3",
                "cc.AudioClip"
            ],
            "cfe8c1d9-1810-4839-b265-8a54f53682aa": [
                "resources/textures/bk/btn_enter_room.png",
                "cc.Texture2D"
            ],
            "cff26fca-aca8-4f1d-9486-ddb04669788b": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "d01163fb-fd9b-4515-91bc-89546113876e": [
                "resources/aqsounds/mj/w_zhaoning.mp3",
                "cc.AudioClip"
            ],
            "d032addd-a122-48bd-80c2-1cd3962bcac0": [
                "resources/textures/AQRes/GameOver/imgLose.png",
                "cc.Texture2D"
            ],
            "d058622b-9a0d-46d6-ba35-f69db29033c9": [
                "resources/textures/AQRes/GameOverOps/bgLight",
                "cc.SpriteFrame",
                1
            ],
            "d058b5dd-3036-4490-82f1-96ddda0bbb32": [
                "resources/textures/AQRes/MJOps/opPeng.png",
                "cc.Texture2D"
            ],
            "d0c01627-4d4d-4e0f-a8cf-872b3965b082": [
                "resources/textures/images/dingque",
                "cc.SpriteFrame",
                1
            ],
            "d0d50645-30a9-4207-9f58-ae6e0d360649": [
                "resources/textures/chat/chat_normol",
                "cc.SpriteFrame",
                1
            ],
            "d0dfc100-c4be-45de-8df6-1c51de881c06": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "d10d3fb4-7a2c-4698-96e9-77c8914eb137": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "d11d2671-a944-47f4-9f98-e224d71e50cd": [
                "resources/textures/AQRes/CreateRoom/lobby.jpg",
                "cc.Texture2D"
            ],
            "d161e7c9-9600-44f0-8860-05bbde58c97f": [
                "resources/textures/images/createroom/creatroom14.png",
                "cc.Texture2D"
            ],
            "d184fd48-1327-4dcd-b226-2f322843dd36": [
                "resources/textures/MJRoom/powerG.PNG",
                "cc.Texture2D"
            ],
            "d19d9b02-9fff-4b3c-ab53-0d4de68e5ad3": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "d1a03cc6-a428-433f-b66a-a2dcb6ce98ef": [
                "resources/textures/images/setting.png",
                "cc.Texture2D"
            ],
            "d1a45b6d-1a41-4ff3-a7e5-3d30942961d3": [
                "resources/textures/AQRes/GameOverOps/bgLight.png",
                "cc.Texture2D"
            ],
            "d1ea8ac0-7e17-4924-899e-1b2bd520fca6": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "d1ec3487-8cce-4e85-9c42-527f44b59e9e": [
                "resources/textures/AQRes/Hall/btnVoice.png",
                "cc.Texture2D"
            ],
            "d1ec606d-1a3a-4ebf-a9f1-5391b946cd31": [
                "resources/textures/MJRoom/Z_wifi.plist",
                "cc.SpriteAtlas"
            ],
            "d229eb34-e990-456f-810d-447881995cb2": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "d26b0b0d-6f06-43e6-81dc-2445fc014a43": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "d278cab0-d89b-4fca-b312-e5a5279015fa": [
                "resources/textures/images/playScenesc/sichuan_room_fangxiang_003",
                "cc.SpriteFrame",
                1
            ],
            "d2bf07d6-cbaa-485a-9e05-b9409bf16b62": [
                "resources/textures/images/main_scene",
                "cc.SpriteFrame",
                1
            ],
            "d2d631ae-5272-47d1-9604-e3ec6d209b9f": [
                "resources/textures/images/public_ui",
                "cc.SpriteFrame",
                1
            ],
            "d32d6096-5429-454a-9716-5f3f04283e70": [
                "resources/textures/AQRes/GameOver/imgPeace",
                "cc.SpriteFrame",
                1
            ],
            "d34dfa25-ad7e-4bc8-8fd3-c1a0f7b7fe45": [
                "resources/textures/images/loading.png",
                "cc.Texture2D"
            ],
            "d36dba82-a906-470d-b55a-a1df59b7c972": [
                "resources/textures/voice/v4.png",
                "cc.Texture2D"
            ],
            "d39e5304-2526-469c-820e-9c156171542d": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "d3d5eade-2419-41b3-bdc8-4a00b8be9398": [
                "resources/aqsounds/voice/chat_m_9.mp3",
                "cc.AudioClip"
            ],
            "d3d93b91-5ef4-40df-9456-d6915b2098a5": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "d3f82fbd-3aea-4483-95bf-e62445554a25": [
                "resources/textures/AQRes/MJOpsNew/opGuan.png",
                "cc.Texture2D"
            ],
            "d460b616-f613-4799-ac5e-ca927f08f7ed": [
                "resources/textures/images/efx/rain1.png",
                "cc.Texture2D"
            ],
            "d4917060-3cf2-4dce-b5f2-4697fe91c839": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "d4b0d222-3ed9-4e3c-b4ac-e26e001af5d1": [
                "resources/textures/hall/z_datingtouxiang.png",
                "cc.Texture2D"
            ],
            "d4b7c258-db82-4cee-99c3-cbdd4c71b941": [
                "resources/textures/images/efx/guafeng1",
                "cc.SpriteFrame",
                1
            ],
            "d4d9167a-807c-4b88-a0e2-4694c98d0253": [
                "resources/aqsounds/mj/w_16_1.mp3",
                "cc.AudioClip"
            ],
            "d4dd7c87-74a1-4d63-a70b-0c7531509b5e": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "d4ed868f-c4a4-4a75-9ae8-e8c005d9fa10": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "d4ee4a99-35be-459e-8221-3d7064ad0051": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "d4f25282-d9d0-4d30-8b2d-0b93e761e717": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "d5119d67-5224-4403-8130-5a1b50128280": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "d533f107-b48c-458b-bbdd-97af6dcf0247": [
                "resources/textures/images/createroom/creatroom17.png",
                "cc.Texture2D"
            ],
            "d53f742d-e434-4147-ae95-498fda6957bb": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "d59ae64d-0860-428b-87a0-dcc69c4e3979": [
                "resources/textures/images/JoinRoom/Num15",
                "cc.SpriteFrame",
                1
            ],
            "d5da5754-a91d-4204-8aca-5e648a4da1c8": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "d61777c9-dc91-434a-af24-e2bcadc24750": [
                "resources/sounds/guess/win.mp3",
                "cc.AudioClip"
            ],
            "d63b4a9e-dc2b-4084-8664-8331e0c904a7": [
                "resources/textures/images/playScenesc/wenzi003.png",
                "cc.Texture2D"
            ],
            "d66bc7a7-2fdf-4b3b-8d82-fa3d5b54e48a": [
                "resources/aqsounds/mj/w_4_2.mp3",
                "cc.AudioClip"
            ],
            "d696c0f7-af2e-49d7-b6f4-3a139c9509be": [
                "resources/textures/AQRes/CreateRoom/解散房间.png",
                "cc.Texture2D"
            ],
            "d6ac5272-f7f6-463c-9483-ac39ba7e68ba": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "d6b3ffaa-1c65-4919-ab9e-e0bdcbf0898e": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "d6c27a49-a8c5-4566-80e5-2ed6c6183140": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "d6e22493-66e7-47b5-a0fa-855e44a69c1a": [
                "resources/aqsounds/mj/w_15_2.mp3",
                "cc.AudioClip"
            ],
            "d7003345-08c2-4992-8b68-31cffc31b677": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "d705dbcc-23e3-4069-98da-552ba69bea94": [
                "resources/textures/images/status/powerG",
                "cc.SpriteFrame",
                1
            ],
            "d77f0c00-c9dc-4603-9162-f4f07331e59d": [
                "resources/aqsounds/mj/w_21_2.mp3",
                "cc.AudioClip"
            ],
            "d793451a-9795-447f-b8e6-00a2e2a17218": [
                "resources/textures/images/GameEnd/GameEnd9",
                "cc.SpriteFrame",
                1
            ],
            "d79c5202-76c4-4caa-938e-36a9452f100b": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "d7ee5835-6b15-4294-a79d-72574036cd3f": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "d8124940-c8ac-4655-87da-af4cb862de8f": [
                "resources/textures/images/youqingTip",
                "cc.SpriteFrame",
                1
            ],
            "d88bc5f5-cb4a-4fbb-bf93-ece751da0850": [
                "resources/aqsounds/mj/hu_1.mp3",
                "cc.AudioClip"
            ],
            "d8c23fde-98ee-47d9-a571-945d1f04e5b8": [
                "resources/textures/AQRes/logoB",
                "cc.SpriteFrame",
                1
            ],
            "d8ea2224-0fb3-4783-ba15-c52de5877b51": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "d91a620b-6005-4c66-b946-fa716df7bfa9": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "d94f8d8b-cd1a-48bb-87ec-c7a9fdcc534f": [
                "resources/textures/loading/load.png",
                "cc.Texture2D"
            ],
            "d968d96e-fe8e-4332-9646-a558343b20ee": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "d98016fb-e005-46b8-b1f2-b7934b265254": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "d98c0735-3f6a-45a2-862d-db432518af85": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "d99d4b2b-e612-48ee-a29d-f65d513fec3d": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "d9a5c7ff-3978-4dd1-a86c-22a18c33ef01": [
                "resources/textures/images/chat.png",
                "cc.Texture2D"
            ],
            "d9b66270-d5fe-4b3d-98ae-312822701bee": [
                "resources/textures/AQRes/GameOver/lixian",
                "cc.SpriteFrame",
                1
            ],
            "d9ce28a2-0eb0-4097-b6eb-b532a8ea28d9": [
                "resources/aqsounds/mj/w_16_2.mp3",
                "cc.AudioClip"
            ],
            "d9eb0394-4dde-4a94-a36e-040e1b6cadfa": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "d9fdfb5f-1f60-4026-bc3a-980e1659e417": [
                "resources/textures/voice/dialog_loading_bg.9.png",
                "cc.Texture2D"
            ],
            "da188ab6-425b-4a6f-bd3b-e78ef20362ce": [
                "resources/textures/AQRes/MJOpsNew/opKan.png",
                "cc.Texture2D"
            ],
            "daa887d9-e85c-4396-b8e3-03bce53ab4a1": [
                "resources/textures/images/chat.plist",
                "cc.SpriteAtlas"
            ],
            "dab6187e-44ef-407f-9621-1fc227ed6c11": [
                "resources/textures/AQRes/MJOpsNew/opShiFenRenShu",
                "cc.SpriteFrame",
                1
            ],
            "dac9ca98-9176-4ebb-b12f-1c55ff7f92af": [
                "resources/textures/AQRes/GameOverOps/mkXian.png",
                "cc.Texture2D"
            ],
            "daf4f6ca-498c-4dd4-91aa-89d63269eb85": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "db2e469d-64f7-4fb2-b62d-bf54447a517e": [
                "resources/textures/chat/chatbg_lt",
                "cc.SpriteFrame",
                1
            ],
            "db2e51a5-9511-4d64-ac51-951d80da0101": [
                "resources/textures/images/GameEnd/GameEnd11",
                "cc.SpriteFrame",
                1
            ],
            "db32874e-c5f0-4610-b7a0-9275ce5b9aad": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "db886f00-0ada-4183-b6d9-f339a7d16dd4": [
                "resources/textures/chat/chat_backbg",
                "cc.SpriteFrame",
                1
            ],
            "dbd19b29-4619-4633-863c-ef9a9aedd7b7": [
                "resources/aqsounds/voice/chat_w_9.mp3",
                "cc.AudioClip"
            ],
            "dbe2b062-0b7c-424c-b928-84658f4324e4": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "dc0f12a0-949b-450f-9d67-9f1054b115f8": [
                "resources/textures/MJ/left/Z_left.plist",
                "cc.SpriteAtlas"
            ],
            "dc18bcef-c6d8-46b3-81a0-d91ce3ed83ea": [
                "resources/textures/bk/bg1",
                "cc.SpriteFrame",
                1
            ],
            "dc1a17e2-17f0-4cae-b88f-af8176f08e24": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "dc8aaac9-73a5-48aa-b04c-6679debab83f": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "dcb3439d-fc5f-48a9-a245-c7395ef34b57": [
                "resources/textures/AQRes/CreateRoom/nv2.png",
                "cc.Texture2D"
            ],
            "dcd60dc6-9aa2-4a73-b761-a68d48fce76f": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "dce56925-2b5d-40fd-9023-28605ac3a8d9": [
                "resources/textures/voice/recorder",
                "cc.SpriteFrame",
                1
            ],
            "dcfd81f9-c553-44c7-b21a-6e485107dd8c": [
                "resources/textures/images/PopupScene",
                "cc.SpriteFrame",
                1
            ],
            "dd067b6a-d750-478c-b239-a845ef554ef7": [
                "resources/textures/images/public_ui",
                "cc.SpriteFrame",
                1
            ],
            "dd3e1879-4f97-44d4-bfca-4eb083547f9b": [
                "resources/sounds/shuffle.mp3",
                "cc.AudioClip"
            ],
            "dd853401-f06b-4c5f-a68b-3b5e58978189": [
                "resources/textures/images/playScenesc/sichuan_room_fangxiang_003_01.png",
                "cc.Texture2D"
            ],
            "dd919080-c9bd-4369-b1a4-a01b0f91cf99": [
                "resources/textures/images/JoinRoom/Num1.png",
                "cc.Texture2D"
            ],
            "dda886d3-5eef-430c-b1ca-02d59e5cb8ba": [
                "resources/textures/AQRes/CreateRoom/progBG",
                "cc.SpriteFrame",
                1
            ],
            "ddc89083-5194-4822-a109-4a65e07411d2": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "ddea37ca-c992-42bf-9e40-1ff628fa5eb8": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "de08319e-2c5e-4643-9b9c-3da6315eee97": [
                "resources/sounds/nv/7.mp3",
                "cc.AudioClip"
            ],
            "de0c43c2-ba63-44a8-b2e8-ace6b0d571ab": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "de3e33a8-6586-423d-a0ad-54778297f707": [
                "resources/aqsounds/mj/w_angang_2.mp3",
                "cc.AudioClip"
            ],
            "de83c74a-5db9-4fb2-a445-f8ee6aa5ed45": [
                "resources/textures/bk/LOGO_mini",
                "cc.SpriteFrame",
                1
            ],
            "dea21e63-8921-4e0c-80c0-dbb6c674a8ab": [
                "resources/aqsounds/mj/7_1.mp3",
                "cc.AudioClip"
            ],
            "debe80de-a6dc-4fba-badc-39eb0779414b": [
                "resources/textures/voice/v2.png",
                "cc.Texture2D"
            ],
            "dee58ff1-5bec-46cf-855a-76c892fdb14f": [
                "resources/textures/images/PopupScene/PopupScene23",
                "cc.SpriteFrame",
                1
            ],
            "dee9b327-1443-40c8-ab14-76110c4bae92": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "df1a088a-f61c-41e7-a225-7b4104d24f1a": [
                "resources/aqsounds/mj/5_1.mp3",
                "cc.AudioClip"
            ],
            "df2accea-10ef-49a2-83d4-be88e35e48d7": [
                "resources/textures/images/playScenesc/wenzi004",
                "cc.SpriteFrame",
                1
            ],
            "df3d826b-8800-4316-8780-e9ecc0a37972": [
                "resources/textures/images/PopupScene/PopupScene7.png",
                "cc.Texture2D"
            ],
            "df5ad157-977f-4d3d-8740-b377bb147b86": [
                "resources/textures/ShaiZiAni/dice.png",
                "cc.Texture2D"
            ],
            "dfc41a69-2240-4cf0-b2fe-2df310a7d156": [
                "resources/textures/AQRes/CreateRoom/btnOrge",
                "cc.SpriteFrame",
                1
            ],
            "dfc88041-d607-409f-bc18-d65dc7226af1": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "dfd021b3-5669-4e73-8a28-5e16a579e110": [
                "resources/textures/images/efx/rain3",
                "cc.SpriteFrame",
                1
            ],
            "e015c7f1-cd70-4c94-b381-5f910532e4cc": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "e01efad3-b4bb-4d7b-a66b-bb0ddb307fee": [
                "resources/textures/images/GameEnd/GameEndHu.png",
                "cc.Texture2D"
            ],
            "e03a94f1-da24-45a3-bcba-0c9e221fbe19": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "e03d658a-02ed-45aa-ba47-fd334f216c2d": [
                "resources/textures/MJ/mjEmpty2",
                "cc.SpriteFrame",
                1
            ],
            "e0906f59-9a5b-4b4a-9406-132008e36610": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "e0d9e42d-cdf9-4055-9da6-f806d4081402": [
                "resources/textures/AQRes/MJOpsNew/opPeng",
                "cc.SpriteFrame",
                1
            ],
            "e0fc5d90-538c-4246-8758-d78b448b86e7": [
                "resources/aqsounds/mj/5_2.mp3",
                "cc.AudioClip"
            ],
            "e115ae6a-05e3-405c-a5c4-cf9ed1488859": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "e1182200-5576-4c3b-8e24-2a06167a7f35": [
                "resources/aqsounds/mj/w_hu_2.mp3",
                "cc.AudioClip"
            ],
            "e11fbea4-d261-413d-beca-282a0edad004": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "e1279c9b-922a-4263-be6e-80f680a805d7": [
                "resources/textures/setting/z_fuxuan_on",
                "cc.SpriteFrame",
                1
            ],
            "e1714551-b433-4d23-8991-52e100dde094": [
                "resources/textures/images/playScenesc/sichuan_room_fangxiang_003_01",
                "cc.SpriteFrame",
                1
            ],
            "e1a3a475-5518-47c8-a3dd-0d00a6ec9eb5": [
                "resources/textures/MJRoom/Z_offline",
                "cc.SpriteFrame",
                1
            ],
            "e1b15a33-b97d-4e1a-b851-6fd562170671": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "e1c391b9-3c69-4637-8876-9a1bbd585e97": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "e1c75566-4120-4dc7-8623-d75450c1813b": [
                "resources/textures/images/efx/guafeng4.png",
                "cc.Texture2D"
            ],
            "e1ffb160-a2ba-4b85-9133-0616b4de100c": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "e21aa672-ddd3-4350-8e8c-5b288d2892f5": [
                "resources/textures/png/money_frame.png",
                "cc.Texture2D"
            ],
            "e252d253-751b-4c11-ac0d-879a48159947": [
                "resources/textures/images/GameEnd/GameEnd3",
                "cc.SpriteFrame",
                1
            ],
            "e2ee7b27-e1c5-42bb-8298-7b15aa9b1823": [
                "resources/textures/AQRes/MJOps/opChi",
                "cc.SpriteFrame",
                1
            ],
            "e2f14414-ace7-452c-a406-a9aa28f711a0": [
                "resources/textures/MJ/mjEmpty2",
                "cc.SpriteFrame",
                1
            ],
            "e32af17a-0ddc-4a4e-bfe2-ebe6ae9f936b": [
                "resources/textures/images/PopupScene/PopupScene5",
                "cc.SpriteFrame",
                1
            ],
            "e34ab6be-5473-4435-94e5-e6c24b404352": [
                "resources/textures/images/JoinRoom.plist",
                "cc.SpriteAtlas"
            ],
            "e35d2605-6a6c-4e32-8086-530b841a53a8": [
                "resources/textures/images/Login/btn_ traveler",
                "cc.SpriteFrame",
                1
            ],
            "e37d1ee9-ac92-47c6-a2a7-66a3c524f7df": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "e3c02479-1e32-4ed9-92db-bd71fe76c9f4": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "e40090f5-e09b-4075-80ae-27bfae117c68": [
                "resources/textures/AQRes/MJOpsNew/opShiFenRenShu.png",
                "cc.Texture2D"
            ],
            "e4104d7a-3e91-4f48-94a8-2b6042c86ed4": [
                "resources/textures/AQRes/BtnPNG/设置",
                "cc.SpriteFrame",
                1
            ],
            "e42df99c-ed6d-4f8e-950e-149a3aa49fc2": [
                "resources/textures/AQRes/CreateRoom/骰子ICON.png",
                "cc.Texture2D"
            ],
            "e4541bad-52d3-4c74-92bb-962535aefddb": [
                "resources/aqsounds/mj/29_2.mp3",
                "cc.AudioClip"
            ],
            "e4559bf5-f5d1-4171-a113-27606ec6ceab": [
                "resources/textures/AQRes/GameOver/imgPeace.png",
                "cc.Texture2D"
            ],
            "e463b89c-e7f1-4ad2-a8cb-eacc9507ca91": [
                "resources/textures/bk/bg2.png",
                "cc.Texture2D"
            ],
            "e4c949c5-bf1d-428a-9321-0eb16afc28b8": [
                "resources/textures/images/JoinRoom/Num19",
                "cc.SpriteFrame",
                1
            ],
            "e4dd8567-0059-4b60-b181-374f80e69466": [
                "resources/aqsounds/voice/chat_m_10.mp3",
                "cc.AudioClip"
            ],
            "e4f8042d-0ace-43c9-b944-929028c7e47b": [
                "resources/textures/images/createroom",
                "cc.SpriteFrame",
                1
            ],
            "e52aa553-af72-440b-b7a5-fc46ce134e8b": [
                "resources/textures/MJ/bottom/B_bamboo_5",
                "cc.SpriteFrame",
                1
            ],
            "e52de18b-8b53-48e1-800d-f73387df3373": [
                "resources/textures/MJ/mjEmpty2",
                "cc.SpriteFrame",
                1
            ],
            "e56906ba-f422-4de3-b978-94c4b56ff676": [
                "HotUpdate/Texture/bg_jinbishu.png",
                "cc.Texture2D"
            ],
            "e59295a7-b9e0-4087-abe4-b1faf7d3e623": [
                "resources/aqsounds/mj/w_5_2.mp3",
                "cc.AudioClip"
            ],
            "e5e1100f-092a-4124-836e-e492a6352765": [
                "resources/textures/AQRes/CreateRoom/个人信息",
                "cc.SpriteFrame",
                1
            ],
            "e61ba129-1955-4226-8fd5-30428ecccd8b": [
                "resources/textures/AQRes/Hall/txtZhang.png",
                "cc.Texture2D"
            ],
            "e65db430-f411-486f-9cb0-1fb8bd0499b4": [
                "resources/aqsounds/mj/w_27_1.mp3",
                "cc.AudioClip"
            ],
            "e66d2a4e-bc86-479b-8927-8872bb07d6cf": [
                "resources/textures/AQRes/MJOpsNew/opGuan",
                "cc.SpriteFrame",
                1
            ],
            "e67f0372-3a3d-4133-9d6a-bd758c14a1cb": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "e6dc53ae-d721-4b5d-be8d-9bf3a914f557": [
                "resources/textures/images/efx/guafeng3.png",
                "cc.Texture2D"
            ],
            "e6e7ab6b-5d8c-49c8-8546-a1da8a60c9f3": [
                "resources/sounds/nv/29.mp3",
                "cc.AudioClip"
            ],
            "e7180329-ad86-43c4-871e-6d337895df56": [
                "resources/textures/chat/Z_chat_bottom_1.png",
                "cc.Texture2D"
            ],
            "e7251a80-651a-43bf-8f5c-5a04eb85ab29": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "e780b529-e5dd-47f4-89b9-085eb0c305ff": [
                "resources/textures/images/playScenesc/sichuan_room_fangxiang_002",
                "cc.SpriteFrame",
                1
            ],
            "e7b17174-dc37-407e-8cd3-09b440c7d89d": [
                "resources/textures/setting/checkbox_void",
                "cc.SpriteFrame",
                1
            ],
            "e80565cc-cf17-4cc3-8fc9-c7a6173f053b": [
                "resources/sounds/nv/41.mp3",
                "cc.AudioClip"
            ],
            "e811c999-f2a8-4aa6-93ea-f0e14f6a71c0": [
                "resources/sounds/fix_msg_3.mp3",
                "cc.AudioClip"
            ],
            "e831abb0-cd33-4ee6-9a46-789093a725d2": [
                "resources/aqsounds/mj/diangang.mp3",
                "cc.AudioClip"
            ],
            "e836456a-11d3-4b62-bad7-38cbbfa8ae15": [
                "resources/textures/images/title",
                "cc.SpriteFrame",
                1
            ],
            "e837c341-ce87-47e3-baf0-dfec2a1a0a0e": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "e86727d5-028b-4b5c-8806-8d5098c5128b": [
                "resources/textures/MJRoom/Z_corner_lbottom",
                "cc.SpriteFrame",
                1
            ],
            "e881bd21-daa1-42fd-a157-832b37bbccd3": [
                "resources/textures/AQRes/GameOverOps/mkGuan",
                "cc.SpriteFrame",
                1
            ],
            "e88648aa-c921-4c4c-a071-9ef8ea95a262": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "e88b910a-e9fc-49b4-bb6b-709011be0fd6": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "e8ccbfa0-a932-4371-8b0b-354910b74d01": [
                "resources/textures/AQRes/GameOver/imgWin.png",
                "cc.Texture2D"
            ],
            "e8d9f695-ac2f-43dc-b9a0-b51e9531a700": [
                "resources/textures/images/public_ui",
                "cc.SpriteFrame",
                1
            ],
            "e8dd3c7f-2177-4c87-8ce7-9f08bc34d8bc": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "e8e2ee02-19dc-4e3b-a16f-02c482589aeb": [
                "resources/textures/MJRoom/Z_nobody.png",
                "cc.Texture2D"
            ],
            "e8eda776-a770-42ff-bd4b-1ddaa5aac8d8": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "e900576e-fc46-4e43-879d-716b08c3a544": [
                "resources/textures/images/JoinRoom/Num2",
                "cc.SpriteFrame",
                1
            ],
            "e90bfd1a-c3d2-4660-8412-c471a55929b0": [
                "resources/textures/loading/dian3.png",
                "cc.Texture2D"
            ],
            "e912bdb2-f94f-4721-b384-7c9c4e6ab229": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "e9566c3a-ec09-4e32-997a-de3573691f84": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "e97f8db4-4c2b-43c9-8b16-2a064aa4c200": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "e982967c-25c6-433c-8d4f-8f007344cfa1": [
                "resources/textures/AQRes/Hall/settings.prefab",
                "cc.Prefab"
            ],
            "e9893f59-7e53-46a2-9bae-31d0a58db638": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "e999a047-0d36-4d3b-a381-c51488eebc23": [
                "resources/textures/AQRes/CreateRoom/创建房间",
                "cc.SpriteFrame",
                1
            ],
            "e99eaed7-5cfc-4a44-9469-4808ce7edca6": [
                "resources/sounds/nv/81.mp3",
                "cc.AudioClip"
            ],
            "e9c83e17-29c5-4a43-9b74-de88c296a526": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "e9e283ed-db3a-4a45-926e-561c35529e91": [
                "resources/textures/images/GameEnd/GameEnd4.jpg",
                "cc.Texture2D"
            ],
            "e9ecb267-78f5-49b7-be14-60cc9116abe1": [
                "resources/textures/images/Login/bg",
                "cc.SpriteFrame",
                1
            ],
            "ea0d9383-c0d7-4b7e-9fa8-4d16992991d5": [
                "resources/textures/images/setting",
                "cc.SpriteFrame",
                1
            ],
            "ea2cac6b-362a-4742-87fc-f5fe97f7fb04": [
                "resources/textures/png/Z_zhunbeizhuangt.png",
                "cc.Texture2D"
            ],
            "ea7d8d70-11a2-48fd-8f20-82bd49ffce05": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "eade92b5-ca98-4158-be48-b64beb663745": [
                "resources/textures/images/GameEnd/GameEnd10.png",
                "cc.Texture2D"
            ],
            "eaf07895-ebbe-4eb1-88e4-c7aa675d15b4": [
                "resources/textures/MJ/my/Z_my.png",
                "cc.Texture2D"
            ],
            "ebb98c10-ffe5-4691-8e14-7a12d8c9d98e": [
                "resources/textures/images/PopupScene",
                "cc.SpriteFrame",
                1
            ],
            "ebc7eab4-4b12-4b2f-8d94-02da7e9ff15b": [
                "resources/textures/voice/v1.png",
                "cc.Texture2D"
            ],
            "ebdc2d33-bfa9-460d-884a-e53cfe537a18": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "ebe3fd18-d11e-449e-a9d7-f90daacfb94b": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "ec4501d1-8b58-467e-994a-8ecb4a0006e1": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "ec539336-40b5-4d8c-8608-9ac8d9f127ba": [
                "resources/aqsounds/voice/chat_w_2.mp3",
                "cc.AudioClip"
            ],
            "ec7d16ec-e9ab-4bde-aded-feb81e933c40": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "ecb7074a-215e-4245-976a-5e0669c94479": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "ecb830a8-95c6-4528-bb97-771d7cd13dc4": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "ecca0b6a-0bc2-4db8-a7f1-6b195c649b9e": [
                "resources/sounds/nv/hu.mp3",
                "cc.AudioClip"
            ],
            "ecd8d18c-8b8d-45a0-b0aa-0acc029f7fce": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "ecec0041-4528-48f0-8b08-49f5358014b8": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "ecf2a72a-7f6b-496d-a333-45912d28efcd": [
                "resources/textures/images/public_ui",
                "cc.SpriteFrame",
                1
            ],
            "ed65a037-64fd-4eff-b0d0-581185f2f016": [
                "resources/aqsounds/mj/w_1_2.mp3",
                "cc.AudioClip"
            ],
            "eda9a42a-6b21-4792-bcde-1916f1f76d2c": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "ee13d31d-74e0-468a-b682-55974196513a": [
                "resources/textures/AQRes/Hall/twater.png",
                "cc.Texture2D"
            ],
            "ee6e3fdb-39fd-49a5-a791-f2b6f68defb5": [
                "resources/sounds/nv/15.mp3",
                "cc.AudioClip"
            ],
            "ee8320f8-12b5-42d7-84b1-9b89a26b8781": [
                "resources/aqsounds/mj/91_1.mp3",
                "cc.AudioClip"
            ],
            "ee8e098b-39ad-4a83-8b9d-97deb1baf03d": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "eebc611c-416f-4c10-99f9-feccc2940a81": [
                "resources/textures/AQRes/MJOps/opPass.png",
                "cc.Texture2D"
            ],
            "eef02d27-5721-44e9-8461-3de54269fc3a": [
                "resources/textures/chat/chat_easychat.png",
                "cc.Texture2D"
            ],
            "ef0b2857-5785-46e2-b02c-63f7171098ab": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "ef4d9a67-ae51-437f-a52b-7bd3c7a13a73": [
                "resources/aqsounds/mj/w_13_2.mp3",
                "cc.AudioClip"
            ],
            "ef96c12a-c070-409f-b419-44632e9f1fac": [
                "resources/sounds/nv/27.mp3",
                "cc.AudioClip"
            ],
            "efa3a4b6-73d2-4a41-80e1-d2552639d22d": [
                "resources/aqsounds/mj/ta_1.mp3",
                "cc.AudioClip"
            ],
            "efcf61ec-d603-43b6-b57d-aecfbe86490b": [
                "resources/textures/AQRes/CreateRoom/mkRoll",
                "cc.SpriteFrame",
                1
            ],
            "efe76235-f585-45dc-9a59-cc7b4e983769": [
                "resources/textures/MJ/left/Z_left2",
                "cc.SpriteFrame",
                1
            ],
            "efed6528-583f-40c3-a9f2-d86923895580": [
                "resources/textures/AQRes/MJOpsNew/opBuXian.png",
                "cc.Texture2D"
            ],
            "f04353b2-89ae-42ca-a5b6-e360278929e6": [
                "resources/aqsounds/voice/chat_m_17.mp3",
                "cc.AudioClip"
            ],
            "f0b401d6-bada-4429-a6d6-dba60bf58966": [
                "resources/textures/AQRes/GameOver/gameEnd.png",
                "cc.Texture2D"
            ],
            "f134d6f5-5f2c-4d67-b979-52b459394449": [
                "resources/textures/images/createroom/creatroom5.png",
                "cc.Texture2D"
            ],
            "f16c2d61-6d80-4487-a008-2735ef49c00b": [
                "resources/textures/voice/dialog_loading_bg.9",
                "cc.SpriteFrame",
                1
            ],
            "f16d11a0-d975-4d4e-8213-9dd82c0a7c83": [
                "resources/textures/AQRes/GameOverOps/mkGuan.png",
                "cc.Texture2D"
            ],
            "f185a906-ccba-4a6e-9f1d-fd84839c25e8": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "f18f83f8-5417-44a2-bc50-45fea2931af1": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "f1dd26da-9d37-4f33-9643-9053183bd76e": [
                "resources/textures/images/PopupScene/PopupScene1.png",
                "cc.Texture2D"
            ],
            "f1def87b-3143-40aa-93a1-57d684dc31dc": [
                "resources/aqsounds/voice/chat_w_12.mp3",
                "cc.AudioClip"
            ],
            "f24db379-9aa2-49eb-8171-b00f87915ffe": [
                "resources/sounds/nv/31.mp3",
                "cc.AudioClip"
            ],
            "f2666293-f619-40bd-850e-04cb601c4f23": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "f273fe57-f086-4c03-bf25-d4e68cc567fe": [
                "resources/textures/images/efx/zimo_glow2.png",
                "cc.Texture2D"
            ],
            "f2855192-6250-41a5-8b9f-fe2b02040bba": [
                "resources/textures/images/playScenesc/wenzi005",
                "cc.SpriteFrame",
                1
            ],
            "f28e4d0e-ea5f-49ae-ab1d-70f531e28076": [
                "resources/textures/MJ/right/Z_right2",
                "cc.SpriteFrame",
                1
            ],
            "f2ae4629-f9e0-4e01-8b47-74fb2b92d719": [
                "resources/aqsounds/mj/w_2_1.mp3",
                "cc.AudioClip"
            ],
            "f2c8ad59-ac6e-4c37-a04f-b0b42fac3432": [
                "resources/aqsounds/voice/chat_m_13.mp3",
                "cc.AudioClip"
            ],
            "f2edef3e-a500-4585-b39d-8dae2b4cc531": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "f2fd3b02-16b0-4352-90fd-894cb73eb272": [
                "resources/textures/images/Login/btn_checkbox",
                "cc.SpriteFrame",
                1
            ],
            "f46d3832-43fd-41cb-971f-fc4a11338769": [
                "resources/aqsounds/voice/chat_w_15.mp3",
                "cc.AudioClip"
            ],
            "f4a71a07-1bc0-459d-8e9a-07a7ee56485a": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "f5344e34-1712-401d-8b26-5da049e24ed6": [
                "resources/aqsounds/mj/w_23_2.mp3",
                "cc.AudioClip"
            ],
            "f55810c1-b53c-4a5f-a9c6-8a9190b30b70": [
                "resources/textures/AQRes/MJOpsNew/opXianNing.png",
                "cc.Texture2D"
            ],
            "f5d20f1c-71f8-4800-899a-c80e99725325": [
                "resources/textures/voice/cancel.png",
                "cc.Texture2D"
            ],
            "f61f86a6-5dce-4f73-b12b-d3053bb15aa8": [
                "resources/textures/AQRes/BtnPNG/消息",
                "cc.SpriteFrame",
                1
            ],
            "f64c1465-0fda-4a52-b12f-1906900281ce": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "f6c224f1-be43-4b53-a56b-a75f5001c952": [
                "resources/textures/bk/btn_return_room",
                "cc.SpriteFrame",
                1
            ],
            "f6e00b41-4095-441b-b760-3fac8e8379df": [
                "resources/textures/images/playScenesc/sichuan_room_xiaoxi_002.png",
                "cc.Texture2D"
            ],
            "f71d47b7-0979-48b1-ac1a-206076b00220": [
                "resources/textures/images/PopupScene.plist",
                "cc.SpriteAtlas"
            ],
            "f78391f5-138d-44c1-9755-3a9b027b0f28": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "f7892218-ddd5-4fb8-b155-0a1be5f1d41a": [
                "resources/textures/AQRes/GameOver/bgWinner.png",
                "cc.Texture2D"
            ],
            "f7a90881-3cc3-4e4a-a5cd-4e0892e94a2d": [
                "resources/textures/images/playScenesc/sichuantouxiang002",
                "cc.SpriteFrame",
                1
            ],
            "f7c3a7c5-24ca-4aec-8c63-63999772365c": [
                "resources/textures/images/status/xinhao1",
                "cc.SpriteFrame",
                1
            ],
            "f7e60ff2-0e18-4a73-b54f-ef4ee43fb020": [
                "resources/textures/MJ/right/Z_right2.plist",
                "cc.SpriteAtlas"
            ],
            "f824f614-8f4b-4f99-b85d-4845c8039956": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "f8357817-9926-4db9-a88c-9a171b5d3fea": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "f87fb18f-2ad0-40ce-92de-be63d7251bad": [
                "resources/textures/AQRes/GameOver/imgLose",
                "cc.SpriteFrame",
                1
            ],
            "f89760d5-2554-46f1-ab00-c302382d8f11": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "f8d26189-7b57-4501-8aff-62dfcacef870": [
                "resources/aqsounds/mj/w_17_1.mp3",
                "cc.AudioClip"
            ],
            "f915730a-c92b-47a6-a1dd-07391d598bbb": [
                "resources/aqsounds/mj/w_18_2.mp3",
                "cc.AudioClip"
            ],
            "f92c3b49-0928-43af-a90c-fa2032436d9f": [
                "resources/aqsounds/mj/8_2.mp3",
                "cc.AudioClip"
            ],
            "f94a44c9-6fb5-471d-a3e3-39fbaba360e0": [
                "resources/textures/MJRoom/Z_corner_rbottom.png",
                "cc.Texture2D"
            ],
            "f9560c03-fff5-4eed-9035-0ec5e547e19c": [
                "resources/textures/MJRoom/Z_wifi",
                "cc.SpriteFrame",
                1
            ],
            "f9cc8afd-c81c-44d8-a2f0-d0806ba6b0ee": [
                "resources/textures/AQRes/MJOps/opBuXianNing",
                "cc.SpriteFrame",
                1
            ],
            "f9db24e2-be19-47f8-9e10-2f946fd74c58": [
                "resources/aqsounds/mj/6_2.mp3",
                "cc.AudioClip"
            ],
            "fa023ec7-866f-4c9e-a690-a999b2b83093": [
                "resources/textures/AQRes/MJOps/opTaPai",
                "cc.SpriteFrame",
                1
            ],
            "fa12fbe4-a9f9-45ed-9abb-f44fd8887906": [
                "resources/textures/images/history_record",
                "cc.SpriteFrame",
                1
            ],
            "fa357fd1-5635-4923-9dd4-26229d033b1e": [
                "resources/aqsounds/mj/chi_1.mp3",
                "cc.AudioClip"
            ],
            "fa3a8d3d-102d-4c8c-afc5-b1c44ebebddc": [
                "resources/textures/AQRes/MJOpsNew/opHu",
                "cc.SpriteFrame",
                1
            ],
            "fa3d3893-c2fb-444d-8697-8a7fd8216a18": [
                "resources/textures/MJ/my/Z_my2.plist",
                "cc.SpriteAtlas"
            ],
            "fa43d352-05ef-40df-87a8-60ddfc6532d8": [
                "resources/textures/MJ/mjEmpty",
                "cc.SpriteFrame",
                1
            ],
            "fa744098-e9fa-426a-81b6-1ee710a2ba9c": [
                "resources/textures/bk/LOGO.png",
                "cc.Texture2D"
            ],
            "faacbdb0-134f-4cf3-9b59-4e1f06c3db66": [
                "resources/textures/voice/adj.png",
                "cc.Texture2D"
            ],
            "fab8a700-71e3-415c-9af5-27398c333981": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "fae99843-dafe-4d6e-9c9a-60e571168533": [
                "resources/textures/AQRes/GameOverOps/txtZJ",
                "cc.SpriteFrame",
                1
            ],
            "faf72d39-16af-409d-8a67-4d22b2ccf088": [
                "resources/textures/AQRes/CreateRoom/nv3.png",
                "cc.Texture2D"
            ],
            "faf9e5db-6959-4d2b-9a87-461a837d130f": [
                "resources/textures/voice/yuyin_bg",
                "cc.SpriteFrame",
                1
            ],
            "fb284fb4-227c-4d21-bc7c-510f804fa112": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "fb70909f-8fd7-45c3-9a8a-409e63c6d6e5": [
                "resources/textures/images/efx/peng_glow2",
                "cc.SpriteFrame",
                1
            ],
            "fb9a2d3b-1b7d-40da-8aaa-007dc3ff5910": [
                "resources/textures/images/chat",
                "cc.SpriteFrame",
                1
            ],
            "fbc3f897-47f6-462d-8d63-0e9192097dc3": [
                "resources/textures/MJ/bottom/Z_bottom2",
                "cc.SpriteFrame",
                1
            ],
            "fbc662c4-dc0c-4ce7-b019-6e2863e10958": [
                "resources/aqsounds/mj/4_2.mp3",
                "cc.AudioClip"
            ],
            "fbcbc31f-1c06-473c-bbc1-0d39efdd7445": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "fbd4f585-4b9f-483f-82ab-02085b92b38e": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "fbdedaa2-842f-4ae9-b95c-5daef3d014c9": [
                "resources/textures/MJ/my/Z_my2",
                "cc.SpriteFrame",
                1
            ],
            "fbfde31f-b206-4b60-8120-809d7c51d59b": [
                "resources/sounds/fix_msg_9.mp3",
                "cc.AudioClip"
            ],
            "fc248c7f-23ff-4019-bfb9-08be4c5722de": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "fc4b62c0-fc72-4386-ab3d-e184b06bc9d7": [
                "resources/textures/AQRes/CreateRoom/麻将牌ICON",
                "cc.SpriteFrame",
                1
            ],
            "fc6eb4ff-c798-4c8c-bea3-6b08ded292fa": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "fc8d435b-7d57-43bf-80b1-b0e1d77eeafd": [
                "resources/textures/images/public_ui",
                "cc.SpriteFrame",
                1
            ],
            "fc8f87c8-a6dc-4a3c-9bbc-7207a9e5d7aa": [
                "resources/textures/ShaiZiAni/dice",
                "cc.SpriteFrame",
                1
            ],
            "fcb826b8-dcca-4795-b94d-ffea6eb25350": [
                "resources/textures/MJ/my/Z_my",
                "cc.SpriteFrame",
                1
            ],
            "fd15829e-3860-49b5-9e85-909d58750275": [
                "resources/textures/chat/emoji_action_texture",
                "cc.SpriteFrame",
                1
            ],
            "fd1eb247-5106-4745-b234-94b01f2badb3": [
                "resources/textures/MJ/left/Z_left",
                "cc.SpriteFrame",
                1
            ],
            "fd379b81-c82c-419c-b55a-7acd43dfbbf7": [
                "resources/textures/images/GameEnd/GameEnd6.png",
                "cc.Texture2D"
            ],
            "fd62cb57-e79b-4fb0-865a-bce7bf226a16": [
                "resources/textures/images/efx/guafeng2",
                "cc.SpriteFrame",
                1
            ],
            "fd8ef505-9fe0-4a2e-a8b6-718a181eb2ec": [
                "resources/textures/AQRes/MJOps/opGang.png",
                "cc.Texture2D"
            ],
            "fda88b04-ad2f-46cb-a509-cbdfb2217ebe": [
                "resources/textures/AQRes/BtnPNG/分享.png",
                "cc.Texture2D"
            ],
            "fdb663a5-8b5d-46dd-81b8-f03217e77e3a": [
                "resources/textures/MJ/right/Z_right",
                "cc.SpriteFrame",
                1
            ],
            "fdc47372-07a8-4b03-8fe9-d2d9cf215b98": [
                "resources/textures/images/JoinRoom/Num10.png",
                "cc.Texture2D"
            ],
            "fdd0ef1e-7b34-4e7c-8cd2-922c3e424299": [
                "resources/textures/MJ/left/Z_left2.plist",
                "cc.SpriteAtlas"
            ],
            "fe0166cd-37e8-43c0-9e3f-b619fa827103": [
                "resources/aqsounds/mj/15_1.mp3",
                "cc.AudioClip"
            ],
            "fe25f3b3-ab7c-48a6-ad17-56de32613c54": [
                "resources/textures/images/GameEnd/GameEnd12",
                "cc.SpriteFrame",
                1
            ],
            "fe7898db-1398-455c-95f6-a52931b456c0": [
                "resources/textures/images/status/xinhao3",
                "cc.SpriteFrame",
                1
            ],
            "feeb6b81-8498-4125-94ce-1528510279c3": [
                "resources/textures/chat/playerinfoline",
                "cc.SpriteFrame",
                1
            ],
            "fef69d00-9cf6-4d00-b7ae-bebb21662a8e": [
                "resources/textures/images/createroom/creatroom17",
                "cc.SpriteFrame",
                1
            ],
            "ff112882-d1ec-4ee1-92ad-c2778e6b14e4": [
                "resources/textures/MJ/bottom/Z_bottom",
                "cc.SpriteFrame",
                1
            ],
            "ff23e3fc-4306-4ac8-8ca6-6a6b8c60a6ba": [
                "resources/sounds/give.mp3",
                "cc.AudioClip"
            ],
            "ff360503-52f4-4bcb-9760-21032e85ef6f": [
                "resources/textures/images/play_scene",
                "cc.SpriteFrame",
                1
            ],
            "ff4e6d82-2644-4e4b-9fe3-2ed10ec4429f": [
                "resources/textures/images/JoinRoom",
                "cc.SpriteFrame",
                1
            ],
            "ff636352-a182-4f78-943a-268eda48dd47": [
                "resources/aqsounds/mj/24_2.mp3",
                "cc.AudioClip"
            ],
            "ff7ce0bf-7443-45b0-9431-26f3113a7b6c": [
                "resources/aqsounds/voice/chat_m_6.mp3",
                "cc.AudioClip"
            ],
            "ffd781e3-3d89-49a1-99af-45446e138558": [
                "resources/textures/images/playScenesc/sichuan_room_fangxiang_001",
                "cc.SpriteFrame",
                1
            ]
        },
        "internal": {
            "0275e94c-56a7-410f-bd1a-fc7483f7d14a": [
                "image/default_sprite_splash.png",
                "cc.Texture2D"
            ],
            "617323dd-11f4-4dd3-8eec-0caf6b3b45b9": [
                "image/default_scrollbar_vertical_bg.png",
                "cc.Texture2D"
            ],
            "6e056173-d285-473c-b206-40a7fff5386e": [
                "image/default_sprite.png",
                "cc.Texture2D"
            ],
            "71561142-4c83-4933-afca-cb7a17f67053": [
                "image/default_btn_disabled.png",
                "cc.Texture2D"
            ],
            "b43ff3c2-02bb-4874-81f7-f2dea6970f18": [
                "image/default_btn_pressed.png",
                "cc.Texture2D"
            ],
            "d6d3ca85-4681-47c1-b5dd-d036a9d39ea2": [
                "image/default_scrollbar_vertical.png",
                "cc.Texture2D"
            ],
            "d81ec8ad-247c-4e62-aa3c-d35c4193c7af": [
                "image/default_panel.png",
                "cc.Texture2D"
            ],
            "e851e89b-faa2-4484-bea6-5c01dd9f06e2": [
                "image/default_btn_normal.png",
                "cc.Texture2D"
            ]
        }
    },
    "launchScene": "db://assets/scenes/start.fire",
    "scenes": [
        {
            "url": "db://assets/scenes/start.fire",
            "uuid": "7898c3aa-75db-4523-8b22-e60bf7d83784"
        },
        {
            "url": "db://assets/scenes/createrole.fire",
            "uuid": "5fad28f0-3c44-4f5e-9351-381fe3fb522c"
        },
        {
            "url": "db://assets/scenes/hall.fire",
            "uuid": "efb1084f-0b5a-4980-b187-1e949555cb55"
        },
        {
            "url": "db://assets/scenes/loading.fire",
            "uuid": "4a1acad9-ce88-4334-aa62-bf23a5ccc79e"
        },
        {
            "url": "db://assets/scenes/login.fire",
            "uuid": "7b486d14-13bc-4be8-91e6-2efa50fc8715"
        },
        {
            "url": "db://assets/scenes/mjgame.fire",
            "uuid": "d092d418-dcdd-4823-a232-eca65fe76fff"
        },
        {
            "url": "db://assets/Scene/helloworld.fire",
            "uuid": "2d2f792f-a40c-49bb-a189-ed176a246e49"
        }
    ],
    "packedAssets": {
        "01147d115": [
            "30220141-dcea-4b2c-82ea-f6932b703abd",
            "33a87e63-83e7-4f98-a40e-96feece06697",
            "3dad14aa-1217-43e2-a654-c22ae3a85d84"
        ],
        "01401b1e0": [
            "0f2c2d0c-5e50-4083-95fa-24318c3ec045",
            "3397af2e-5ee1-4618-8617-928a579a7b2b",
            "39c35782-a56c-44a0-8180-f73c78fe44cd",
            "493590e2-08a1-4d4d-bae5-dde74df63fd6",
            "515b0125-af3a-4931-aba8-a0cfccea10d8",
            "53f7829c-0db7-4721-860d-0b5750394369",
            "6444fd19-c8c7-489e-9f94-6c84563ca6a7",
            "69891e8b-d860-4e69-84e3-5508c591d72d",
            "756d3eda-86e9-4dbe-9b08-4578da7112c8",
            "7b28fa7e-44ac-4ad4-af05-c55655fc9026",
            "7c1e8c3f-c203-44f6-b6d7-00c636842c27",
            "7ec892c4-3541-4d63-a2de-913c96695f31",
            "7eecafa6-b14f-4298-a71a-bc2c013f9fba",
            "980b840d-604a-4098-aba3-1dd2edca1a4c",
            "af18652e-cd2e-411f-bfc0-cb7496293145",
            "b30dea44-8f96-492e-b068-3d78fc87768a",
            "c4d115bb-13ff-4b23-b77e-4c8cda3419b1",
            "c97c08d8-0928-41f9-a577-05f4d19b31aa",
            "e3c02479-1e32-4ed9-92db-bd71fe76c9f4",
            "fc8f87c8-a6dc-4a3c-9bbc-7207a9e5d7aa"
        ],
        "014612121": [
            "001488ed-61c9-40a1-a7e0-9152dfe8df0e",
            "08aa8adc-4f84-4b32-bc75-bb402d5b134b",
            "1aecfa70-0b27-44ba-b6c5-65efb20c5948",
            "1cbd6b1a-6cd4-4f59-a49f-53005d224e95",
            "20e91468-a3cc-4bcb-b150-8cc19161b1ba",
            "2a712174-365d-40a5-8c39-85860c809e56",
            "3a4bc43a-320e-496b-84e3-9cd7b192cafa",
            "4226dac1-b3ec-43e0-bd22-9fe16f8c4de7",
            "4424c05a-7f85-4e5f-8c8c-a8102270bbf1",
            "4c4a9d3c-37b6-4aa6-b692-8e04c6818225",
            "4d42ddcf-8e6c-4396-bf69-26dd2b288e2b",
            "58189fa6-582c-4bc1-8818-d64112bed4e1",
            "5ab46b86-6642-499f-89ea-b70b7ce6752e",
            "5ea4cbbf-d3ad-469d-8488-e3b5c9e14d66",
            "5ebc8906-0653-4bf2-9325-d4368a3d46db",
            "6ed16926-a1df-40e2-b4c0-b9c2bf4a2aeb",
            "815524ef-1c74-45c5-9b08-27960b851e85",
            "836d2a01-b443-4b1a-92ca-642a907122fb",
            "89250d26-0fc2-447e-bf18-58c7af624cb7",
            "8a1ef75b-231d-4577-aad3-4ccce7a1cd95",
            "8a7516b1-6171-4406-8413-297aa4123326",
            "8e0d9625-3f6f-46a6-861b-b5301050174c",
            "9639ea2b-ad15-48ac-9335-68c6e2f45330",
            "96934e02-32aa-4e99-9cbc-de01b7dff81b",
            "a8be677d-5057-42f3-9c45-ef5b5595b5f5",
            "ace46e09-37ab-4607-9282-99981f565cef",
            "b191c7fe-e16c-496a-94df-6cfd5865f8f2",
            "b8d90545-c2e4-408b-891b-bc8a6f2f72ac",
            "ba318dd9-5759-42a3-9d92-dd5765808651",
            "be329022-a763-47b1-a16e-55e317f4464a",
            "c80abbb2-c412-4736-96c6-734930fa9dd9",
            "cead5d37-33b1-4d99-a054-e64168a887f6",
            "d0dfc100-c4be-45de-8df6-1c51de881c06",
            "d98016fb-e005-46b8-b1f2-b7934b265254",
            "e015c7f1-cd70-4c94-b381-5f910532e4cc",
            "e37d1ee9-ac92-47c6-a2a7-66a3c524f7df",
            "ef0b2857-5785-46e2-b02c-63f7171098ab",
            "f824f614-8f4b-4f99-b85d-4845c8039956",
            "ff112882-d1ec-4ee1-92ad-c2778e6b14e4"
        ],
        "0172659ba": [
            "05b80eb8-2054-4f28-9be0-36044421240d",
            "0a6fef68-87a0-430b-abe7-82b0526bb5bc",
            "312c5295-8210-4203-b06e-c72d765f0928",
            "397f7028-2225-43bd-9c38-80f857a464cb",
            "50e77c44-284c-44b7-8797-2a246a7b60fd",
            "5df0fb42-67cf-4df8-8ba4-539ef53a3ba7",
            "718e98bd-522b-4ff0-b5cd-e955aa0d620b",
            "7c699688-794b-436c-92aa-8044ae3896f0",
            "8c2e8172-65aa-4d5f-ad62-5383e3f1f939",
            "99590587-bbe2-476a-8f41-0ee43fb6e872",
            "afc96215-f657-4e45-b5e6-ff5152f52566",
            "d2d631ae-5272-47d1-9604-e3ec6d209b9f",
            "dd067b6a-d750-478c-b239-a845ef554ef7",
            "e8d9f695-ac2f-43dc-b9a0-b51e9531a700",
            "ecf2a72a-7f6b-496d-a333-45912d28efcd",
            "fc8d435b-7d57-43bf-80b1-b0e1d77eeafd"
        ],
        "019d923a6": [
            "10c6b231-c25e-4eab-808c-f107e0e66dff",
            "1272c881-cc49-4cca-be3f-1dfd7be3a100",
            "1be49286-2bf6-4081-9470-97cad682f5f9",
            "31e651f0-4cf7-4da8-a63c-d112bfb9f786",
            "d3d93b91-5ef4-40df-9456-d6915b2098a5",
            "f8357817-9926-4db9-a88c-9a171b5d3fea"
        ],
        "01a3d61c7": [
            "03dd702a-324a-4cf4-ba00-244c404b1ca3",
            "0f2c2d0c-5e50-4083-95fa-24318c3ec045",
            "0fd65e09-64e6-4139-94e5-7e0970d0dfc5",
            "1839fc15-5c8d-41e1-9322-b5a6a323f491",
            "18fed740-e8f3-4349-8e97-73673edef33e",
            "1de08c57-3928-4b19-bfbe-1180881ae115",
            "2b2161e2-b451-4940-9cea-5b617bd0d91f",
            "3397af2e-5ee1-4618-8617-928a579a7b2b",
            "3757bf86-3fac-4cbc-9556-93e763dd0c1a",
            "39c35782-a56c-44a0-8180-f73c78fe44cd",
            "493590e2-08a1-4d4d-bae5-dde74df63fd6",
            "515b0125-af3a-4931-aba8-a0cfccea10d8",
            "5359c4d0-0996-45dc-b404-acf132ba9cf6",
            "53f7829c-0db7-4721-860d-0b5750394369",
            "53fe22cf-ae48-47c8-8ce0-3ac782cedcc9",
            "618fff69-1cf5-40a9-b30f-4280b73cec6b",
            "6444fd19-c8c7-489e-9f94-6c84563ca6a7",
            "657b7db6-b17b-44ab-8ad0-83bd3d733a0d",
            "69891e8b-d860-4e69-84e3-5508c591d72d",
            "6fd800e5-bfae-4081-a4ad-e036ab366470",
            "70d2aa00-55f9-45fd-9fbf-33fae2a52aac",
            "70ffb9be-b583-44af-a555-cb85d8390e39",
            "756d3eda-86e9-4dbe-9b08-4578da7112c8",
            "758a5f69-3874-4dc7-9f99-14e171d7305b",
            "7ae049d9-857e-4d3c-8997-fa1fe9b46500",
            "7b28fa7e-44ac-4ad4-af05-c55655fc9026",
            "7c1e8c3f-c203-44f6-b6d7-00c636842c27",
            "7ec892c4-3541-4d63-a2de-913c96695f31",
            "8d5677bd-d6b5-4b79-ab90-74fe525c4b67",
            "8e76b616-7ec0-4c9e-96e9-0496f443ae13",
            "962e3375-0506-4086-aa57-b1a1d5d3bc9c",
            "9770bb1e-26a5-4105-b369-25491c6ce586",
            "980b840d-604a-4098-aba3-1dd2edca1a4c",
            "9adbdc97-5b89-48bc-9769-9c9f54f950ac",
            "9eac6e81-9a8e-40c0-afb6-ccfdeb75362e",
            "a27b9173-533c-4b87-8ac9-87f36df61b8d",
            "a9cb833c-608a-4cd0-8cb1-ae3604023ce9",
            "af18652e-cd2e-411f-bfc0-cb7496293145",
            "b30dea44-8f96-492e-b068-3d78fc87768a",
            "b686c99a-593f-447a-8a21-d096c0fc4da0",
            "c1465d41-eb74-48aa-a3a2-e002d38185f4",
            "c4d115bb-13ff-4b23-b77e-4c8cda3419b1",
            "c5d05eae-3036-461d-a295-3fb72044ef95",
            "c88c7f40-f82e-468d-856b-1b9c0150791b",
            "c97c08d8-0928-41f9-a577-05f4d19b31aa",
            "cc6528c2-c73e-4073-acc8-7c5e470a55a7",
            "cd2a8d74-a66a-4bea-b642-acc37b231b73",
            "d26b0b0d-6f06-43e6-81dc-2445fc014a43",
            "e3c02479-1e32-4ed9-92db-bd71fe76c9f4",
            "ebdc2d33-bfa9-460d-884a-e53cfe537a18",
            "f18f83f8-5417-44a2-bc50-45fea2931af1",
            "fc6eb4ff-c798-4c8c-bea3-6b08ded292fa",
            "fc8f87c8-a6dc-4a3c-9bbc-7207a9e5d7aa"
        ],
        "01b3352cd": [
            "088725d4-b66d-40d0-a3fe-aeeec07307b5",
            "0e1a1779-cd6d-466b-880c-fd89275274fa",
            "17c15905-21e5-4911-b272-afa575f1d9c2",
            "1a96991d-0fec-46e8-a593-768de4e1d105",
            "24bd55f3-2acf-46f4-99b5-7e32b4127773",
            "2c9aa949-8b8e-4dc3-a42e-88939ee2e84d",
            "360eb57b-9704-40f6-9284-c2593ebbc324",
            "40a7ce28-a17d-4952-8d33-6853ee0d060c",
            "4ac30c8e-023a-4e83-aa95-9022172c9bee",
            "516faa4e-cd1e-4501-b5e3-6f83650549c4",
            "59409a24-303b-4212-b2c3-d3ae09c28d7f",
            "6120fc28-be94-496c-9a0b-e636a54ef049",
            "61749169-1f57-43e2-91d0-e473992efe49",
            "62ce077e-9af1-4617-ab01-50d67a3b457a",
            "74b83ad5-4e4d-46db-8222-cc98a66cddef",
            "78e898c8-ab73-4740-912b-74f95a405d93",
            "8350eb62-c416-4e8a-bb91-30762b045da6",
            "83a3e8f0-e9d7-4eab-a67a-cf9bc5ae69a5",
            "893d538d-23fc-4e55-8669-b717cb905636",
            "8976316c-cae9-43b9-8fa0-36532ccced65",
            "8ccc7c83-6ae1-4396-b287-d7cdf5dd87cc",
            "8f63fcfb-d714-44db-b840-2cde17648738",
            "9c8e2f45-a85d-4b5a-9347-5868f3e355bc",
            "9cc2dc4e-e55c-498b-83b1-ce01f3804123",
            "a1aca5ef-b367-4b65-ab8e-e567e039e9c3",
            "ab2500fa-9ff8-4489-a1c7-5febd50136b7",
            "af1c4afa-6fe9-433e-a9eb-5fb2e7086fd6",
            "b0ba2ebe-a39f-473f-8de1-4b49c8690940",
            "b2bde301-3bb6-4d07-a5b0-99cafcbec717",
            "bb5e640f-7f9f-48c5-8695-deb1e57f0a91",
            "bfab0cb0-774f-4c38-8263-4ed7c6c763f2",
            "caeb40a8-4f48-4fcc-b20e-2406ff408450",
            "cba1400b-4e40-44d4-a288-1d74f6757332",
            "d53f742d-e434-4147-ae95-498fda6957bb",
            "d968d96e-fe8e-4332-9646-a558343b20ee",
            "dbe2b062-0b7c-424c-b928-84658f4324e4",
            "dfc88041-d607-409f-bc18-d65dc7226af1",
            "e1b15a33-b97d-4e1a-b851-6fd562170671",
            "ec4501d1-8b58-467e-994a-8ecb4a0006e1",
            "eda9a42a-6b21-4792-bcde-1916f1f76d2c",
            "fbcbc31f-1c06-473c-bbc1-0d39efdd7445",
            "fc248c7f-23ff-4019-bfb9-08be4c5722de",
            "fcb826b8-dcca-4795-b94d-ffea6eb25350"
        ],
        "01cc16395": [
            "0742b288-ef58-49da-aa18-226ca275227f",
            "0d1d8c59-b160-4c74-8a8f-9d77877502fa",
            "721fb52f-dc4f-48e0-b583-43fb72c4b452",
            "b1b5f887-6e69-4701-b38c-b25252c3a569",
            "fb284fb4-227c-4d21-bc7c-510f804fa112"
        ],
        "02066d14d": [
            "30c659d8-e2e6-4820-883b-0fc87ed5fd21",
            "e0d9e42d-cdf9-4055-9da6-f806d4081402"
        ],
        "0237f11c9": [
            "150829b1-d3a8-4b43-a7d2-b894c15fb133",
            "234dedb0-678d-4893-9486-2e471a0f0a17",
            "237032f8-9668-4bdc-bc99-960e5c53bd9f",
            "23fdb5af-9999-464a-9131-bcf186d31075",
            "249efc01-de45-4d92-8851-12b8e612751d",
            "28cd14db-6ba3-458d-bcb3-ea53a7adddc3",
            "3329caac-ff7b-4e43-815f-ae3ce3d83c7a",
            "3479e792-95f8-4d4d-b196-2f8c6a125cd4",
            "38be1123-65ab-4374-9b79-5ba993c9e089",
            "3a3a48a1-be45-46c4-b82b-1b5d10a7132e",
            "3e01c30e-044d-4d20-9cb9-05c97a4bee7a",
            "40070852-dace-4de0-976c-f37aa482b996",
            "4201ef1f-4abc-4af0-8e84-e9e776943900",
            "4338b286-ca55-4eec-877f-ca22150bc313",
            "49b48549-9276-4962-bce2-5c6733965a79",
            "535753f9-162b-4819-969d-6726b942ab53",
            "56958185-085b-40b3-8c94-6e58181b7ca5",
            "5e5d01f9-5a85-4abc-9330-a89e7f44dbbb",
            "69d89393-64d9-47ff-8411-f7147b162086",
            "7a731f97-f192-4f9b-ba2f-a5f7e6fa9b7a",
            "7ce05240-d123-4f98-9258-7c67fee072b5",
            "87349c4b-caa4-428d-ae53-549b97438ce4",
            "8c3ba05e-1fbf-4af6-934e-40020fa8a371",
            "8d09e976-25a0-4c78-81cd-65fac8505830",
            "9fdbad27-6226-4502-a818-f93f2d543cf8",
            "a230e2f8-3203-4f06-8299-6a187f59b5eb",
            "b35a9d33-5790-459f-950c-eee12b27056c",
            "b3daa091-3e8e-4358-8255-7543c401efd7",
            "b4cd8eb2-b901-43e6-85c2-8791641cecaa",
            "b8bed57c-7c55-41cc-8f87-b746d4bf0c73",
            "bd840aff-153b-4609-9f4e-c203d8d31d2e",
            "c310366e-416a-4166-ba90-9b5a589f1dd6",
            "c3fde634-7152-4408-813e-581c2ca09f45",
            "c6792fab-a883-4d0f-8ec5-ad4d738671cd",
            "cedd15d4-6953-4ab8-b2c3-db44e1d961f6",
            "d4ee4a99-35be-459e-8221-3d7064ad0051",
            "dc8aaac9-73a5-48aa-b04c-6679debab83f",
            "ddc89083-5194-4822-a109-4a65e07411d2",
            "ea7d8d70-11a2-48fd-8f20-82bd49ffce05",
            "ecb830a8-95c6-4528-bb97-771d7cd13dc4",
            "ff360503-52f4-4bcb-9760-21032e85ef6f"
        ],
        "025f0977d": [
            "563ab8c2-4c26-441a-b04b-8ab369d4948d",
            "8c2e8172-65aa-4d5f-ad62-5383e3f1f939",
            "99838854-3a79-4687-ac8d-5484f3c4f6aa",
            "9bbda31e-ad49-43c9-aaf2-f7d9896bac69",
            "a23235d1-15db-4b95-8439-a2e005bfff91",
            "afc96215-f657-4e45-b5e6-ff5152f52566",
            "e5e1100f-092a-4124-836e-e492a6352765",
            "ecf2a72a-7f6b-496d-a333-45912d28efcd"
        ],
        "02a41c64b": [
            "2ad8e984-fb49-4a51-ac0f-46bb27251f6e",
            "38d62777-d02a-4b17-a3f0-493cc47d334e",
            "667bb892-2b32-4444-9750-6c7dfa65b275",
            "72f3f982-810b-4ca7-8662-602d2424e179",
            "7498f8b2-3d65-47a5-a5dc-3f484a6e5f42",
            "dfd021b3-5669-4e73-8a28-5e16a579e110",
            "e4237ef8-fd32-4874-9ab0-6f94b5393b8b"
        ],
        "02e35e096": [
            "02491912-f027-4dce-88b9-9296333c8dee",
            "118e175e-deaa-47da-8f22-aa3b92233165",
            "147fe42f-7640-4b8a-adf2-b4097e0c7e65",
            "23790826-1d37-4f95-8765-1a77abc15c3f",
            "24670f05-f6d6-42a2-b7a4-ef9e463011f7",
            "3c83a88f-ffc9-4db5-8925-323eba63cccb",
            "3e2ce021-e723-452d-94ed-bb743dfdc7ee",
            "4db720d9-b5ed-47db-b457-e7dff65d0c6f",
            "52a1abf5-8762-4bb7-82dd-c6c7afc88610",
            "653c33e2-391d-4d4b-b922-e48e6b9f3a42",
            "792edbfd-c7c3-4fbc-af11-abc267ab9d7d",
            "79ca1461-bcce-4e92-a148-bbd4884778f1",
            "9b5079b4-d62b-4dcb-95c2-27e71c3d757d",
            "9ef235e3-46fd-45ef-8e0a-d6531e7f1ffb",
            "9fe76483-1a41-4ec8-9779-147f104cf4f7",
            "9ff38dc8-149e-464e-b574-d093ce5967d5",
            "b03dc8dc-0c47-41b0-96e5-e638457d4fa4",
            "b0737cf8-25e7-4122-a577-66181b3ffbb8",
            "b824bcbe-b02d-4032-9eee-253b4368c088",
            "b874b9e3-55b7-46b5-911e-46d8e00ae34d",
            "c2236cae-cdc3-41d4-a58f-b5b4e18fb6e2",
            "c35d6dfe-70ee-4bc0-8bc8-38b9ec4618f2",
            "cf017ebf-8a52-4b86-b6b8-e84ad1dd9ff0",
            "d2bf07d6-cbaa-485a-9e05-b9409bf16b62"
        ],
        "02f0fab50": [
            "09bfe702-65eb-47f4-9ccb-7593d9233e0d",
            "0acd8ea4-0cc4-4240-9438-b5591b52d6bb",
            "498383c9-347c-434f-84e3-53e4f304d72a",
            "66a82289-42b1-4fb6-a223-220e32129cfa",
            "7f5d1ce1-ad62-48c3-891d-2feb0457a744",
            "8ee53e49-397a-4974-b996-923c19d3cf0a",
            "8f8b031e-48ad-4814-8eb0-0e51c2961831",
            "95c85b47-abd5-4ff1-bf7f-7ec7cbb6e596",
            "9d456e8d-b4a7-492e-bc9a-bd60dff93263",
            "c4d40921-9a54-4928-8409-ce5c5c456840",
            "c993886c-1bec-463c-a4dd-baab41d8173f",
            "ea0d9383-c0d7-4b7e-9fa8-4d16992991d5"
        ],
        "03073e0a": [
            "014e2b9c-aece-45ab-a588-ec19c669546b",
            "02070caa-b061-47a3-b8fe-8be4bb252b3a",
            "026b4040-ebfd-487c-a7d4-0806ea5820e1",
            "06357dfd-5b12-40cd-b94c-cfa0e20d2e72",
            "0b79f9c7-27bc-4cbe-b226-5cae256501c3",
            "0e08d727-5083-4210-872c-fd5d2faddd3b",
            "11ac894c-3582-4768-bc25-80de5f46a0cd",
            "1d8873d5-1f11-43be-b233-8047bb53fa5f",
            "1dd2be1a-f5cb-4386-a063-625c0acd8eb6",
            "2db7b3e5-2efd-409b-b434-17bd3c764929",
            "2f63db7a-573e-4821-89b2-35f72edf67f0",
            "329a4adb-a9ca-4d95-9e9b-85475a1d74ab",
            "37baaa16-b7be-4e0e-8c38-d471fd2bf334",
            "3fa8e33b-2a70-4bfb-8693-b1d1c6fbef6a",
            "4c1e3125-a324-404a-a515-639653fdeb54",
            "5322e3e8-b47b-408a-9bf0-23ec6fc1936c",
            "5703d1da-4911-4396-a82d-b6b5f0f7eab6",
            "5a8ba0b6-f0d2-40c8-a712-8a42f323c602",
            "5c27ea1c-ead5-4b7a-b7fc-60ed40a3cd94",
            "5fae1714-f8c0-4683-bef2-8ad3dc5d96a8",
            "6927a755-077d-4825-a595-3c4c0998d29d",
            "6cfad644-dfdd-40a9-b947-5644eb2906b4",
            "866ccc82-41fe-4046-9c1e-4466c7933b0c",
            "87b96191-fa48-423f-b671-8b942f81b8b0",
            "8b77d34d-496d-4e36-aa08-27096567bf0f",
            "8c082abd-20ad-4fb2-b1e2-4b99e943da66",
            "ac8be0c2-1c7f-4675-b462-dd8f52283017",
            "b23a8d60-d314-40c1-9abb-424a335ddc26",
            "b23dea61-eec0-4d8b-8397-5148277e21ba",
            "bbb5fe8c-0f31-41f3-8774-635da092fbff",
            "c682722e-501f-491f-83b9-6ac4cf7101e4",
            "c87018b3-f070-4296-89a1-b381ed5cf759",
            "d10d3fb4-7a2c-4698-96e9-77c8914eb137",
            "d1ea8ac0-7e17-4924-899e-1b2bd520fca6",
            "d39e5304-2526-469c-820e-9c156171542d",
            "d4ed868f-c4a4-4a75-9ae8-e8c005d9fa10",
            "d9eb0394-4dde-4a94-a36e-040e1b6cadfa",
            "daf4f6ca-498c-4dd4-91aa-89d63269eb85",
            "dc0f12a0-949b-450f-9d67-9f1054b115f8",
            "e1ffb160-a2ba-4b85-9133-0616b4de100c",
            "f2edef3e-a500-4585-b39d-8dae2b4cc531",
            "fab8a700-71e3-415c-9af5-27398c333981",
            "fd1eb247-5106-4745-b234-94b01f2badb3"
        ],
        "0369dd42c": [
            "0d6a1d80-5319-4829-861a-5d28bbff3782",
            "0fb27d8f-8360-4c08-92c5-2d18eb854610",
            "11241202-69c2-49ac-a07d-56a8f172c4d5",
            "19800559-9d4f-4615-a91f-83b6e82daed6",
            "1b59695e-5540-424c-9039-1f104391aeda",
            "1d09396e-f87c-4ba0-8b4b-6b1473ff279b",
            "1d96874b-13bf-4d95-be33-7d45957d435a",
            "3836d8d3-441e-4db9-a771-3d17e1233842",
            "393f1987-dda3-4e38-8936-1aa059f8ee7a",
            "48bbc34e-d304-4947-a0fc-bba08f27205b",
            "4afc8a40-c557-4507-9998-1eecb96c46e6",
            "537e486e-8ef6-48ce-8094-32aa50d9410e",
            "539d920f-2dd2-40d2-ae1c-41f880f07042",
            "5cac63a0-5716-436d-9066-8ade4e05d6a6",
            "73551241-ffce-43ee-9e14-9159c4954e57",
            "75a98499-2f66-4c05-a7c3-6efdbf2becb5",
            "7a56407b-28b3-447f-9a20-d16c0a7c03a3",
            "7c2ce215-1bd6-499a-a441-a242784bfe85",
            "7d691697-7e83-4729-a1b2-8f7ad43304da",
            "7e4b0329-9455-4d54-8daa-8e36668a4a34",
            "80236eb7-5080-4170-96ee-a8c4f8a016cb",
            "835db317-51f5-4689-9dc8-71442c726ab4",
            "870c289f-6778-4a0e-b7a7-788a831eba65",
            "9c49f08b-f821-4a09-9f66-c1711f0330e0",
            "9f844e2e-44df-45d4-bc1f-9cdef2d8d1cc",
            "a5490aed-a2bf-48f8-bfdf-5f48327a061f",
            "ac2f73dd-07af-497f-9af1-a6c12260d106",
            "af421aaf-6a5b-4663-a033-8ed750ca5a54",
            "b2a05403-3e08-4089-852f-d5b90261ade2",
            "bc866d92-d324-4d6b-91bd-5b26df85577f",
            "c3c044c5-9bab-4f50-8fe9-38982a5a648e",
            "caeffa32-dd50-48ec-9df4-2ad873d8535c",
            "cd8f59ff-9bdc-4b86-8bd8-2ed99cc3da9a",
            "d4f25282-d9d0-4d30-8b2d-0b93e761e717",
            "d5da5754-a91d-4204-8aca-5e648a4da1c8",
            "d7ee5835-6b15-4294-a79d-72574036cd3f",
            "d98c0735-3f6a-45a2-862d-db432518af85",
            "e1c391b9-3c69-4637-8876-9a1bbd585e97",
            "e8dd3c7f-2177-4c87-8ce7-9f08bc34d8bc",
            "e97f8db4-4c2b-43c9-8b16-2a064aa4c200",
            "ecd8d18c-8b8d-45a0-b0aa-0acc029f7fce",
            "f78391f5-138d-44c1-9755-3a9b027b0f28",
            "fdb663a5-8b5d-46dd-81b8-f03217e77e3a"
        ],
        "03f04aa39": [
            "ad6e7acb-acbf-4957-94e6-3e80a99794be",
            "dab6187e-44ef-407f-9621-1fc227ed6c11"
        ],
        "045cb9105": [
            "2d167769-a80b-4b3d-89f1-b150fa64008e",
            "306a0d16-09f5-4c00-9bdb-0dc53c00990c",
            "350940fd-9691-4aaf-8685-7764627333ad",
            "38744dcf-8d01-40b5-868b-e74014d78dcd",
            "47e441c3-81cc-4916-bf1c-fa13f1359c8f",
            "5914f056-b6c1-4bbd-979b-92b5b721c7f4",
            "6fb8913c-4002-42c2-9c10-e9db64c2b2a3",
            "8fa76a26-7936-4bc0-af9b-c81d5f39cb0b",
            "93c93965-d3d4-4483-a96e-497e1b243dbc",
            "999350d5-eb8e-4a6e-9ac3-f1690a3ea842",
            "9ca3657f-6216-4cb9-bc0b-f6972a05ae86",
            "a8880732-446c-4797-a83e-7b1376458309",
            "abb55ec9-187f-436e-9a1a-5ba431862e87",
            "b421470e-abca-4b84-9e4c-eab8b6b7806e",
            "b526316a-0879-4b6e-8b03-63cf91505eff",
            "bf6861d5-7745-42f4-a6a7-82278bb6d583",
            "dcfd81f9-c553-44c7-b21a-6e485107dd8c",
            "f71d47b7-0979-48b1-ac1a-206076b00220"
        ],
        "049d3e942": [
            "0470db5f-e2e7-4a23-985e-087e2faf1e35",
            "7394d8f5-2aae-4850-9a3e-e8dc71fad997"
        ],
        "04def15c3": [
            "001488ed-61c9-40a1-a7e0-9152dfe8df0e",
            "009b55b1-565e-483a-8a5c-f9195d4cfa76",
            "035608a9-3a9a-40dc-b34b-161eed24d3a9",
            "03a988d2-a7c8-4c62-b205-5e38971143f4",
            "045cbcbc-b04d-4966-942f-c5fa20e8b925",
            "06ab8343-75d9-41b2-9388-165764ded274",
            "06d4bb9e-6c43-4338-896b-376455357acb",
            "072c09ea-3414-4073-9273-97fb0f638311",
            "092833c8-627a-4f92-9774-83e743083685",
            "09e3083b-13a3-44f1-b9eb-b468cdff6aeb",
            "0c985548-c639-4c10-9952-50a1381d627e",
            "0e6b66aa-1318-43b7-8874-21b3166ecae2",
            "10b2f590-dd13-4057-9ea3-ac53c1487106",
            "111122ca-2fa5-48c3-96b5-f1b78a2a27bc",
            "135fe57a-7ae8-489e-8b86-e55f927522ae",
            "150829b1-d3a8-4b43-a7d2-b894c15fb133",
            "1517fd6b-e1d5-41e2-9ca6-c0e716735a4f",
            "180c0977-60a4-4821-b910-2a561dbe412f",
            "19902851-ccc2-4726-96c2-4d25a61d2a6c",
            "1a69f4e8-766e-4685-bb19-a37cd1138f67",
            "1b59695e-5540-424c-9039-1f104391aeda",
            "1bd05074-4290-4ce2-87a2-81b7dfa838f6",
            "1c807c1c-3b26-4bce-8480-7d0447af76c7",
            "1cc34a36-700a-461d-b136-0f8ce6b9a284",
            "1f337ec9-dafc-41c7-9f98-213d943efcb9",
            "209ec84a-5f4d-44f1-b6b3-7f06a19554e6",
            "234dedb0-678d-4893-9486-2e471a0f0a17",
            "2397833d-46a0-4fff-b8ab-422b96e2c4df",
            "249efc01-de45-4d92-8851-12b8e612751d",
            "278637fb-c606-45a9-8ebd-7772964f75db",
            "2822bb44-286f-40a1-9398-35a3bb0bd761",
            "29158224-f8dd-4661-a796-1ffab537140e",
            "2d167769-a80b-4b3d-89f1-b150fa64008e",
            "2d4ce8ab-e533-478f-bacf-bea0f249cca9",
            "30220141-dcea-4b2c-82ea-f6932b703abd",
            "30a1da14-4bcc-420c-8f6f-2bf454f76f89",
            "3397af2e-5ee1-4618-8617-928a579a7b2b",
            "33a87e63-83e7-4f98-a40e-96feece06697",
            "3479e792-95f8-4d4d-b196-2f8c6a125cd4",
            "37481f79-c4df-464a-bf6c-a75486171fd6",
            "3776c26b-553b-419d-b952-a2e0db712436",
            "397f7028-2225-43bd-9c38-80f857a464cb",
            "3a4bc43a-320e-496b-84e3-9cd7b192cafa",
            "3d241371-739e-4441-963c-b0159a01a19e",
            "3d29250c-190a-49f1-9ca5-21b78b2f378a",
            "3da34545-0b07-47f7-a10e-5b149e7b2634",
            "42a50b85-90bb-4955-9d63-ede2418818fa",
            "42b93dfd-edd2-4df7-9f64-fd9f2cf8b298",
            "42fff579-3e8b-4b57-babb-7ef5e6a31ba9",
            "4338b286-ca55-4eec-877f-ca22150bc313",
            "43554c84-ff61-4bb6-aa29-ed8bbc83b320",
            "45ea0d44-3b78-4fd1-90d0-9be8bac103e4",
            "467f9022-0098-4c13-81a4-228596dd3b6d",
            "48285984-e828-4033-8a54-dd4e5654f01e",
            "4847516a-87d2-4711-b571-a5f0d5c97f54",
            "48c1d635-cb9e-4f2a-96ec-07c30130e107",
            "498383c9-347c-434f-84e3-53e4f304d72a",
            "4afa1cfd-87fe-40a0-b8d9-0567336c3f98",
            "4b48cbcd-ecdf-418a-83d1-a109e527bcf1",
            "4c2f5efb-7665-403d-ba70-565b194ce592",
            "4ce3b837-de40-4f63-b43b-5171c043cd8b",
            "4d42ddcf-8e6c-4396-bf69-26dd2b288e2b",
            "4d53c7ba-0950-4e59-833e-2d2cb816a110",
            "4fe25066-c6a7-472a-b128-1829bc117da0",
            "50500b39-b2bb-4c1e-838b-b7874b8370a1",
            "56958185-085b-40b3-8c94-6e58181b7ca5",
            "56be11b4-9c6a-4350-b4ab-d660a127e72e",
            "5814d418-f75c-49fa-b3ff-910c85c9c9ba",
            "58ac7267-1489-431b-b1b4-b27995c63e8a",
            "58c53b75-bfa3-4b1a-b430-9eda7a788ee7",
            "59bbd743-7bfd-42bc-84d3-d0bb0898bea5",
            "5a4b139c-cf52-4e01-b03c-eea87102060a",
            "5a94418d-0b8c-4a5f-b8d8-c983eb71278f",
            "5c16d753-6196-48e1-9f33-bba0dd6ed13d",
            "5c3bb932-6c3c-468f-88a9-c8c61d458641",
            "5c43e9f1-79ae-4e95-a7ba-15defc4a9da1",
            "5df0fb42-67cf-4df8-8ba4-539ef53a3ba7",
            "5e5d01f9-5a85-4abc-9330-a89e7f44dbbb",
            "5ea4cbbf-d3ad-469d-8488-e3b5c9e14d66",
            "5ebc8906-0653-4bf2-9325-d4368a3d46db",
            "5f2ac589-4aa8-49e4-b05e-250966238126",
            "5fe5dcaa-b513-4dc5-a166-573627b3a159",
            "6122ca93-d106-4b25-b065-41b544edf7f7",
            "61f45545-f8cb-461e-a7a4-0dc551d1d004",
            "62176a70-123b-4d93-9811-b2038ecfd192",
            "630d3be3-d850-489c-94d3-bc52be73e5aa",
            "63fa6292-3d47-4714-83fc-3cd356f4a9fb",
            "64fdd981-c297-4460-b3c5-147da72f0ed0",
            "667a75f9-c138-42c3-b60b-0442b125545e",
            "66a82289-42b1-4fb6-a223-220e32129cfa",
            "67368907-8fe6-493f-be21-083e0ed0306b",
            "676a445d-bb91-4c54-aa79-f748cd862233",
            "69de2f4f-8180-46b7-8455-eb93ffb620f1",
            "6a2399bf-128f-4d7f-93e6-adf7addb7bb6",
            "6ac36432-8401-455a-9e3b-a2098de789c5",
            "6be85d5c-ed50-4f26-829e-5193f43208a5",
            "6d5fd117-932e-4145-89c7-a804c8b76658",
            "6e972622-d153-4795-8db0-35f2692d5dc8",
            "6fb8913c-4002-42c2-9c10-e9db64c2b2a3",
            "70e14a7e-9eac-46a0-9aab-6e6584e120ab",
            "718e98bd-522b-4ff0-b5cd-e955aa0d620b",
            "7394d8f5-2aae-4850-9a3e-e8dc71fad997",
            "74885e9d-0ba7-4c31-ad8a-2f5a2b4fc6e7",
            "74c13a66-8bf5-4f6e-9650-4e14615242cb",
            "7a731f97-f192-4f9b-ba2f-a5f7e6fa9b7a",
            "7ce05240-d123-4f98-9258-7c67fee072b5",
            "7f5d1ce1-ad62-48c3-891d-2feb0457a744",
            "82bc79eb-7392-4632-a970-eb7d087a9b8f",
            "836d2a01-b443-4b1a-92ca-642a907122fb",
            "8532d2cf-5b76-4801-a64f-cf9e12a138f7",
            "85a8cdbb-b77e-4d81-b843-9852c08c0215",
            "86a5d0fd-9685-43d7-8d52-13c20033db5d",
            "87b96191-fa48-423f-b671-8b942f81b8b0",
            "8988418a-46a0-461d-959b-07af93600051",
            "8ae39b2a-9963-42f5-8988-e5152803f870",
            "8c2e8172-65aa-4d5f-ad62-5383e3f1f939",
            "8c5c832c-e19f-4f46-83ed-3c4dd318ec84",
            "8cdb44ac-a3f6-449f-b354-7cd48cf84061",
            "8d651a30-6ca2-4201-921d-57cb78876a99",
            "8d68fedf-6682-4a51-8977-b860121d0b71",
            "8ee53e49-397a-4974-b996-923c19d3cf0a",
            "8f8b031e-48ad-4814-8eb0-0e51c2961831",
            "8fe224e5-640d-4a87-99b6-fa1e261713bf",
            "90e4b45c-8044-49b6-a842-5bd38dafe909",
            "926fad4d-58f9-4703-9298-300293e852cc",
            "959919cd-6b2f-4614-8d35-cd9442e7cb93",
            "95c85b47-abd5-4ff1-bf7f-7ec7cbb6e596",
            "97642bab-67c3-4371-aa43-b4c666583d36",
            "98f003d9-67d0-4a2e-9c32-eac85db84ea5",
            "99838854-3a79-4687-ac8d-5484f3c4f6aa",
            "999350d5-eb8e-4a6e-9ac3-f1690a3ea842",
            "9bbda31e-ad49-43c9-aaf2-f7d9896bac69",
            "9c24b693-4be6-4fee-8fec-9614208b37fc",
            "9d456e8d-b4a7-492e-bc9a-bd60dff93263",
            "9d6427da-be3c-40e6-adf5-144216d8db41",
            "9e19ffd6-a16d-4b13-ae6d-88c23b105202",
            "9e2804f4-de5a-4115-9388-07834b86d64c",
            "9ea541bf-5c7d-4910-8330-31e7f474c8b0",
            "a1aca5ef-b367-4b65-ab8e-e567e039e9c3",
            "a230e2f8-3203-4f06-8299-6a187f59b5eb",
            "a23235d1-15db-4b95-8439-a2e005bfff91",
            "a3b4e04d-1fbb-487e-9a70-18596c2a2239",
            "a40bf275-43ad-4948-9509-695448b0f94b",
            "a42cfa3a-43b6-49f1-be9b-8839596e9976",
            "a4ae414c-de85-4cce-a7b2-cce89a7b764c",
            "a87ec412-9067-4265-a7f4-a8054f7a1c46",
            "aba10ad2-fc76-482a-9ff6-beae79b7b3c6",
            "abb55ec9-187f-436e-9a1a-5ba431862e87",
            "ade38b7c-3887-4e67-8e39-c4f8c36799dd",
            "aeae578b-3e35-42a8-aa3f-1bcae5b1a84b",
            "afc96215-f657-4e45-b5e6-ff5152f52566",
            "b0ba2ebe-a39f-473f-8de1-4b49c8690940",
            "b1671f1a-3797-4611-a461-430f8836ecc4",
            "b24fd999-39a2-4837-8d99-194f958d36a4",
            "b2c1781b-17c0-4385-ae58-5ca7a67b741d",
            "b336e47e-89fc-40d9-9048-d4bfa232739c",
            "b3e784f7-26e9-4729-8264-e675b6bd8be1",
            "b72b5f69-e7e2-4eb0-ae43-d668c5b4609a",
            "b8d90545-c2e4-408b-891b-bc8a6f2f72ac",
            "bac6f5f0-d06b-41be-8122-b47ea55c8788",
            "bb32a3d6-8237-4434-b9b2-286b9cc07dc1",
            "bb5e640f-7f9f-48c5-8695-deb1e57f0a91",
            "bf2dbab1-06b0-4735-b270-270158435e32",
            "c0371335-9e49-4fb9-93c3-706e70fa6816",
            "c0e753fd-dfcf-4a82-9e39-da0c48c8846c",
            "c190547d-93bf-45fa-8e7f-43847fc0ad26",
            "c4d40921-9a54-4928-8409-ce5c5c456840",
            "c6317a07-e5db-4930-a7d5-bd6fe7ef7b5e",
            "c6b304ee-4826-4b27-a6b7-488e344ec3fe",
            "c993886c-1bec-463c-a4dd-baab41d8173f",
            "cc85a55a-6a5f-4376-a1e7-9def6d14f73b",
            "cccb9d35-4492-48ef-87e1-e550adfca1c6",
            "cead5d37-33b1-4d99-a054-e64168a887f6",
            "ceb6e600-b231-4f3e-9528-4131277712b7",
            "cedd15d4-6953-4ab8-b2c3-db44e1d961f6",
            "cf878df7-9ee1-43b4-b87a-d6a457e4f8f6",
            "cff26fca-aca8-4f1d-9486-ddb04669788b",
            "d058622b-9a0d-46d6-ba35-f69db29033c9",
            "d092d418-dcdd-4823-a232-eca65fe76fff",
            "d0c01627-4d4d-4e0f-a8cf-872b3965b082",
            "d0d50645-30a9-4207-9f58-ae6e0d360649",
            "d278cab0-d89b-4fca-b312-e5a5279015fa",
            "d32d6096-5429-454a-9716-5f3f04283e70",
            "d3d93b91-5ef4-40df-9456-d6915b2098a5",
            "d705dbcc-23e3-4069-98da-552ba69bea94",
            "d8c23fde-98ee-47d9-a571-945d1f04e5b8",
            "d9b66270-d5fe-4b3d-98ae-312822701bee",
            "dab6187e-44ef-407f-9621-1fc227ed6c11",
            "db2e469d-64f7-4fb2-b62d-bf54447a517e",
            "db2e51a5-9511-4d64-ac51-951d80da0101",
            "dc8aaac9-73a5-48aa-b04c-6679debab83f",
            "dce56925-2b5d-40fd-9023-28605ac3a8d9",
            "de0c43c2-ba63-44a8-b2e8-ace6b0d571ab",
            "dfc41a69-2240-4cf0-b2fe-2df310a7d156",
            "e0d9e42d-cdf9-4055-9da6-f806d4081402",
            "e252d253-751b-4c11-ac0d-879a48159947",
            "e2ee7b27-e1c5-42bb-8298-7b15aa9b1823",
            "e5e1100f-092a-4124-836e-e492a6352765",
            "e66d2a4e-bc86-479b-8927-8872bb07d6cf",
            "e780b529-e5dd-47f4-89b9-085eb0c305ff",
            "e881bd21-daa1-42fd-a157-832b37bbccd3",
            "e9ec654c-97a2-4787-9325-e6a10375219a",
            "ea7d8d70-11a2-48fd-8f20-82bd49ffce05",
            "ecb830a8-95c6-4528-bb97-771d7cd13dc4",
            "ecf2a72a-7f6b-496d-a333-45912d28efcd",
            "ef0b2857-5785-46e2-b02c-63f7171098ab",
            "f0048c10-f03e-4c97-b9d3-3506e1d58952",
            "f87fb18f-2ad0-40ce-92de-be63d7251bad",
            "f9cc8afd-c81c-44d8-a2f0-d0806ba6b0ee",
            "fa023ec7-866f-4c9e-a690-a999b2b83093",
            "fa3a8d3d-102d-4c8c-afc5-b1c44ebebddc",
            "fa43d352-05ef-40df-87a8-60ddfc6532d8",
            "fae99843-dafe-4d6e-9c9a-60e571168533",
            "faf9e5db-6959-4d2b-9a87-461a837d130f",
            "fb284fb4-227c-4d21-bc7c-510f804fa112",
            "fe25f3b3-ab7c-48a6-ad17-56de32613c54",
            "feeb6b81-8498-4125-94ce-1528510279c3",
            "ff360503-52f4-4bcb-9760-21032e85ef6f",
            "ffd781e3-3d89-49a1-99af-45446e138558"
        ],
        "04f6f5f5": [
            "033b55ea-b302-4dc0-8712-6a4f9c5891bb",
            "43b9fcb2-199d-484a-a0ee-ecd177ac8654",
            "642e1638-9814-483a-989b-35d15fa2c638",
            "6b15e8bd-cb47-471b-afae-6d6b572752bf",
            "6dd4e069-6192-450f-9679-3a4fe0b1f401",
            "7e35b463-181e-4951-8651-d3ab5a468ea0",
            "abd1e9cd-4685-4ddb-8fa3-0582e76e8669",
            "e03d658a-02ed-45aa-ba47-fd334f216c2d",
            "e2f14414-ace7-452c-a406-a9aa28f711a0",
            "e52de18b-8b53-48e1-800d-f73387df3373"
        ],
        "05759a6ab": [
            "03a6660a-ee76-4896-8673-0f9b9e65a1c0",
            "0d530de2-7c69-407b-a81e-58f8aa35e92a",
            "1b8853f2-29c4-4c1e-a04d-3976d4fca30e",
            "272b1e58-0469-43a4-97ad-12c0ba28aa77",
            "4de83bb7-b70d-4463-991d-171370d155ec",
            "563d622c-fc5b-4733-adfe-9c3c3706b09c",
            "583f998b-da82-4a5f-ab27-12109f495fa6",
            "59788439-5d0c-4124-842b-44d03e21f1bd",
            "61b4cc23-12d5-45ee-a302-b15d54acaf4c",
            "67a4a21e-ebe7-4f16-993d-a8f9c2549e7d",
            "6e6ccb6f-b30e-4cb3-9f91-ed6d59001410",
            "9a015ff4-f39b-41a5-bc1f-fff72dafcdb6",
            "a856e807-c132-4e91-bda1-b95a0593291b",
            "aa33de03-9b55-459d-b5b0-8e7c6fb20b00",
            "aa52ef21-75a9-45ac-9725-afbf473df0b7",
            "ac66e690-86e7-4aee-9368-fa9e02643e0b",
            "b1851886-6654-48f8-bbda-22f762d3e3b3",
            "bb6c6ad0-5e36-473d-a41f-370dfcdfcce5",
            "c9b8641e-3063-4634-b242-cb58a6a1215e",
            "cb10cfc4-0088-4bc5-8f3b-a2af00fb88b1",
            "cf7639d5-3f54-4160-94e6-01298d35fd02",
            "d5119d67-5224-4403-8130-5a1b50128280",
            "d99d4b2b-e612-48ee-a29d-f65d513fec3d",
            "e34ab6be-5473-4435-94e5-e6c24b404352",
            "fbd4f585-4b9f-483f-82ab-02085b92b38e",
            "ff4e6d82-2644-4e4b-9fe3-2ed10ec4429f"
        ],
        "05beb089c": [
            "045cbcbc-b04d-4966-942f-c5fa20e8b925",
            "ab791c45-5ec0-4a4f-aece-b8718992f0e0"
        ],
        "05cda1805": [
            "397f7028-2225-43bd-9c38-80f857a464cb",
            "4a1acad9-ce88-4334-aa62-bf23a5ccc79e",
            "718e98bd-522b-4ff0-b5cd-e955aa0d620b",
            "886dc670-2d16-4ddd-9ad5-40bc8f6cecfd",
            "d8c23fde-98ee-47d9-a571-945d1f04e5b8",
            "dc18bcef-c6d8-46b3-81a0-d91ce3ed83ea",
            "dda886d3-5eef-430c-b1ca-02d59e5cb8ba"
        ],
        "0608e18d3": [
            "0b79c326-c0e2-4d0f-bddb-3945ee7f73cd",
            "282e64c4-424f-4d65-b265-430ae59d9fa5",
            "37481f79-c4df-464a-bf6c-a75486171fd6",
            "5fcb8009-ae70-4dcd-aea5-079bc448ac55",
            "9289e00c-6c8c-4489-b57d-ee322cacc7d8",
            "9b326c9e-2120-4262-9131-04cc8d824e24",
            "b9abadb1-b872-4b4e-b997-81d092746c94",
            "bac6f5f0-d06b-41be-8122-b47ea55c8788",
            "bd489e17-6703-43ae-9f9a-f4962e2b5f6a",
            "fa43d352-05ef-40df-87a8-60ddfc6532d8"
        ],
        "06265726f": [
            "0c5946b9-cb83-41b9-af8e-1e882e5c6ca6",
            "74885e9d-0ba7-4c31-ad8a-2f5a2b4fc6e7",
            "ec7d16ec-e9ab-4bde-aded-feb81e933c40"
        ],
        "064345eec": [
            "c0e753fd-dfcf-4a82-9e39-da0c48c8846c",
            "cfe73f3f-b979-4827-a0b3-83e422a5d8df"
        ],
        "068065810": [
            "0b1a57cf-a3d2-4dc6-bc40-bb6112c63062",
            "9bbda31e-ad49-43c9-aaf2-f7d9896bac69",
            "feeb6b81-8498-4125-94ce-1528510279c3"
        ],
        "0694719b2": [
            "00d442fc-5cf0-417a-bb75-566f8d97b3b4",
            "09de0d73-53fe-44c3-8fd5-f02d3dfe80bb",
            "5bd44f21-8a1b-413d-8d64-08e3c4b67f90",
            "7d776ae8-340f-4a13-ba9e-8c05afa9ac30",
            "a0fc3f61-d6aa-459a-acce-8203cea59a66",
            "d4b7c258-db82-4cee-99c3-cbdd4c71b941",
            "ea90eb71-da66-44b9-b343-b577cdb7d53f",
            "fd62cb57-e79b-4fb0-865a-bce7bf226a16"
        ],
        "06c0f9654": [
            "092833c8-627a-4f92-9774-83e743083685",
            "5c72c12b-d37a-47aa-8a04-faa20f4d8be0",
            "78b9174d-35ce-4e92-af35-e003f3348d34"
        ],
        "06da3a89a": [
            "225cd024-e1df-4ebc-8769-ef31947d3051",
            "5683f3a3-b0ef-4b92-b5a4-643a3aa10911",
            "7e91eac6-b184-45c2-82c6-92362176638e",
            "8192a3ff-7ebf-4298-a972-68a6920c2616",
            "9e65e011-14ff-48cd-935d-4cda9c43c9a3",
            "b44e445a-e2fa-4326-9080-22ffacd9208e",
            "c0371335-9e49-4fb9-93c3-706e70fa6816",
            "dd06aa46-aac2-48a9-8d16-44de1a1fd36d",
            "e912bdb2-f94f-4721-b384-7c9c4e6ab229",
            "f89760d5-2554-46f1-ab00-c302382d8f11"
        ],
        "0769af81c": [
            "0a0a734f-3633-4b7e-8480-e01c52cb9b6f",
            "29158224-f8dd-4661-a796-1ffab537140e",
            "5914f056-b6c1-4bbd-979b-92b5b721c7f4",
            "5fad28f0-3c44-4f5e-9351-381fe3fb522c",
            "61f45545-f8cb-461e-a7a4-0dc551d1d004",
            "8c2e8172-65aa-4d5f-ad62-5383e3f1f939",
            "93c93965-d3d4-4483-a96e-497e1b243dbc",
            "95c85b47-abd5-4ff1-bf7f-7ec7cbb6e596",
            "d793451a-9795-447f-b8e6-00a2e2a17218",
            "e9ec654c-97a2-4787-9325-e6a10375219a",
            "ecf2a72a-7f6b-496d-a333-45912d28efcd"
        ],
        "076fc9c82": [
            "17edb7e7-93e7-45fb-ba64-741dc50a7d0d",
            "aba10ad2-fc76-482a-9ff6-beae79b7b3c6"
        ],
        "07a19187e": [
            "0c68b464-2b10-4d64-a025-e93952845161",
            "1c88086c-8bdc-4480-b651-82a71273f079",
            "43cf194b-951f-41d2-abf8-4b00596fc99c",
            "574a6b87-40f5-467f-9bd4-5decd2e86138",
            "7411c09d-2f89-4202-9d7a-1101493513d4",
            "8fe224e5-640d-4a87-99b6-fa1e261713bf",
            "e88648aa-c921-4c4c-a071-9ef8ea95a262",
            "f6874ded-313f-4ce5-a3d4-08925be31265"
        ],
        "07b5bdec8": [
            "0127a5c8-3692-46f9-8bfb-ab869d931ca3",
            "0a0a734f-3633-4b7e-8480-e01c52cb9b6f",
            "0c503f66-4348-4934-9ced-33b9e5b76e72",
            "0fa4979d-58c8-40d4-b4d7-b40a2b24fae1",
            "42d1a30b-c8ec-40b2-a673-12f6440ad588",
            "48b4c355-d155-4d8f-8e2e-a25ac45dbefe",
            "54981393-3c96-41e0-8694-b871cf7564d9",
            "60a385cf-e2f4-49ae-b1d0-071e77ded151",
            "6756aeaf-a941-47e1-b050-40340620e637",
            "7edc1f0f-2db2-45ba-8d93-2215736b67e5",
            "87b90ce4-92f1-4888-ad52-77f4164d6af0",
            "af984eb9-3ddc-4c57-aacd-82887f7a19ee",
            "daa887d9-e85c-4396-b8e3-03bce53ab4a1"
        ],
        "07cc94826": [
            "29158224-f8dd-4661-a796-1ffab537140e",
            "2d2f792f-a40c-49bb-a189-ed176a246e49",
            "31bc895a-c003-4566-a9f3-2e54ae1c17dc",
            "410fb916-8721-4663-bab8-34397391ace7",
            "55e78761-73ec-4def-8cbf-89c0a226a1a0",
            "81c8e597-ea47-4265-825d-f0eb9ed02c58",
            "b160fad8-5794-4076-81c8-d0729418d370",
            "d7b72419-f187-44f8-a7a4-d5a6cf85af48",
            "da51836a-bea6-4b04-b6b5-b1d715b49ef4",
            "e0d4d661-42f8-4d62-be14-4deffdf671d9",
            "e9ec654c-97a2-4787-9325-e6a10375219a"
        ],
        "0838691b6": [
            "006c6abe-45c5-421e-be27-44d827370cc9",
            "07ef7eba-7d2c-416f-8dc5-80b50fea62e3",
            "1ca0ad3c-f918-44f8-b146-9c3afd14da18",
            "4acfc9b0-d737-42b1-ade6-8254cbe914d3",
            "54a6caf2-73d4-4f69-a6dd-507e00f7539f",
            "789d279c-3505-47f1-aff4-62d2cd22ad66",
            "7ecd578f-9f4e-4b1b-aa7f-ca8ca0e2e7c0",
            "c247a0e4-43d3-4d92-a51a-6ed4c7a8c040",
            "cdb95d9b-b7ae-4d5d-bd67-fa757a4c282d",
            "cf1eac64-de9c-4dd6-9a26-a417abeb55e1",
            "d79c5202-76c4-4caa-938e-36a9452f100b",
            "de0c43c2-ba63-44a8-b2e8-ace6b0d571ab",
            "e8eda776-a770-42ff-bd4b-1ddaa5aac8d8",
            "f185a906-ccba-4a6e-9f1d-fd84839c25e8"
        ],
        "08636f384": [
            "2f267281-0313-4ab5-bb20-2c8c084b9db7",
            "37d4ee65-bd1a-47ae-be4d-eddc881e8ff0",
            "cc85a55a-6a5f-4376-a1e7-9def6d14f73b"
        ],
        "086ff1b91": [
            "1bd05074-4290-4ce2-87a2-81b7dfa838f6",
            "6f459ef4-925a-4952-9a46-3c374c0f48a4"
        ],
        "0885d953b": [
            "1c7cfb7a-1156-4ce2-94cc-449ee730297f",
            "8532d2cf-5b76-4801-a64f-cf9e12a138f7",
            "d6891d75-4cbd-44cd-88ff-7c3b6387946f"
        ],
        "088fdc44a": [
            "006c6abe-45c5-421e-be27-44d827370cc9",
            "009b55b1-565e-483a-8a5c-f9195d4cfa76",
            "02fb2d75-6aff-4e2f-b971-ecc8a65de7a5",
            "04ddd588-57c3-4d45-8322-0bb2eb8e0dd1",
            "073df302-1989-4307-aba9-73f2b6364525",
            "0742b288-ef58-49da-aa18-226ca275227f",
            "07ef7eba-7d2c-416f-8dc5-80b50fea62e3",
            "092833c8-627a-4f92-9774-83e743083685",
            "0c68b464-2b10-4d64-a025-e93952845161",
            "1132394a-d9cc-4421-8e45-fec617cde021",
            "1272c881-cc49-4cca-be3f-1dfd7be3a100",
            "16228643-2a90-4cd0-b781-45f6bdc338d6",
            "17ad7104-10d9-418c-96cc-0af9154385d4",
            "1be49286-2bf6-4081-9470-97cad682f5f9",
            "1c88086c-8bdc-4480-b651-82a71273f079",
            "1ca0ad3c-f918-44f8-b146-9c3afd14da18",
            "216ecf6c-1d78-4218-a52d-9ed7712b5831",
            "225cd024-e1df-4ebc-8769-ef31947d3051",
            "31e651f0-4cf7-4da8-a63c-d112bfb9f786",
            "37d4ee65-bd1a-47ae-be4d-eddc881e8ff0",
            "3910b006-24a5-4e8b-9c51-4592b88ad2c7",
            "3d241371-739e-4441-963c-b0159a01a19e",
            "4327d1d7-afcc-49af-b6e0-f0abd482649c",
            "43cf194b-951f-41d2-abf8-4b00596fc99c",
            "443c94c6-7c19-47d2-afea-91a6c097f7a6",
            "48285984-e828-4033-8a54-dd4e5654f01e",
            "4cf4ed66-5b23-4410-ad0e-ddeac313e067",
            "50500b39-b2bb-4c1e-838b-b7874b8370a1",
            "54a6caf2-73d4-4f69-a6dd-507e00f7539f",
            "5683f3a3-b0ef-4b92-b5a4-643a3aa10911",
            "574a6b87-40f5-467f-9bd4-5decd2e86138",
            "59bbd743-7bfd-42bc-84d3-d0bb0898bea5",
            "5c72c12b-d37a-47aa-8a04-faa20f4d8be0",
            "5c8f3de3-2dba-4e42-b354-d776cadfa12b",
            "5dec5a15-1c35-46ca-94a1-df6f0d024e29",
            "63c63e1a-d30c-450a-8795-e60c8408f715",
            "66b308cb-b553-47c2-a12c-21370349a729",
            "6f711013-a943-4391-a5e2-5dc5ee2c43fe",
            "721fb52f-dc4f-48e0-b583-43fb72c4b452",
            "73c15f0e-7dc2-4002-b206-11fef2951af3",
            "7411c09d-2f89-4202-9d7a-1101493513d4",
            "74885e9d-0ba7-4c31-ad8a-2f5a2b4fc6e7",
            "7635d991-faef-4770-bd40-3a3eebb3b281",
            "789d279c-3505-47f1-aff4-62d2cd22ad66",
            "7e7a797e-7896-4577-92a9-7c39115c97cd",
            "7e91eac6-b184-45c2-82c6-92362176638e",
            "7ecd578f-9f4e-4b1b-aa7f-ca8ca0e2e7c0",
            "7fd48fa0-f426-4a16-8ffa-35093afdef59",
            "8192a3ff-7ebf-4298-a972-68a6920c2616",
            "895585dc-65c0-41a6-a6c6-fff6ec95714b",
            "8fe224e5-640d-4a87-99b6-fa1e261713bf",
            "9c24b693-4be6-4fee-8fec-9614208b37fc",
            "9e65e011-14ff-48cd-935d-4cda9c43c9a3",
            "a8b8cff8-f7ee-405a-905d-00ee80a86c86",
            "b1b5f887-6e69-4701-b38c-b25252c3a569",
            "b2f29176-11c6-44dd-8c7a-0638772e4ead",
            "b412c14f-56c7-4725-8896-d342425609bd",
            "b44e445a-e2fa-4326-9080-22ffacd9208e",
            "b460ae13-6304-4916-a065-092f5544ae00",
            "c0371335-9e49-4fb9-93c3-706e70fa6816",
            "c08937b5-d001-4d18-8270-cd3ec1f0d4c0",
            "c247a0e4-43d3-4d92-a51a-6ed4c7a8c040",
            "caae3f7d-5afc-40df-b058-2c229216895e",
            "cc85a55a-6a5f-4376-a1e7-9def6d14f73b",
            "cdb95d9b-b7ae-4d5d-bd67-fa757a4c282d",
            "cf1eac64-de9c-4dd6-9a26-a417abeb55e1",
            "cff26fca-aca8-4f1d-9486-ddb04669788b",
            "d3d93b91-5ef4-40df-9456-d6915b2098a5",
            "d79c5202-76c4-4caa-938e-36a9452f100b",
            "dc1a17e2-17f0-4cae-b88f-af8176f08e24",
            "de0c43c2-ba63-44a8-b2e8-ace6b0d571ab",
            "dee9b327-1443-40c8-ab14-76110c4bae92",
            "e0906f59-9a5b-4b4a-9406-132008e36610",
            "e88648aa-c921-4c4c-a071-9ef8ea95a262",
            "e8eda776-a770-42ff-bd4b-1ddaa5aac8d8",
            "e912bdb2-f94f-4721-b384-7c9c4e6ab229",
            "ebe3fd18-d11e-449e-a9d7-f90daacfb94b",
            "ec7d16ec-e9ab-4bde-aded-feb81e933c40",
            "ecb7074a-215e-4245-976a-5e0669c94479",
            "ecec0041-4528-48f0-8b08-49f5358014b8",
            "f185a906-ccba-4a6e-9f1d-fd84839c25e8",
            "f2666293-f619-40bd-850e-04cb601c4f23",
            "f4a71a07-1bc0-459d-8e9a-07a7ee56485a",
            "f8357817-9926-4db9-a88c-9a171b5d3fea",
            "f89760d5-2554-46f1-ab00-c302382d8f11",
            "fb284fb4-227c-4d21-bc7c-510f804fa112",
            "fd15829e-3860-49b5-9e85-909d58750275"
        ],
        "08c4f0ebb": [
            "180c0977-60a4-4821-b910-2a561dbe412f",
            "9c4b8a1b-2741-4c4d-8133-c52edee68f5b"
        ],
        "08da3b456": [
            "50500b39-b2bb-4c1e-838b-b7874b8370a1",
            "895585dc-65c0-41a6-a6c6-fff6ec95714b",
            "b0a14769-d96d-4be0-b02e-fbddca936daa",
            "b2f29176-11c6-44dd-8c7a-0638772e4ead",
            "dc1a17e2-17f0-4cae-b88f-af8176f08e24"
        ],
        "08eaa4a09": [
            "8b018912-da14-4c07-a104-b7f00b41e9a9",
            "9c8dd523-f857-44ba-ba67-77eb5c258e76",
            "a5c2bb3a-45eb-41d7-ac46-f2823c669104",
            "d1ec606d-1a3a-4ebf-a9f1-5391b946cd31",
            "f9560c03-fff5-4eed-9035-0ec5e547e19c"
        ],
        "0903fc50b": [
            "13b58dd0-76e4-47ec-8a37-f89329404493",
            "14272b0f-9000-4015-a365-e12fc26abb21",
            "16f456fc-f3f2-4e0e-b469-3e8d83222be4",
            "17ecfbb7-86b1-4302-8bcf-5e3e67bece41",
            "1ca02602-2f5a-4484-8762-b014c741e229",
            "1f050b19-2ac9-4dbd-9907-e79b479a843e",
            "238d0017-c32c-44fa-bcc5-0211ed1cd08e",
            "24d5f805-9693-41e3-9916-b5a4c9d4a57e",
            "2e3e5873-faef-4a1f-9c02-fcc15cd67c18",
            "379b4755-9c85-429f-ae42-86b09f6aed36",
            "38aec0a3-0371-4216-8c40-f817a157cc4b",
            "3c45ae01-9d4a-4a4b-a37b-cd455578c477",
            "42357971-cd40-4884-aa4a-3fe679d5b921",
            "4243664a-6b56-4c14-8344-1d264f284d7e",
            "5bb86f14-3ed0-4990-af57-270b0bd5678f",
            "68127178-10b0-4e9c-82b5-cf5dcf9e789c",
            "6835e3f3-ed48-43db-b2fb-062450b6f5d3",
            "7dacc6e0-2bd8-4e8d-ad65-2f42f7b2c1cf",
            "7de01f9c-dfca-4591-8443-e0b8e1c090e4",
            "867ab745-6488-47a4-81d3-455c2a94ceee",
            "8bf395a3-1da7-4d31-9483-15efba4e6d51",
            "8cc7b00a-137b-426d-95f1-2e3161c753e0",
            "8f100c01-ea79-41e2-8aa8-16cf777b64e9",
            "8f91d550-c7af-4087-99ea-55c5540d109e",
            "94407aa6-7af9-4a74-84f2-8e80d38cad41",
            "95992f20-92f0-4657-ad0f-bc0cb82a90fb",
            "96364271-796e-4c1d-8543-32d35515bc59",
            "a2112db5-77e1-49b8-99f7-c5b76167609c",
            "a694183b-8e31-401e-a13f-692f22e1bea6",
            "aba9e519-81c2-4efe-9468-27e784ec0196",
            "b279ae58-afa2-4643-9f89-11feb92e1bba",
            "bdc29270-9213-43b9-9c21-bb8c6684c8a1",
            "c2176d76-6782-4489-8c79-f11a8a8830ee",
            "d4917060-3cf2-4dce-b5f2-4697fe91c839",
            "d6ac5272-f7f6-463c-9483-ac39ba7e68ba",
            "d6b3ffaa-1c65-4919-ab9e-e0bdcbf0898e",
            "ddea37ca-c992-42bf-9e40-1ff628fa5eb8",
            "e67f0372-3a3d-4133-9d6a-bd758c14a1cb",
            "e837c341-ce87-47e3-baf0-dfec2a1a0a0e",
            "e9566c3a-ec09-4e32-997a-de3573691f84",
            "ee8e098b-39ad-4a83-8b9d-97deb1baf03d",
            "f28e4d0e-ea5f-49ae-ab1d-70f531e28076",
            "f7e60ff2-0e18-4a73-b54f-ef4ee43fb020"
        ],
        "09197b393": [
            "6cb9374f-d49b-48dd-91f1-f0f3889f3fec",
            "8d651a30-6ca2-4201-921d-57cb78876a99"
        ],
        "0960f1722": [
            "073df302-1989-4307-aba9-73f2b6364525",
            "1132394a-d9cc-4421-8e45-fec617cde021",
            "16228643-2a90-4cd0-b781-45f6bdc338d6",
            "17ad7104-10d9-418c-96cc-0af9154385d4",
            "336a1d18-1647-4f4d-a297-0f54688e1cfe",
            "3910b006-24a5-4e8b-9c51-4592b88ad2c7",
            "6f711013-a943-4391-a5e2-5dc5ee2c43fe",
            "a8b8cff8-f7ee-405a-905d-00ee80a86c86",
            "cff26fca-aca8-4f1d-9486-ddb04669788b",
            "fd15829e-3860-49b5-9e85-909d58750275"
        ],
        "0a304c365": [
            "145e2c4b-09c7-4e03-85cc-3a9e0ca4207d",
            "48285984-e828-4033-8a54-dd4e5654f01e",
            "5c8f3de3-2dba-4e42-b354-d776cadfa12b"
        ],
        "0a4865fcb": [
            "29158224-f8dd-4661-a796-1ffab537140e",
            "397f7028-2225-43bd-9c38-80f857a464cb",
            "55e78761-73ec-4def-8cbf-89c0a226a1a0",
            "718e98bd-522b-4ff0-b5cd-e955aa0d620b",
            "7898c3aa-75db-4523-8b22-e60bf7d83784",
            "81c8e597-ea47-4265-825d-f0eb9ed02c58",
            "886dc670-2d16-4ddd-9ad5-40bc8f6cecfd",
            "99838854-3a79-4687-ac8d-5484f3c4f6aa",
            "a23235d1-15db-4b95-8439-a2e005bfff91",
            "b160fad8-5794-4076-81c8-d0729418d370",
            "d7b72419-f187-44f8-a7a4-d5a6cf85af48",
            "d8c23fde-98ee-47d9-a571-945d1f04e5b8",
            "da51836a-bea6-4b04-b6b5-b1d715b49ef4",
            "dc18bcef-c6d8-46b3-81a0-d91ce3ed83ea",
            "dda886d3-5eef-430c-b1ca-02d59e5cb8ba",
            "e9ec654c-97a2-4787-9325-e6a10375219a"
        ],
        "0b0d291bc": [
            "9c94b7d2-a8d6-44e0-9306-366da9af297a",
            "e66d2a4e-bc86-479b-8927-8872bb07d6cf"
        ],
        "0b7302988": [
            "1b484497-3bab-434f-986d-2f12c762a3ba",
            "29158224-f8dd-4661-a796-1ffab537140e",
            "30220141-dcea-4b2c-82ea-f6932b703abd",
            "3040cec3-8b19-4c7a-929c-9bff1cd882ed",
            "33a87e63-83e7-4f98-a40e-96feece06697",
            "397f7028-2225-43bd-9c38-80f857a464cb",
            "7b486d14-13bc-4be8-91e6-2efa50fc8715",
            "9bbda31e-ad49-43c9-aaf2-f7d9896bac69",
            "b83a3ad2-17dd-4c8f-9c20-30dba9f80f99",
            "d8c23fde-98ee-47d9-a571-945d1f04e5b8",
            "dc18bcef-c6d8-46b3-81a0-d91ce3ed83ea",
            "e35d2605-6a6c-4e32-8086-530b841a53a8",
            "e9ec654c-97a2-4787-9325-e6a10375219a",
            "f2fd3b02-16b0-4352-90fd-894cb73eb272"
        ],
        "0bbe36e7a": [
            "04ddd588-57c3-4d45-8322-0bb2eb8e0dd1",
            "08ca58e0-b0e7-4cbc-bddc-0e5a54bd8c48",
            "3d241371-739e-4441-963c-b0159a01a19e",
            "4cf4ed66-5b23-4410-ad0e-ddeac313e067",
            "5dec5a15-1c35-46ca-94a1-df6f0d024e29",
            "7635d991-faef-4770-bd40-3a3eebb3b281",
            "dee9b327-1443-40c8-ab14-76110c4bae92"
        ],
        "0bd5030e8": [
            "3172929a-6752-4840-b747-9ab45c973a88",
            "fa3a8d3d-102d-4c8c-afc5-b1c44ebebddc"
        ],
        "0c37889a0": [
            "98f003d9-67d0-4a2e-9c32-eac85db84ea5",
            "a812d421-cef6-49b1-88ec-f64a8d3fda71"
        ],
        "0c5aa5c7a": [
            "1c7cfb7a-1156-4ce2-94cc-449ee730297f",
            "5a8ba0b6-f0d2-40c8-a712-8a42f323c602",
            "65bb91f9-898a-4f92-a37d-fb84bed2cd5c"
        ],
        "0ca20fee8": [
            "02fb2d75-6aff-4e2f-b971-ecc8a65de7a5",
            "1713f177-ed64-4b6e-8535-a5cea8101f5a",
            "59bbd743-7bfd-42bc-84d3-d0bb0898bea5",
            "7fd48fa0-f426-4a16-8ffa-35093afdef59",
            "b412c14f-56c7-4725-8896-d342425609bd",
            "b460ae13-6304-4916-a065-092f5544ae00",
            "caae3f7d-5afc-40df-b058-2c229216895e",
            "ebe3fd18-d11e-449e-a9d7-f90daacfb94b",
            "ecec0041-4528-48f0-8b08-49f5358014b8"
        ],
        "0cb16329": [
            "216ecf6c-1d78-4218-a52d-9ed7712b5831",
            "66b308cb-b553-47c2-a12c-21370349a729",
            "7503bd7f-78f1-4c8b-b68b-202bcf26cc40",
            "9c24b693-4be6-4fee-8fec-9614208b37fc",
            "e0906f59-9a5b-4b4a-9406-132008e36610",
            "ecb7074a-215e-4245-976a-5e0669c94479",
            "f2666293-f619-40bd-850e-04cb601c4f23"
        ],
        "0d43bb00c": [
            "0107f7bb-a93d-4c7d-b0e4-2967304d3a22",
            "04bde641-0dc4-4f16-8a5b-688dc20e38e5",
            "1188769b-bd20-41c1-81b0-52a7cda472b3",
            "26c30d9f-add5-4218-b3e2-c0ad68c33899",
            "276d22af-1571-4d2d-9ba0-e97ff28b8df5",
            "2c5b589e-8c49-4f61-b48b-b90a655d31af",
            "336b8ac5-c44d-4419-8e69-0ffb8910eaa0",
            "3846f030-dc17-4aaf-b4e5-fc68e15115e3",
            "3be0c2da-0b75-4890-8c6e-cd3f3e8ccb46",
            "4dee9526-3350-459c-b2d9-62e637332701",
            "4f916f6f-c06a-45a8-929e-019c80c3df8e",
            "585568a2-a137-440d-930a-c92c3864a9b1",
            "5f8094b8-32ac-45b4-93e4-70aa6795365e",
            "5fbb01e9-eb13-415a-9f30-17a2a8e637f2",
            "60b070a2-91ad-45b8-a9bc-6ca678165e8b",
            "6d592fac-739f-43fc-b2ad-5a0e22e5e6f7",
            "6d707f84-4d45-4735-aca9-2a8e6465c3dd",
            "6e574349-bab4-4e43-b860-8e54dbcbda09",
            "709647ae-5aab-4247-988b-d07aab8dc2fd",
            "73cc07e5-2ba7-4c9a-a0b9-b7778e2536e9",
            "74ce78bd-6b55-482d-8249-a98d8e303198",
            "76924c13-b3b9-42c9-a8bd-61f59eaab236",
            "8482cbc1-1426-4289-8106-a0b2a0a172e0",
            "882013b2-8c26-441c-894d-d1ccc3fe7138",
            "988addaa-8694-457d-9735-bf937e3e7cc5",
            "9a3eca63-0e5f-440a-96c6-da5f15f89975",
            "a41e1abe-f2b0-4973-8b7f-8e2e32474304",
            "a473ce24-d8a4-4174-90d2-619d254237db",
            "a492da20-25c2-4a3b-aef5-cf34259ed405",
            "a4edf10a-382b-48b4-a259-90ba750f6f59",
            "b003010f-fc3a-40ff-b15a-7aae7419e759",
            "b08518a0-b1fe-4f1c-b2b8-b18bdef4a630",
            "b2ef90d0-53d4-40dc-85e8-3657ec41e761",
            "b6db1388-a08c-4034-b44a-79ed091917ed",
            "bfc5a117-78d6-40af-ae9c-a2ee8bc6f8b9",
            "c462b30b-01c4-474f-a739-55066947ee14",
            "c8782787-96e7-45b0-9822-e165b1bb84d9",
            "d4dd7c87-74a1-4d63-a70b-0c7531509b5e",
            "d8ea2224-0fb3-4783-ba15-c52de5877b51",
            "d91a620b-6005-4c66-b946-fa716df7bfa9",
            "e03a94f1-da24-45a3-bcba-0c9e221fbe19",
            "fa3d3893-c2fb-444d-8697-8a7fd8216a18",
            "fbdedaa2-842f-4ae9-b95c-5daef3d014c9"
        ],
        "0d44daa51": [
            "009b55b1-565e-483a-8a5c-f9195d4cfa76",
            "4327d1d7-afcc-49af-b6e0-f0abd482649c",
            "4369d0a6-ab05-48f7-b182-f4da05b42051",
            "63c63e1a-d30c-450a-8795-e60c8408f715",
            "7e7a797e-7896-4577-92a9-7c39115c97cd"
        ],
        "0d89244f2": [
            "5368328b-77a9-48e3-9398-ff4265ba65ed",
            "6df48294-00fd-4003-bee3-14447d7cceb0",
            "9b2e97ec-e82c-4602-b9b7-3c073095962d",
            "cdbf24b6-487a-47c4-89c8-a26c904af2d4"
        ],
        "0e31ddd59": [
            "1f337ec9-dafc-41c7-9f98-213d943efcb9",
            "3776c26b-553b-419d-b952-a2e0db712436",
            "5814d418-f75c-49fa-b3ff-910c85c9c9ba",
            "70e14a7e-9eac-46a0-9aab-6e6584e120ab",
            "8aa59a77-ca7f-41f2-8ff5-0978bd1fe4e8",
            "b336e47e-89fc-40d9-9048-d4bfa232739c",
            "d0c01627-4d4d-4e0f-a8cf-872b3965b082"
        ],
        "0ea0720cb": [
            "01ce6d04-c3f8-4fb2-90a0-063813b8fd9b",
            "0a88337d-d80a-4ebb-91a8-d59053d13980",
            "0d530de2-7c69-407b-a81e-58f8aa35e92a",
            "147fe42f-7640-4b8a-adf2-b4097e0c7e65",
            "1cc34a36-700a-461d-b136-0f8ce6b9a284",
            "21b4a1e0-3c00-4150-a6b6-b4b41829f1e2",
            "24670f05-f6d6-42a2-b7a4-ef9e463011f7",
            "29158224-f8dd-4661-a796-1ffab537140e",
            "2d167769-a80b-4b3d-89f1-b150fa64008e",
            "30220141-dcea-4b2c-82ea-f6932b703abd",
            "306a0d16-09f5-4c00-9bdb-0dc53c00990c",
            "31aa3b0f-133e-4fe4-9daf-f5c267d8c8d1",
            "33a87e63-83e7-4f98-a40e-96feece06697",
            "35e3e7d7-2bcb-46cf-aa5e-c10fa40eb6d7",
            "36839b7e-2d75-4b5c-9e36-9c4005ac8be0",
            "38744dcf-8d01-40b5-868b-e74014d78dcd",
            "397f7028-2225-43bd-9c38-80f857a464cb",
            "3c5b70b1-96cb-4dc2-865e-8a883d2756d6",
            "3c896ebf-5977-4663-a603-5d50428f2373",
            "3d08e1eb-e155-4540-a4cd-8575dddb2be9",
            "3dae001a-9bfa-4fcc-b018-fb3c74e77eed",
            "45ea0d44-3b78-4fd1-90d0-9be8bac103e4",
            "498383c9-347c-434f-84e3-53e4f304d72a",
            "499cf869-7ba5-42bc-8564-795bbeb49a88",
            "50e77c44-284c-44b7-8797-2a246a7b60fd",
            "52a1abf5-8762-4bb7-82dd-c6c7afc88610",
            "52a7522a-a70c-48ea-a2ed-f4e08841a8d0",
            "55830f4d-a21d-4a98-a7c4-a3fb006c3d42",
            "5914f056-b6c1-4bbd-979b-92b5b721c7f4",
            "59788439-5d0c-4124-842b-44d03e21f1bd",
            "59821a17-576c-458a-9bbc-9930b55e0690",
            "5c3bb932-6c3c-468f-88a9-c8c61d458641",
            "5df0fb42-67cf-4df8-8ba4-539ef53a3ba7",
            "5fe5dcaa-b513-4dc5-a166-573627b3a159",
            "61b4cc23-12d5-45ee-a302-b15d54acaf4c",
            "6656bc3b-58ee-4b27-b058-13208d96e148",
            "66a82289-42b1-4fb6-a223-220e32129cfa",
            "718e98bd-522b-4ff0-b5cd-e955aa0d620b",
            "779b5b85-5444-47b7-aab8-7e1206186bb1",
            "792edbfd-c7c3-4fbc-af11-abc267ab9d7d",
            "7f5d1ce1-ad62-48c3-891d-2feb0457a744",
            "7f83d309-ba3c-4869-8942-2d727697a3c3",
            "84b64846-a29a-4dab-a733-b8c5744becd6",
            "85246ecf-6f60-49c6-bc6f-f18606e771b2",
            "8c2e8172-65aa-4d5f-ad62-5383e3f1f939",
            "8e32e58c-94c2-4196-8360-83171aa5e873",
            "8ee53e49-397a-4974-b996-923c19d3cf0a",
            "8f8b031e-48ad-4814-8eb0-0e51c2961831",
            "8fa76a26-7936-4bc0-af9b-c81d5f39cb0b",
            "93c93965-d3d4-4483-a96e-497e1b243dbc",
            "95c85b47-abd5-4ff1-bf7f-7ec7cbb6e596",
            "99838854-3a79-4687-ac8d-5484f3c4f6aa",
            "9a015ff4-f39b-41a5-bc1f-fff72dafcdb6",
            "9bbda31e-ad49-43c9-aaf2-f7d9896bac69",
            "9ca3657f-6216-4cb9-bc0b-f6972a05ae86",
            "9d456e8d-b4a7-492e-bc9a-bd60dff93263",
            "9ef235e3-46fd-45ef-8e0a-d6531e7f1ffb",
            "9fe76483-1a41-4ec8-9779-147f104cf4f7",
            "a23235d1-15db-4b95-8439-a2e005bfff91",
            "a856e807-c132-4e91-bda1-b95a0593291b",
            "aa33de03-9b55-459d-b5b0-8e7c6fb20b00",
            "aa52ef21-75a9-45ac-9725-afbf473df0b7",
            "abb55ec9-187f-436e-9a1a-5ba431862e87",
            "afc96215-f657-4e45-b5e6-ff5152f52566",
            "affa8f85-a84c-4118-b555-3675656670f4",
            "b043e12f-af52-49e5-ab0e-17f848fbd3ec",
            "b0737cf8-25e7-4122-a577-66181b3ffbb8",
            "b1851886-6654-48f8-bbda-22f762d3e3b3",
            "b421470e-abca-4b84-9e4c-eab8b6b7806e",
            "b85a52c9-fae9-4d2f-ac10-238071cc869f",
            "b874b9e3-55b7-46b5-911e-46d8e00ae34d",
            "bf6861d5-7745-42f4-a6a7-82278bb6d583",
            "c35d6dfe-70ee-4bc0-8bc8-38b9ec4618f2",
            "c4d40921-9a54-4928-8409-ce5c5c456840",
            "c51375b5-4c61-4f7c-b50f-a1a4dfec35d2",
            "c7497944-f5d2-423e-a9ff-ae79ed23e315",
            "cb10cfc4-0088-4bc5-8f3b-a2af00fb88b1",
            "cc8fee98-6e24-485c-8c73-0c51abc59d09",
            "ceb4144f-fb9b-4869-ac37-cbcff0a0bef7",
            "cf017ebf-8a52-4b86-b6b8-e84ad1dd9ff0",
            "cf4f1ee6-e71c-44a5-adf4-d71d7d33530e",
            "cf7639d5-3f54-4160-94e6-01298d35fd02",
            "d5119d67-5224-4403-8130-5a1b50128280",
            "d8c23fde-98ee-47d9-a571-945d1f04e5b8",
            "dfc41a69-2240-4cf0-b2fe-2df310a7d156",
            "e4104d7a-3e91-4f48-94a8-2b6042c86ed4",
            "e5e1100f-092a-4124-836e-e492a6352765",
            "e7b17174-dc37-407e-8cd3-09b440c7d89d",
            "e999a047-0d36-4d3b-a381-c51488eebc23",
            "e9ec654c-97a2-4787-9325-e6a10375219a",
            "ea0d9383-c0d7-4b7e-9fa8-4d16992991d5",
            "ecf2a72a-7f6b-496d-a333-45912d28efcd",
            "efb1084f-0b5a-4980-b187-1e949555cb55",
            "efcf61ec-d603-43b6-b57d-aecfbe86490b",
            "f0048c10-f03e-4c97-b9d3-3506e1d58952",
            "f61f86a6-5dce-4f73-b12b-d3053bb15aa8",
            "fbd4f585-4b9f-483f-82ab-02085b92b38e",
            "fc4b62c0-fc72-4386-ab3d-e184b06bc9d7",
            "feeb6b81-8498-4125-94ce-1528510279c3"
        ],
        "0ec672a2a": [
            "0f4ac92f-b2b9-4f92-a58b-330190ba9e60",
            "102f4dd8-cb58-463e-bc4f-1b0a85e3a345",
            "1f7d9ced-8c7a-4afb-865a-e73236e33562",
            "2113dde6-d3ae-4298-947d-6f74dda3f683",
            "293b5669-025c-486a-b872-8323da18436f",
            "2b5fe287-d337-41ad-be98-d757ea40791f",
            "2c0b5062-8b59-4c1e-abfb-89b7d6bb517d",
            "31a389df-2100-4e4b-89b3-eb2607a2b7bc",
            "322ee6ba-7532-47a0-8128-5e63b474c582",
            "32c8c078-be76-4229-8a1c-b3d986764fb6",
            "35b3b2d4-3f4d-4b82-98b9-f635952c7fb7",
            "39f0cacf-e4a4-40eb-bffb-ee05ad70b2bb",
            "3dbce642-676b-4129-a151-ef0011d82d15",
            "3edb56bb-012e-4b13-a421-2751d3a2afa5",
            "4b40d27e-d45c-42e4-9eb7-d0bc2900de7a",
            "4bcb0c38-9d2e-4701-b877-f7c0df2f4f9f",
            "4da718fc-aa53-4be0-a166-f8269b715272",
            "58d6af99-d267-455e-8401-2b77b7454370",
            "59735cfe-7b45-41af-944b-a86ea7e0f4e1",
            "6062c9da-a21f-49ce-b7aa-a57031cbd27a",
            "64fd300f-ac11-4467-a6f8-91ee90f766ee",
            "7e91d978-d04d-45da-b0d8-c24810bc0c20",
            "86c83b49-ab28-415d-a2e1-7929c62a48f4",
            "870a43c8-fa7e-46f1-999c-3c5fd18d5365",
            "922abc6d-8ef3-45e1-8dab-e30cdd5ef110",
            "95742161-74af-4cad-9b14-9539902da54b",
            "9a764540-1212-4f35-a443-8738c6d13e91",
            "9c5e7965-202b-4206-a3c2-4b39d7a2b0e1",
            "a13a8bc0-f8dc-4c3f-b37b-edb746c05dae",
            "aa98f5fe-95cd-4633-8e11-c40deb30b72a",
            "ac582907-0ed7-4ad1-99d2-e4ae45d24ddf",
            "bcc4657a-c4e1-4ea7-9055-a4bd8db3e4f2",
            "c485c89b-5f66-43d0-bb9a-5de9cf23d4d2",
            "c7aa7464-f605-4b77-80db-fcc3ebced9f0",
            "c8811479-fd90-4c3a-9ac6-61a1dfeb1c0e",
            "ccd18d7e-f6c1-4764-8fd5-2bacd8e34d36",
            "d19d9b02-9fff-4b3c-ab53-0d4de68e5ad3",
            "d229eb34-e990-456f-810d-447881995cb2",
            "dcd60dc6-9aa2-4a73-b761-a68d48fce76f",
            "e11fbea4-d261-413d-beca-282a0edad004",
            "e7251a80-651a-43bf-8f5c-5a04eb85ab29",
            "efe76235-f585-45dc-9a59-cc7b4e983769",
            "fdd0ef1e-7b34-4e7c-8cd2-922c3e424299"
        ],
        "0eeaf7ca4": [
            "081223b3-b04d-4d0f-bc16-a80718b48e62",
            "1054cdb7-065f-4a23-80fe-bb1896a9f6d6",
            "1abc02dc-385f-42e1-8538-c55cb93d544a",
            "2352a9bf-5e78-4c9c-9509-33673bea9b74",
            "254047b0-2815-4789-b31d-33f7caf0c12e",
            "31a27cc7-b11c-4c19-b249-e3a0c1770ec4",
            "31a3e154-80f7-49c6-93dc-3175b26fa332",
            "3206a1c4-cbc6-4dff-b19b-d145312257e1",
            "3818ec47-6770-4357-bf71-f319d6ec6bb6",
            "3b20ea89-c10e-4437-b1c7-6b7c14e09811",
            "3f995474-491e-4f4e-8644-25d5940be4d3",
            "40b74c6a-207b-4d26-95ce-f4ad15edf09a",
            "44129e17-53c1-4d26-9254-bd4a631eaffa",
            "4a110901-52ed-4eb3-af08-5341ae4b93cc",
            "5071c6b3-5b8f-4da1-baaf-59d19d6a22b0",
            "50bfe9e8-2df2-49fe-9cdf-1412023aa3b6",
            "56f0c8d0-5d35-431b-bda9-50a8cac76b1f",
            "57c08b9b-a3bc-41e7-b4d2-b307d901caa2",
            "599ee608-1a08-48ef-b0be-643bce6bcb26",
            "64791f91-8f1b-4782-ae02-061bb3f6ec7d",
            "6ab272cb-1935-48d5-aedb-7534f08b321a",
            "726d60cb-b817-43a0-89ab-a6ce2e876b06",
            "772eb957-95ef-4401-b6d2-ef125643fbfa",
            "813d2583-3a1c-4686-8c12-be33c893fe52",
            "824f364f-6403-4ecb-8049-3cb432746878",
            "85210fff-afc9-418a-8b69-e79caf61632f",
            "8c76d92e-5904-4628-8f81-7d602f18f8e9",
            "99d2b170-2609-4014-b6d9-35262670becd",
            "a10b6e2b-6769-4441-8b30-8ecb58302982",
            "a185106a-459a-4ed0-8884-eea7337bc23f",
            "a8d3e765-2db3-4289-9245-411651957e6a",
            "aa02e7a4-30b1-4dab-a152-6ce669de7472",
            "b5a659d5-eae2-4021-b6bf-dae425caa639",
            "d6c27a49-a8c5-4566-80e5-2ed6c6183140",
            "db32874e-c5f0-4610-b7a0-9275ce5b9aad",
            "e115ae6a-05e3-405c-a5c4-cf9ed1488859",
            "e88b910a-e9fc-49b4-bb6b-709011be0fd6",
            "e9893f59-7e53-46a2-9bae-31d0a58db638",
            "f64c1465-0fda-4a52-b12f-1906900281ce",
            "fbc3f897-47f6-462d-8d63-0e9192097dc3"
        ],
        "0efd69062": [
            "24697d0f-0baa-431b-82e8-d74c1124a91c",
            "4b48cbcd-ecdf-418a-83d1-a109e527bcf1",
            "5a4b139c-cf52-4e01-b03c-eea87102060a",
            "6fbdf7d9-eeba-4a7d-b7b4-bba89d411657",
            "926fad4d-58f9-4703-9298-300293e852cc",
            "ca63a0ab-a2bd-4ec1-8ec3-a72e34ed2499"
        ],
        "0f5998e4": [
            "737ce392-0510-4d29-abc2-788dd4d02bff",
            "ee5ff482-e2c9-47e9-972a-ee4e4f8f2adb"
        ],
        "0f6c7b32d": [
            "03082215-5162-43c9-87df-e1a275d40258",
            "0eb58ac1-9097-4322-8878-b0c4ffeead29",
            "1efbc606-874d-4c6c-b5e9-1799a28ad426",
            "225cf3da-f4d4-463d-ad5a-6db4211fec6f",
            "31aa3b0f-133e-4fe4-9daf-f5c267d8c8d1",
            "36839b7e-2d75-4b5c-9e36-9c4005ac8be0",
            "59821a17-576c-458a-9bbc-9930b55e0690",
            "65f60710-5e7a-420b-8be7-50f81c38f9c6",
            "6656bc3b-58ee-4b27-b058-13208d96e148",
            "79a3cddf-5aee-4cf6-b4bf-6a33c1fc99a3",
            "7c4bcc81-506d-4d5d-80b4-b13b9c4694a4",
            "7f224e05-a30c-4e02-afcb-4985914b24c8",
            "7f83d309-ba3c-4869-8942-2d727697a3c3",
            "94195fae-fcaf-440c-95d9-46b8dfa27914",
            "abf21e13-82d4-4347-b135-b547dfbbe254",
            "b043e12f-af52-49e5-ab0e-17f848fbd3ec",
            "c600d178-02b7-4c68-920c-c79cf0f74790",
            "ceb4144f-fb9b-4869-ac37-cbcff0a0bef7",
            "cf4f1ee6-e71c-44a5-adf4-d71d7d33530e",
            "e4f8042d-0ace-43c9-b944-929028c7e47b"
        ],
        "0fe02336b": [
            "0406e813-de32-4d34-8de8-c2774ac54fb7",
            "0a32c3c0-32d3-4c09-9d87-2bc46021a29b",
            "1fdd675c-f41b-4fdf-9ff2-8f848772a92a",
            "229ca516-c90e-4092-9f73-2f758b46b2fb",
            "2799b8e3-7e4a-4963-a626-dedf9f936481",
            "2a5d1317-547d-40b3-9360-1b876afa8189",
            "31f437f7-7887-42e3-8ed5-a251209d690c",
            "35246e42-2c3d-44d1-b9f1-9dd63d38e6ea",
            "6d733fad-01cd-404f-bc0f-a2e262378be4",
            "6d985fdd-4d46-4216-8f13-4a214f0c0977",
            "9283cd92-ac83-457a-b2e0-acaf8c7c46e7",
            "9935d202-1938-4b59-98b7-ba8383303d8c",
            "abe298a3-71f9-4e37-908d-007aa727983c",
            "b9e6033e-3d63-439a-bbcb-ef77a7574dcb",
            "c35aecc1-b59d-4b7b-a4c9-96ae664695f0",
            "cba31817-ea68-42a5-a400-1b979dd6e434",
            "ceddd85f-1240-4cce-81d8-d3cb25c13b31",
            "fa12fbe4-a9f9-45ed-9abb-f44fd8887906"
        ],
        "0fe3d7f65": [
            "29158224-f8dd-4661-a796-1ffab537140e",
            "498383c9-347c-434f-84e3-53e4f304d72a",
            "66a82289-42b1-4fb6-a223-220e32129cfa",
            "7f5d1ce1-ad62-48c3-891d-2feb0457a744",
            "8ee53e49-397a-4974-b996-923c19d3cf0a",
            "8f8b031e-48ad-4814-8eb0-0e51c2961831",
            "95c85b47-abd5-4ff1-bf7f-7ec7cbb6e596",
            "99838854-3a79-4687-ac8d-5484f3c4f6aa",
            "9d456e8d-b4a7-492e-bc9a-bd60dff93263",
            "a23235d1-15db-4b95-8439-a2e005bfff91",
            "abb55ec9-187f-436e-9a1a-5ba431862e87",
            "c4d40921-9a54-4928-8409-ce5c5c456840",
            "dfc41a69-2240-4cf0-b2fe-2df310a7d156",
            "e982967c-25c6-433c-8d4f-8f007344cfa1",
            "e9ec654c-97a2-4787-9325-e6a10375219a"
        ]
    },
    "orientation": "landscape",
    "debug": true
};
